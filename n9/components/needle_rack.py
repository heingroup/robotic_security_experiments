from north_robots.n9 import N9Robot
from north_robots.components import Grid, Location


class NeedleRackError(Exception):
    pass


class NeedleRackEmptyError(NeedleRackError):
    pass


class NeedleRack:
    def __init__(self, robot: N9Robot, tiers: int=4, rows: int=4, columns: int=22, safe_height_mm: float=200.0, name: str='needle_rack'):
        self.robot = robot
        self.tiers = tiers
        self.rows = rows
        self.columns = columns
        self.safe_height_mm = safe_height_mm
        self.grids = [Grid(f'{name}_{i}', rows=rows, columns=columns, use_probe=True) for i in range(tiers)]
        self.current_index = 0
        self.indexes = []
        self.build_grid_indexes()

    @property
    def empty(self) -> bool:
        return self.current_index >= len(self.indexes)

    @property
    def current_location(self) -> Location:
        grid, index = self.indexes[self.current_index]
        return grid.locations[index]

    def locate(self):
        for grid in self.grids:
            grid.locate(self.robot.controller)

    def build_grid_indexes(self):
        self.indexes = []

        for grid in self.grids:
            self.indexes += [(grid, index) for index in grid.grid_indexes()]

    def increment_location(self):
        if self.empty:
            raise NeedleRackEmptyError(f'Need rack empty')

    def pickup_needle(self):
        needle_location = self.current_location
        safe_location = needle_location.copy(z=min(needle_location.z + self.safe_height_mm, 305.0))

        print(needle_location, safe_location)

        self.robot.move_to_locations([safe_location, needle_location, safe_location], gripper=90,
                                     order=N9Robot.MOVE_Z_XY, probe=True)

        self.increment_location()


