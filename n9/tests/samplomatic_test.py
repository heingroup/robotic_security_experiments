import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
from niraapad.shared.utils import MO as NirapaadModes
NiraapadClient.niraapad_mo = NirapaadModes.VIA_MIDDLEBOX # Set the mode specific to Phase 2
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server

from north_c9.controller import C9Controller
from north_robots.components import GridTray, Location
from north_robots.n9 import N9Robot
import time
from sampleomatic_stepper.sampleomatic_stepper.sampleomatic_stepper_api import SampleomaticStepper
sm=SampleomaticStepper(serial_port=20)
#for i in range(100):
#    print(i,"started")
#    sm.home()
#    #time.sleep(2)
#    sm.move_needle(distance=-10,speed=2)
#    #time.sleep(2)
#    #sm.move_needle(distance=-10,speed=1)
#    sm.move_needle(distance=5)
#    time.sleep(60)
##sm.move_needle(distance=-10,speed=2)
##sm.home()
c9 = C9Controller(device_serial='FT2FT5C1', use_joystick=True)
n9 = N9Robot(c9)
#c9.home(0,1,2,3)
for i in range(5):
    c9.output(0, False)
    n9.move(x=-237.1-7.5, y=136.0-16, z=300, gripper=-175.51)
    n9.move(x=-237.1-7.5, y=136.0-16, z=167.00810058593748, gripper=-175.51)
    c9.output(0, True)
    n9.move(x=-237.1-7.5, y=136.0-16, z=300, gripper=-175.51)
    reaction_grid_gripper = GridTray(Location(x=-170.5-9, y=157-16+8, z=218, rz=-90),
                                     rows=5, columns=4, spacing=(20, 20))
    reaction_vial_positions = [
        'A1', 'A2', 'A3', 'A4', 'A5',
        'B1', 'B2', 'B3', 'B4', 'B5',
        'C1', 'C2', 'C3', 'C4', 'C5',
        'D1', 'D2', 'D3', 'D4', 'D5', ]
    for i, val in enumerate(reaction_vial_positions):
        location = reaction_grid_gripper.locations[val]
        safe_location = location.copy(z=300)
        n9.move_to_location(safe_location, probe=False, gripper=-175.5)
        n9.move_to_location(location, probe=False, gripper=-174.5)
        sm.move_needle(distance=-10)
        time.sleep(3)
        sm.home()
        time.sleep(60)
        n9.move_to_location(safe_location, probe=False, gripper=-174.5)
    n9.move(x=-237.1-7.5, y=136.0-16, z=300, gripper=-175.51)
    n9.move(x=-237.1-7.5, y=136.0-16, z=167.00810058593748, gripper=-174.51)
    c9.output(0, False)
    n9.move(x=-237.1-7.5, y=136.0-16, z=300, gripper=-175.51)
