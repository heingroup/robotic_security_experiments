import math
import numpy as np
from north_c9.controller import C9Controller
from north_c9 import axis
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.robotics import Location
from ur.components.vision_angle_measurement.measurement import VisionDotAngleMeasurement, NoDotFoundException

c9 = C9Controller(device_serial='FT2FT5C1', use_joystick=False)
centrifuge_power = axis.Output(c9,12)
ur = UR3Arm('192.168.254.88', default_velocity=25)
centrifuge_angle_measurement = VisionDotAngleMeasurement('../../components/vision_angle_measurement/roi_selection.json',
                                                         '../../components/vision_angle_measurement/colour_limits.yaml', camera_index=2)

# centrifuge_angle_measurement.select_roi()

CENTRIFUGE_START_ANGLE = 230
CENTRIFUGE_RADIUS = 16.0
CENTRIFUGE_LOCATION_JOINTS = [-212.22571541975591, -106.82900237286003, 104.07166432784166, -87.25573928626716, 270.19966077306316, 132.51733586134975]
CENTRIFUGE_LOCATION = Location(x=264.8, y=-10.54, z=152.8, rx=-180.0, ry=0, rz=180.0)

def centrifuge_relative_location(index, angle, radius=CENTRIFUGE_RADIUS):
    index_angle = index * 60.0 + angle

    x = math.cos(math.radians(index_angle)) * radius
    y = math.sin(math.radians(index_angle)) * radius

    return Location(x, y)


def move_to_centrifuge_measurement_location():
    ur.move_joints(CENTRIFUGE_LOCATION_JOINTS)
    ur.move_to_location(CENTRIFUGE_LOCATION.translate(z=50, y=100))


def calibrate_centrifuge_angle():
    index_location = CENTRIFUGE_LOCATION + centrifuge_relative_location(0, 0, radius=13)
    ur.move_to_location(index_location.translate(z=-10))
    print('Rotate the centrifuge so index 0 lines up with the probe')
    move_to_centrifuge_measurement_location()
    end_angle = centrifuge_angle_measurement.measure_angle()
    print(f'Calibrated angle: {end_angle}')


def rotate_centrifuge():
    safe_height = 10
    plunge_depth = 23
    # take 5-10 measurements
    raw_angles = []
    for i in range(10):
        try:
            raw_angle = centrifuge_angle_measurement.measure_angle(show_image=True)
            raw_angles.append(raw_angle)
        except NoDotFoundException:
            pass
    if len(raw_angles) == 0:
        raise NoDotFoundException
    raw_angle = np.median(raw_angles)
    angle = CENTRIFUGE_START_ANGLE - raw_angle
    relative_index_location = centrifuge_relative_location(0, angle)

    # move the arm into one of the indexes so we can start rotating
    index_location = CENTRIFUGE_LOCATION + relative_index_location
    approach_location = index_location.translate(z=safe_height)

    ur.move_to_location(CENTRIFUGE_LOCATION.translate(z=safe_height))
    ur.move_to_location(approach_location)
    ur.move_to_location(index_location.translate(z=-plunge_depth))

    lower_centrifuge_location = CENTRIFUGE_LOCATION.translate(z=-plunge_depth)

    # start rotating the centrifuge, splitting it up into 2 movements if needed
    if abs(angle) > 45:
        midpoint = lower_centrifuge_location + centrifuge_relative_location(0, angle / 2)
        endpoint = lower_centrifuge_location + centrifuge_relative_location(0, 0)
        ur.move_circular(midpoint, endpoint, velocity=10)
    else:
        endpoint = lower_centrifuge_location + centrifuge_relative_location(0, 0)
        ur.move_to_location(endpoint, velocity=10)


# calibrate_centrifuge_angle()
while True:
    move_to_centrifuge_measurement_location()
    #centrifuge_power.on()
    #time.sleep(4)
    #centrifuge_power.off()
    #time.sleep(5)
    rotate_centrifuge()
