import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
from niraapad.shared.utils import MO as NirapaadModes
NiraapadClient.niraapad_mo = NirapaadModes.VIA_MIDDLEBOX # Set the mode specific to Phase 2
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server

import inspect
from ftdi_serial import Serial

from north_devices.pumps.tecan_cavro import TecanCavro

serial_cavro = Serial(device_serial='FT3FJFPV', baudrate=9600)  # Cavro pumps default to 9600
dosing_pump = TecanCavro(serial_cavro, address=0, syringe_volume_ml=2.5)
dosing_pump.home()
dosing_pump.switch_valve(2)
dosing_pump.move_absolute_ml( 0.2, 1)
