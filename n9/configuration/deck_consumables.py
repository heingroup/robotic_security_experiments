from n9.configuration import deck
from north_robots.components import GridTray, Location
import pandas as pd
import time

# important paths to files
stock_info_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\stock_info.csv'
cup_info_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\cup_info.csv'
hplc_vial_info_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\hplc_vial_info.csv'

shaker_info_path= r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\shaker_location_info.csv'
tube_info_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\tube_info.csv'
solid_info_path=r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\solid_info.csv'
kinova_tray_info_path=r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\kinova_tray.csv'
stocks_df = pd.read_csv(stock_info_path)
cups_df = pd.read_csv(cup_info_path)
hplc_vial_df = pd.read_csv(hplc_vial_info_path)


def read_stocks_file():
    global stocks_df
    stocks_df = pd.read_csv(stock_info_path)


class Chemical:
    def __init__(self,
                 name: str,
                 density: float = None,  # g/mL
                 formula: str = None
                 ):
        self.name = name
        self.density = density
        self.formula = formula


class Container:
    def __init__(self,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        self.max_volume = max_volume
        self.safe_volume = safe_volume
        self.current_volume = current_volume


class DeckContainer(Container):
    def __init__(self,
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        """
        Some kind of container on the deck
        :param tray:
        :param index:
        :param capped:
        """
        super().__init__(max_volume=max_volume,
                         safe_volume=safe_volume,
                         current_volume=current_volume,
                         )
        self.tray: GridTray = tray
        self.index: str = index
        self.capped: bool = capped


class ChemicalContainer(Chemical, DeckContainer):
    def __init__(self,
                 name: str,
                 density: float,  # g/mL
                 tray: GridTray,
                 index: str,
                 capped: bool = False,
                 max_volume: float = None,
                 safe_volume: float = None,
                 current_volume: float = None,
                 ):
        Chemical.__init__(self,
                          name=name,
                          density=density)
        DeckContainer.__init__(self,
                               tray=tray,
                               index=index,
                               capped=capped,
                               max_volume=max_volume,
                               safe_volume=safe_volume,
                               current_volume=current_volume,
                               )


# function to organize vials on the deck
def get_clean_vial(with_stirbar: bool = True, type : str = 'cup'):
    # find which vial to use
    if type is 'cup':
        vials = pd.read_csv(cup_info_path)
    if type is 'hplc':
        vials = pd.read_csv(hplc_vial_info_path)

    #print(vials)
    # find first clean vial row
    vial = vials[(vials['used'] == False) & (vials['stirbar'] == with_stirbar)].first_valid_index()# row index in dataframe
    #print(vial)
    if vial is None:
        # no clean vials
        return None, None
    # read the index and capped status
    vial_index = vials.loc[vial, 'vial_index']
    vial_cap_status = vials.loc[vial, 'capped']
    vials.loc[vial, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            if type is 'cup':
                vials.to_csv(cup_info_path, index=False)
            if type is 'hplc':
                vials.to_csv(hplc_vial_info_path, index=False)

            break
        except PermissionError as e:
            time.sleep(1)
    return vial_index , vial_cap_status

# function to organize vials on the deck
def get_clean_tube(with_stirbar: bool = False):
    # find which vial to use
    tubes = pd.read_csv(tube_info_path)
    # find first clean vial row
    tube = tubes[(tubes['used'] == False)].first_valid_index()# row index in dataframe
    if tube is None:
        # no clean tubes
        # print('no clean tubes')
        return None
    # read the index and capped status
    tube_index = tubes.loc[tube, 'tube_index']
    tubes.loc[tube, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            tubes.to_csv(tube_info_path, index=False)
            return tube_index
        except PermissionError as e:
          tube_index

# function to organize vials on the deck
def get_empty_shaker_location(type: str = 'sample'):
    tube = None
    # find which vial to use
    tubes = pd.read_csv(shaker_info_path)
    if type=='cup':
        # find first clean vial row
        tube = tubes[(tubes['used'] == False)& (tubes['cup'] == True)].first_valid_index()# row index in dataframe

    if type=='sample':
        # find first clean vial row
        tube = tubes[(tubes['used'] == False)& (tubes['cup'] == False)& (tubes['filter'] == False)].first_valid_index()# row index in

    if type=='clean_assembly':
        # find first clean vial row
        tube = tubes[(tubes['used'] == False)& (tubes['cup'] == True)& (tubes['filter'] == True)].first_valid_index()# row index in dataframe
        # associate it with the last sample picked
        sample_tube = tubes[(tubes['used'] == True)& (tubes['cup'] == False)& (tubes['filter'] == False)].last_valid_index()# row index in dataframe
        sample_index = tubes.loc[sample_tube, 'tube_index']
        tubes.loc[tube, 'associatedwith'] = sample_index
    if tube is None:
        # no clean tubes
        # print('no clean tubes')
        return None, None
    tube_index = tubes.loc[tube, 'tube_index']
    weight = tubes.loc[tube,'weight']
    tubes.loc[tube, 'used'] = True
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            tubes.to_csv(shaker_info_path, index=False)
            if type == 'clean_assembly':
                return tube_index, weight
            else:
                return tube_index
        except PermissionError as e:
            tube_index

def get_stock_info(name: str = 'acetone'):
    """
    Example use:
        density, tray, index, capped = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    print(df)
    #return its info
    print(df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0])
    return df.loc[:,'density'].iloc[0], df.loc[:,'tray'].iloc[0], df.loc[:,'index'].iloc[0], df.loc[:,'capped'].iloc[0]



def get_stock_data(name: str = 'acetone'):
    """
    Example use:
        index, current_volume = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    print(df)
    #return its info
    print(df.loc[:,'index'].iloc[0], df.loc[:,'current volume'].iloc[0])
    return df.loc[:,'index'].iloc[0], df.loc[:,'current volume'].iloc[0]

def update_stock_volume(name: str= 'acetone', addition_volume: float = 0):
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that name
    df= stocks[stocks['name'].str.match(name)]
    update_volume=df.loc[:, 'current volume'].iloc[0]-addition_volume
    stocks.loc[stocks.name==name,'current volume']=update_volume
    stocks.to_csv(stock_info_path, index=False)
    return

def get_callibration_info(index: str = 'A1'):
    """
    Example use:
        R2, slope, int_y = deck_consumables.get_stock_info(solvent_name)

    :param name:
    :return:
    """
    # find which vial to use
    stocks = pd.read_csv(stock_info_path)
    # print row which contains that index
    df= stocks[stocks['index'].str.match(index)]
    print(df)
    #return its info
    print(df.loc[:,'R2'].iloc[0], df.loc[:,'slope'].iloc[0], df.loc[:,'y_int'].iloc[0])
    return df.loc[:,'R2'].iloc[0], df.loc[:,'slope'].iloc[0], df.loc[:,'y_int'].iloc[0]

def get_kinova_hplc_tray_info(solid: str, solvent: str, solubility: float, index: str):
    """function to keep track of info of the sample passed to kinova deck for hplc, returns next available spot row & column """
    #find which vial to use
    tray_location = pd.read_csv(kinova_tray_info_path)
    tray_location.iloc[-1, 2] = solid
    tray_location.iloc[-1, 3] = solvent
    tray_location.iloc[-1, 4] = solubility
    tray_location.iloc[-1, 5] = index
    if int(tray_location.iloc[-1, 0])>=1:
        if int(tray_location.iloc[-1, 1])>1:
            tray_location= tray_location.append({'row': tray_location.iloc[-1,0],'column': tray_location.iloc[-1,1]-1,
                              }, ignore_index=True)
        else:
            if tray_location.iloc[-1,0]-1 == 0:
                print("empty")

            else:
                tray_location= tray_location.append({'row': tray_location.iloc[-1,0]-1,'column': 6},ignore_index=True)

    tray_location.to_csv(kinova_tray_info_path, sep=',', index=False, mode='w')
    return tray_location.iloc[tray_location.shape[0]-1, 0],tray_location.iloc[tray_location.shape[0]-1, 1]


def get_solid_info(name: str = 'sample'):
    # find which vial to use
    solids = pd.read_csv(solid_info_path)
    # print row which contains that name
    df= solids[solids['name'].str.match(name)]
    print(df)
    #return its info
    print( df.loc[:,'index'].iloc[0])
    return df.loc[:,'index'].iloc[0]


def get_stock(name: str) -> ChemicalContainer:
    """
    Return a ChemicalContainer for a chemical using information from the stock csv file
    :param name: name of chemical
    :return:
    """
    read_stocks_file()
    info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
    name = info['name'].values[0]
    density = info['density'].values[0]
    tray = f"{info['tray'].values[0]}"
    index = info['index'].values[0]
    capped = info['capped'].values[0]
    max_volume = info['max volume'].values[0]
    safe_volume = info['safe volume'].values[0]
    current_volume = info['current volume'].values[0]
    if tray == 'deck.stock_grid':
        tray = deck.stock_grid
    stock = ChemicalContainer(name=name,
                              density=density,
                              tray=tray,
                              index=index,
                              capped=capped,
                              max_volume=max_volume,
                              safe_volume=safe_volume,
                              current_volume=current_volume,
                              )
    return stock


def update_stock_file(name: str,
                      column: str,
                      value):
    """

    :param str, name: stock name
    :param str, column: column heading
    :param value: new value
    :return:
    """
    read_stocks_file()
    info: pd.DataFrame = stocks_df[stocks_df.name == name]  # all the info for the chemical
    row_index = info.index[0]
    stocks_df.at[row_index, column] = value
    try:
        stocks_df.to_csv(stock_info_path, sep=',', index=False, mode='w')
        time.sleep(1)
    except PermissionError as e:
        pass


if __name__ == '__main__':
    a,b,index,c=get_stock_info('acetone')
    print(a,b,index,c)
    a,b=get_stock_data('acetone')
    print(a,b)