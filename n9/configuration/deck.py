import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server
from niraapad.shared.utils import MO
from niraapad.shared.utils import BACKENDS
NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})



import os
import time
import datetime
from shutil import copy

import cv2
import pandas as pd
from ftdi_serial import Serial
from hein_robots.universal_robots.ur3 import UR3Arm
from ika import Thermoshaker
from north_devices.pumps.tecan_cavro import TecanCavro
from north_c9.controller import C9Controller
from north_c9 import axis
from north_robots.components import GridTray
from north_robots.n9 import N9Robot
from ika.magnetic_stirrer import MagneticStirrer
from n9.components.managers import NeedleTrayManager
#from north_devices.scales.scientech_zeta import ScientechZeta
from north_robots.components import Location
from pathlib import Path
from mtbalance.arduino import ArduinoAugmentedQuantos
from yolov5.yoloCheckFunction import run

from heinsight.vision_utilities.camera import Camera
from heinsight.vision_utilities.colour_matcher import HSVMatcher
from hein_utilities.datetime_utilities import datetimeManager

# system specific constants
# switching sample loop on valves
#from n9.experiments.main_end_point_turbidity import experiment_folder

SAMPLE_INLINE = "A"
SAMPLE_BYPASS = "B"
HPLC_FILL = "A"
HPLC_INJECT = "B"
LOOP_A_INJECT = "A"
LOOP_A_LOAD = "B"
LOOP_B_INJECT = "B"
LOOP_B_LOAD = "A"

# ports on the Cavro pumps
SAMPLE_PORT = 1
SOLVENT_PORT = 2

# syringe volumes
SAMPLE_PUMP_VOL = 2.5  # mL
PUSH_PUMP_VOL = 2.5  # mL
DOSING_PUMP_VOL = 2.5  # mL
AIR_VOLUME = 0.273  # ml air needed to transfer sample from sampleomatic head to sample loop
# flow rates
STOCK_DRAW_RATE = 2
SAMPLE_DRAW_RATE = 1  # mL/min
SOLVENT_DRAW_RATE = 2  # mL/min
SOLVENT_PUSH_RATE = 2  # mL/min
SOLVENT_DRAW_RATE_PPUMP = 5  # mL/min push pump
SOLVENT_PUSH_RATE_PPUMP = 0.42  # mL/min  push pump
SAMPLE_FLUSH_RATE = 3  # mL/min

# flush volumes
SAMPLE_FLUSH_VOLUME = SAMPLE_PUMP_VOL  # mL
PUSH_FLUSH_VOLUME = PUSH_PUMP_VOL  # mL

#offset_cam = cv2.VideoCapture(2)
#cam = cv2.VideoCapture(2)
#arms
#UR_arm = UR3Arm('192.168.254.88')
#UR_arm.open_gripper(0.5)
c9 = C9Controller(device_serial='AM006376', use_joystick=True,debug_protocol=True)
#c9.home(0,1,2,3)
n9 = N9Robot(c9)
n9.home()
#to turn off
##shaker_output.on()
#to turn back on
#shaker_output.off()

#relay controlled  devices
centrifuge_output = axis.Output(c9,12)
#centrifuge_output.on()
#centrifuge_output.off()
#ur = UR3Arm('192.168.254.88')
UR_arm = UR3Arm('192.168.254.88')

arm = UR_arm
# tool_offset = Location(z=0.2-0.035)
# arm.robot.set_tcp([*tool_offset.position, *tool_offset.orientation])
#UR script paths for locations
#grid_sequence=URScriptSequence('../../UR/tests/testgrid.script')
#gun_swap_sequence=URScriptSequence('../../UR/tests/rbtprogrm.script')


# read gripper base current average
sum = 0
for i in range(10):
    sum += c9.axis_current(0)
ave=sum/10
print("gripper base current is", ave)
with open(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\gripper_base_current_data.csv', 'a+') as f:
    f.write(f'{ave}\n')

#scale = ScientechZeta(c9.com(1, baudrate=19200)) # baudrate = 19200, 9600
scale = None


# serial_cavro = c9.com(0, baudrate=9600)  # Cavro pumps default to 9600
print("Serial Initialization",flush=True)
serial_cavro = Serial(device_serial='FT3FJFPV', baudrate=9600)  # Cavro pumps default to 9600
print("serial after Initialization",flush=True)
dosing_pump = TecanCavro(serial_cavro, address=0, syringe_volume_ml=DOSING_PUMP_VOL)

# dosing_pump.home()
# # print(1)
# # dosing_pump.switch_valve(1)
# # print(2)
# # dosing_pump.move_absolute_ml(0,velocity_ml=5)
# # print(3)
# IKA heater
#heater_stirrer_port = 'COM16'
#heater_stirrer= MagneticStirrer(device_port=heater_stirrer_port)

# mini Ika
mini_heater_stirrer_port = 'COM17'
#mini_heater_stirrer_port = 'COM9'
print("Magnetic Stirrer Initialization",flush=True)
mini_heater_stirrer= MagneticStirrer(device_port=mini_heater_stirrer_port)

shaker_shutdown = axis.Output(c9,15)
shaker_shutdown.off()
#thermoshaker
#port = 'COM7'  # todo set to the correct port
port = 'COM16'
dummy = False # todo set to true if testing without connecting to an actual thermoshaker
kwargs = {
    'port': port,
    'dummy': dummy,
}
ts = Thermoshaker.create(**kwargs)
ts.watchdog_safety_temperature = 15.5
ts.start_watchdog_mode_1(30)
ts.start_watchdog_mode_2(30)
ts.switch_to_normal_operation_mode()

# Quantos
quan = ArduinoAugmentedQuantos('192.168.254.2', 13, logging_level=10)
quan.set_home_direction(0)
quan.lock_dosing_pin_position()
#quan.arduino.rms_current = 1300
#.arduino.idle_mode = True




needle_tray_state_file_location = str(Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\state\needle_tray.json'))
long_needle_tray_state_file_location = str(Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\state\long_needle_tray.json'))
filter_tray_state_file_location = str(Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\state\filter_tray.json'))

# Using NeedleTrayManager class to define and array of needle trays, then picking them up one by one by pickup method
#Position: x=297.7608154296875, y=146.70191650390626, z=164.9070361328125, gripper=-96.31454101562497

needle_tray = NeedleTrayManager(n9, trays=[
    # Eleanor update:original Location(x=296, y=149.2, z=158.4, rz=-89.45), rows=2, columns=24, spacing=(8.9, 8.8)
    #works for fist row     GridTray(Location(x=296.5, y=148.5, z=174, rz=-89.45), rows=2, columns=24, spacing=(8.9, 7.26)),
    GridTray(Location(x=296.4, y=148, z=158.4, rz=-89.45), rows=2, columns=24, spacing=(8.9, 9.7)),
    GridTray(Location(x=325.9, y=148, z=184.4, rz=-89.45), rows=2, columns=24, spacing=(8.9, 8.8)),
], probe=True, state_file=needle_tray_state_file_location)
long_needle_tray = NeedleTrayManager(n9, trays=[
    GridTray(Location(x=324.7, y=148.1, z=181.01, rz=-89.45), rows=2, columns=24, spacing=(8.9, 8.8)),
], probe=True, state_file=long_needle_tray_state_file_location)
filter_tray = NeedleTrayManager(n9, trays=[
    GridTray(Location(x=324.4, y=148.1, z=185, rz=-89.45), rows=2, columns=24, spacing=(17.5,8.8)),
], probe=True, state_file=filter_tray_state_file_location)

# single locations
# gripper
BALANCE_GRIPPER = Location(x=-187, y=-14.2, z=116.05)
CAP_PARKING = Location(x=-317.7, y=-16.4, z=186.75254150390628)
QUANTOS_HOLDER=Location(x=-223.8, y=-181.3, z=130.3299560546875)
VISION_OPEN_INIT=Location(x=187.4, y=203.7, z=144.63)
VISION_OPEN_FINAL=Location(x=187.4, y=196.6, z=144.63)
VISION_CLOSE_INIT=Location(x=185.7851318359375, y=159.11104736328124, z=143.4)
VISION_CLOSE_FINAL=Location(x=185.8, y=166.6, z=143.4)
SOLID_WASTE = Location(x=-249, y=161, z=117)
FILTER_STATION = Location(x=-196.9, y=204.1, z=139.89+14) # location for filter
FILTER_HOLDER = Location(x=-199.82,y=239.36,z=145) # location for filter
CAP_STATION = Location(x=-242.86, y=117.70, z=105)

# grids
stock_grid = GridTray(Location(-26.6, 315.7, 135, rz=-89), rows=2, columns=5, spacing=(24.8, 49))
# reaction_grid_gripper = GridTray(Location(x=-179.38223876953123, y=262.6559692382813, z=144.488525390625, rz=-91), rows=5, columns=4, spacing=(20,20))
reaction_grid_gripper = GridTray(Location(  x=-199.100537109375, y=262.2477783203125, z=137.68597412109375, rz=-90), rows=6, columns=4, spacing=(20,20))
hplc_vial_tray=GridTray(Location(x=-35.54768066406251, y=153.367578125, z=102.25, rz=-90), rows=6, columns=4, spacing=(20,20))
#hplc_vial_tray=GridTray(Location(x=-35.54768066406251, y=153.367578125, z=102.251708984375, rz=-90), rows=6, columns=4, spacing=(20,20))
#Changing elbow bias to C9Controller.BIAS_MIN_SHOULDER
#Changing elbow bias to C9Controller.BIAS_MAX_SHOULDER
#Changing elbow bias to C9Controller.BIAS_CLOSEST
tube_grid_gripper = GridTray(Location(x=-178.5, y=152.4, z=108.66886230468751, rz=-90.5), rows=6, columns=4, spacing=(20, 20))
tube_mini_grid_gripper = GridTray(Location (x=-312.2, y=116.2, z=102, rz=-91), rows=2, columns=2, spacing=(35.63, 25.1))#spacing=(35.63, 25.4))

#tube_mini_grid_gripper = GridTray(Location (x=-312.2, y=116.2, z=91, rz=-91), rows=2, columns=2, spacing=(35.63, 25.1))#spacing=(35.63, 25.4))
# 115.74068603515624
#vision_grid = GridTray(Location(x=153.3898681640625, y=175.037060546875, z=146.9, rz=-89), rows=2, columns=1, spacing=(30,30))
vision_grid_needle = GridTray(Location(x=168.1775634765625, y=185.0051025390625, z=146.782578125, rz=-89), rows=2, columns=1, spacing=(32.06,32.06))

# vision_grid_sampleomatic = GridTray(Location(x=183.2, y=185.5, z=263.2, rz=-91), rows=2, columns=1, spacing=(31,31))
# vision_grid=GridTray(Location(x=162.472998046875, y=185.3513427734375, z=145.5, rz=-89), rows=2, columns=1, spacing=(20,20))
vision_grid=GridTray(Location(x=172.2239013671875, y=186.6397705078125, z=146.782578125, rz=-89), rows=2, columns=1, spacing=(31.29,31.29))
vision_station_camera_port = 2
deck_camera_port = 0
offset_camera_port = 1
#quantos_camera_port=2




# checking which camera port is which
# display(Camera(port=deck_camera_port).take_photo())
# display((port=vision_station_camera_port).take_photo())
# Camera
# for saving images captured by the deck camera used to check if the needle is on the probe

def create_experiment_folder():
    dir_name = Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\deck camera')
    experiment_number = 1
    experiment_name = f'study_{experiment_number}'
    experiment_folder = Path(os.path.join(dir_name, experiment_name))
    while True:
        if experiment_folder.exists():
            experiment_number += 1
            experiment_name = f'study_{str(experiment_number)}'
            experiment_folder = Path(os.path.join(dir_name,experiment_name))
        else:
            Path.mkdir(experiment_folder)
            break
    return experiment_folder

path_to_save_images=create_experiment_folder()
#quantos_camera_images_folder = Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\quantos camera')
deck_camera = Camera(port=deck_camera_port,
                     save_folder=path_to_save_images,
                     datetime_string_format='%Y_%m_%d_%H_%M_%S',
                     )
offset_camera = Camera(port=offset_camera_port,
                     save_folder= Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\shakerlocationimages\B5'),
                     datetime_string_format='%Y_%m_%d_%H_%M_%S',
                     )
#quantos_camera = Camera(port=quantos_camera_port,
#                     save_folder=quantos_camera_images_folder,
#                     datetime_string_format='%Y_%m_%d_%H_%M_%S',
#                     )

def initialize_turbidity_camera(camera_images_folder: Path=None):
    datetime_format ='%Y_%m_%d_%H_%M_%S_%f'
    c = Camera(port=vision_station_camera_port,
               save_folder=camera_images_folder,
               datetime_string_format=datetime_format,
               )
    return c

# path to save file with roi selection to monitor turbidity
turbidity_monitor_base_file_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\vision_selection_monitor_roi\turbidity_monitor_base_file'
turbidity_monitor_base_file_path_json = fr'{turbidity_monitor_base_file_path}.json'
annotated_monitor_turbidity_roi_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\vision_selection_monitor_roi\vision turbidity roi check selection annotated.png'
not_annotated_monitor_turbidity_roi_path = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\vision_selection_monitor_roi\vision turbidity roi check selection.png'

# vision_needle_check error handling
vision_config_folder = Path(r'C:/Users/User/PycharmProjects/automated_solubility_h1/n9/configuration/vision_needle_check')
vision_error_check_path = vision_config_folder.joinpath('vision needle check.json')
needle_roi = 'needle'
vision_check = HSVMatcher()
vision_check.load_data(str(vision_error_check_path))

annotation_width = 10
annotation_height = 25
green = (0, 255, 0)
annotation_font_scale = 0.55
annotation_thickness = 2
datetime_formatter = datetimeManager()


#vision_needle_check(image=None) -> Tuple[bool, str]:
#needle_on: List[bool] = []
#if image is None:
#    images = deck_camera.take_photos(n=11, save_photo=False)
#else:
#    images = [image]
#for idx, image in enumerate(images):
#    clone = copy(image)
#    needle_on_probe: bool = vision_check.good(clone,
#                                              roi_name=needle_roi,
#                                              channel='h')
#    needle_on.append(needle_on_probe)
#    now = datetime_formatter.now_string()
#    cv2.putText(clone,
#                f'needle on probe check: {needle_on_probe}',
#                (annotation_width, annotation_height),
#                cv2.FONT_HERSHEY_SIMPLEX,
#                annotation_font_scale,
#                green,
#                annotation_thickness)
#    image_name = f'needle_check_{needle_on_probe}_{now}.png'
#    if idx == 0:
#        path_to_save_image = str(deck_camera_images_folder.joinpath(image_name))
#        cv2.imwrite(path_to_save_image, clone)
#needle_on_probe = needle_on.count(True) >= needle_on.count(False)
#print(f'needle on probe: {needle_on_probe} - {path_to_save_image}')
#n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)

#return needle_on_probe, path_to_save_image

def vision_needle_check(image=None):
    for i in range(10):
        time.sleep(1)
        now = datetime_formatter.now_string()
        image_name = f'needle_check_{now}.png'
        image = deck_camera.take_photo(name=image_name, save_photo=True)
        if image is None:
              image = deck_camera.take_photo(name=image_name, save_photo=True)
        else:
              image = [image]
    needle_on_probe , cap = run(weights="C:\\Users\\User\\PycharmProjects\\yolov5\\yolov5\\best.pt",
                        source=path_to_save_images,
                        imgsz=640, conf_thres=0.25, view_img=False, view_delay=1000)
    n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)

    #clone = copy(image)
    #cv2.putText(clone,
    #            f'needle on probe and cap check: {needle_on_probe},{cap}',
    #            (annotation_width, annotation_height),
    #            cv2.FONT_HERSHEY_SIMPLEX,
    #            annotation_font_scale,
    #            green,
    #            annotation_thickness)
    return needle_on_probe, cap

#def vision_cap_detection(camera = quantos_camera):
#    frame=camera.take_photo(save_photo=True)
#    # Convert BGR to HSV
#    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#
#    # define range of blue color in HSV
#    if camera == quantos_camera:
#        lower_blue = np.array([37,34,48] ,dtype=np.uint8)
#        upper_blue = np.array([132, 255, 160], dtype=np.uint8)
#    else:
#        lower_blue = np.array([25,124,93], dtype=np.uint8)
#        upper_blue = np.array([161, 255, 255], dtype=np.uint8)
#
#
#    # Threshold the HSV image to get only blue colors
#    mask = cv2.inRange(hsv, lower_blue, upper_blue)
#
#    # Bitwise-AND mask and original image
#    res = cv2.bitwise_and(frame,frame, mask= mask)
#    pixels = cv2.countNonZero(mask)
#    if pixels>1000:
#        return True

def position_record():
    axis_position_info = r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\axis_position_info.csv'
    df = pd.read_csv(axis_position_info)
    dic_row={'1':c9.axis_position(1), "1motor":c9.axis_position(1,motor=True),
             "1difference": abs(c9.axis_position(1) - c9.axis_position(1, motor=True)),
             "2":c9.axis_position(2), "2motor":c9.axis_position(2,motor=True),
             "2difference":abs(c9.axis_position(2) - c9.axis_position(2, motor=True)),
             "3":c9.axis_position(3), "3motor": c9.axis_position(3, motor=True),
             "3difference": abs(c9.axis_position(3) - c9.axis_position(3, motor=True)),"timestamp": datetime.datetime.now()}
    df=df.append(dic_row,ignore_index=True)
    if abs(c9.axis_position(2) - c9.axis_position(2, motor=True))>162:
        print("drift identified!Homing now")
        c9.home(if_needed=False)
    while True:
        try:
            # try to write the data to the csv file, but if the file is open, then dont crash the script because it cannot
            # be written to while it is open
            df.to_csv(axis_position_info, index=False)
            break
        except PermissionError as e:
            time.sleep(1)

def powercycle_shaker():
    shaker_shutdown.on()
    time.sleep(5)
    shaker_shutdown.off()
    time.sleep(25)
    port = 'COM7'  # todo set to the correct port
    # port = 'COM16'
    dummy = False  # todo set to true if testing without connecting to an actual thermoshaker
    kwargs = {
        'port': port,
        'dummy': dummy,
    }
    ts = Thermoshaker.create(**kwargs)
    ts.watchdog_safety_temperature = 15.5
    ts.start_watchdog_mode_1(30)
    ts.start_watchdog_mode_2(30)
    ts.switch_to_normal_operation_mode()
