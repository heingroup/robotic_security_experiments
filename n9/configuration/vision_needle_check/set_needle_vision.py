from pathlib import Path
import cv2
from heinsight.vision_utilities.colour_matcher import HSVMatcher
from heinsight.vision_utilities.roi import ROI
from n9.configuration import deck
from n9.experiments.reaction import uncap_needle, dump_needle, move_for_needle_check

root = Path.cwd()
save_path = Path.cwd().joinpath('vision needle check.json')
with_roi_path = str(root.joinpath('with roi.png'))
cap_on_path = str(root.joinpath('cap on.png'))
needle_path = str(root.joinpath('needle.png'))
no_needle_path = str(root.joinpath('no needle.png'))
needle_roi = 'needle'


def create_vision_file(long_needle: bool = False):
    if long_needle==True:
        deck.needle_tray.pickup()
    else:
        # make n9 pick up a needle
        deck.needle_tray.pickup()
    # move capped needle to check
    move_for_needle_check()
    deck.deck_camera.take_photo(True, cap_on_path)
    got_needle = input('type "y" if n9 got needle, n otherwise')
    while got_needle != 'y':
        dump_needle()
        if long_needle:
            deck.long_needle_tray.pickup()
        else:
            deck.needle_tray.pickup()
        # move capped needle to check
        move_for_needle_check()
        deck.deck_camera.take_photo(True, cap_on_path)
        got_needle = input('type "y" if n9 got needle, n otherwise')
    print("")

    # uncap
    uncap_needle()
    move_for_needle_check()
    deck.deck_camera.take_photo(True, needle_path)

    # dump
    dump_needle()
    move_for_needle_check()
    deck.deck_camera.take_photo(True, no_needle_path)

    # create matcher and make json file for needle check
    needle = cv2.imread(needle_path)
    matcher = HSVMatcher()
    # set up and save
    matcher.reference_image = needle
    matcher.add_roi(roi_type=ROI.rectangle_roi,
                    name=needle_roi,
                    window_name='Select the green part of the needle'
                    )
    matcher.hsv_reference_manager.update_reference(name=needle_roi,
                                                   h_ul=40,
                                                   h_ll=60,
                                                   )
    image_with_roi = matcher.roi_manager.rois_image(needle)
    cv2.imwrite(with_roi_path, image_with_roi)
    matcher.save_data(str(save_path))

    test()

def test():
    matcher = HSVMatcher()

    # load and test_tecan_cavro.py
    matcher.load_data(str(save_path))
    needle = cv2.imread(needle_path)
    no_needle = cv2.imread(no_needle_path)
    cap_on = cv2.imread(cap_on_path)

    matcher.roi_manager.show_rois(needle)
    matcher.roi_manager.show_rois(no_needle)
    matcher.roi_manager.show_rois(cap_on)

    needle_on = matcher.good(needle, roi_name=needle_roi, channel='h')
    print(f"""needle. should be true: needle on: {needle_on}""")
    needle_on = matcher.good(no_needle, roi_name=needle_roi, channel='h')
    print(f"""no needle. should be false: needle on: {needle_on}""")
    needle_on = matcher.good(cap_on, roi_name=needle_roi, channel='h')
    print(f"""cap on. should be false: needle on: {needle_on}""")


def live_test(long_needle: bool = False):
    matcher = HSVMatcher()
    matcher.load_data(str(save_path))

    # make n9 pick up a needle
    if long_needle:
        deck.long_needle_tray.pickup()
    else:
        deck.needle_tray.pickup()
    # move capped needle to check
    move_for_needle_check()

    # check
    deck.deck_camera.take_photo(True, cap_on_path)
    p = deck.deck_camera.take_photo(False)
    needle_on = matcher.good(p, roi_name=needle_roi, channel='h')
    print(f"""cap on. should be false: cap on: {needle_on}""")

    # uncap
    uncap_needle()
    move_for_needle_check()
    p = deck.deck_camera.take_photo(False)
    needle_on = matcher.good(p, roi_name=needle_roi, channel='h')
    print(f"""needle. should be true: needle on: {needle_on}""")

    # dump
    dump_needle()
    move_for_needle_check()
    p = deck.deck_camera.take_photo(False)
    needle_on = matcher.good(p, roi_name=needle_roi, channel='h')
    print(f"""no needle. should be false: needle on: {needle_on}""")


if __name__ == '__main__':
    #deck.needle_tray.reset()
    #dump_needle()
    #deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=308, probe=True)
    #create_vision_file(long_needle=False)  # set up needle check
    live_test(long_needle=False)  # test_tecan_cavro.py it






