from pathlib import Path
import cv2
import time
from heinsight.vision.turbidity import TurbidityMonitor
from heinsight.vision_utilities.camera import Camera
from n9.configuration import deck
from n9.configuration.deck import turbidity_monitor_base_file_path, annotated_monitor_turbidity_roi_path, not_annotated_monitor_turbidity_roi_path


def select_roi(dissolved_reference: float = None):
    """

    :param dissolved_reference: dissolved reference value to set
    :return:
    """
    roi_cam = deck.initialize_turbidity_camera(camera_images_folder=None)
    vision_selection_tm = TurbidityMonitor(turbidity_monitor_data_save_path=turbidity_monitor_base_file_path)
    deck.mini_heater_stirrer.target_stir_rate = 400
    deck.mini_heater_stirrer.start_stirring()
    time.sleep(10)
    vision_selection_tm = make_vision_selections(vision_selection_tm=vision_selection_tm,
                                                 camera=roi_cam,
                                                 annotated_regions_path=annotated_monitor_turbidity_roi_path,
                                                 not_annotated_regions_path=not_annotated_monitor_turbidity_roi_path,
                                                 )
    deck.mini_heater_stirrer.stop_stirring()
    if dissolved_reference is not None:
        vision_selection_tm.turbidity_dissolved_reference = dissolved_reference
    vision_selection_tm.save_json_data()
    vision_selection_tm.roi_manager.save_data(Path(r'vision selection'))
    roi_cam.disconnect()


def make_vision_selections(vision_selection_tm: TurbidityMonitor, camera: Camera, annotated_regions_path: str, not_annotated_regions_path: str):
    """
    Select region of interest and normalization region and saturated reference for all experiments
    :param tm_selection_path: path to json file to load selections for turbidity monitoring
    """
    image = camera.take_photo(save_photo=False)
    vision_selection_tm.select_normalization_region(image)
    vision_selection_tm.select_monitor_region(image)
    # save image with selected regions drawn on it
    selected_regions_image_annotated = vision_selection_tm.draw_regions(image=camera.last_frame)
    cv2.imwrite(annotated_regions_path,
                selected_regions_image_annotated,
                )
    selected_regions_image_not_annotated = vision_selection_tm.draw_regions(image=camera.last_frame, annotate=False)
    cv2.imwrite(not_annotated_regions_path,
                selected_regions_image_not_annotated,
                )
    # set dissolved reference
    dissolved_reference_images = camera.take_photos(n=200,
                                                    save_photo=False)
    vision_selection_tm.set_dissolved_reference(*dissolved_reference_images, select_region=False)
    vision_selection_tm.turbidity_dissolved_reference *= 1.1
    return vision_selection_tm


if __name__ == '__main__':
    # dissolved_reference_value = 23.0  # manually set dissolved reference value
    dissolved_reference_value = None  # manually set dissolved reference value
    select_roi(dissolved_reference_value)
    print("done")
