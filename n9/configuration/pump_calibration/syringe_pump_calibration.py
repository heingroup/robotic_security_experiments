from typing import List, Tuple
from datetime import datetime
import logging
from pathlib import Path
import pandas as pd
import time

from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot
from mtbalance.api.balance import Balance
from north_devices.pumps.tecan_cavro import TecanCavro
from n9.experiments import reaction
from n9.experiments import station_robot_engagement
from n9.experiments import capping_uncapping
from n9.configuration import deck_consumables, deck
from n9.configuration.pump_calibration.calibration_utilities import LinearRegression, update_df, save_csv, subset_means


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_slope_intercept_r2(calibration_csv_path: Path) -> Tuple[float, float, float]:
    """
    From a csv created from the SyringePumpCalibration class, return the slope, intercept, and r2 from the csv file
    :param calibration_csv_path:
    :return:
    """
    calibration_df = pd.read_csv(calibration_csv_path.absolute())
    slope: float = calibration_df['Slope (Calculated volume vs target volume)'].tolist()[-1]
    intercept: float = calibration_df['Y-int (Calculated volume vs target volume)'].tolist()[-1]
    r2: float = calibration_df['R2 (Calculated volume vs target volume)'].tolist()[-1]
    return slope, intercept, r2


class SyringePumpCalibration:
    def __init__(self,
                 pump: TecanCavro,
                 balance: Balance,
                 ):
        """
        To calibrate volume

        :param pump:
        :param balance:
        """
        self.pump = pump
        self.balance = balance

    def create_df(self):
        columns = ['Replicate',
                   'Calculated volume (mL)',
                   'Target volume (mL)',
                   'Calculated volume - target volume (mL)',
                   'Mass (g)',
                   'Pull flow rate (mL/min)',
                   'Push flow rate (mL/min)',
                   'From port',
                   'To port',
                   'Density (g/mL)',
                   'Slope (flow rate calculated vs target volume)',
                   'Y-int (flow rate calculated vs target volume)',
                   'R2 (flow rate calculated vs target volume)',
                   ]
        df = pd.DataFrame(columns=columns)
        return df

    def create_test_df(self):
        columns = ['Calculated volume (mL)',
                   'Target volume calibration adjusted (mL)',
                   'Target volume (mL)',
                   'Calculated volume - target volume calibration adjusted (mL)',
                   'Calculated volume - target volume (mL)',
                   'Pull flow rate (mL/min)',
                   'Push flow rate (mL/min)',
                   'From port',
                   'To port',
                   'Mass (g)',
                   'Density (g/mL)',
                   'Slope (flow rate calculated vs target volume)',
                   'Y-int (flow rate calculated vs target volume)',
                   'R2 (flow rate calculated vs target volume)',
                   ]
        df = pd.DataFrame(columns=columns)
        return df

    def dispense(self, volume: float, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int, stock_index: str='A1') -> None:
        """

        :param volume: mL
        """
        raise NotImplementedError()

    def weigh(self):
        """Return weight in grams"""
        raise NotImplementedError()

    def dispense_weigh_record(self, replicate, target_volume, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int, density, df, csv_output_path, stock_index: str ='A1'):
        initial_mass = self.weigh()
        print('dispense_weigh_record, stock_index= ', stock_index)
        self.dispense(target_volume, pull_flow_rate, push_flow_rate, from_port, to_port, stock_index=stock_index)
        time.sleep(1)
        final_mass = self.weigh()
        mass = final_mass - initial_mass

        volume = round(mass / density, 3)
        data = {
            'Replicate': replicate + 1,
            'Calculated volume (mL)': volume,
            'Target volume (mL)': target_volume,
            'Calculated volume - target volume (mL)': volume - target_volume,
            'Pull flow rate (mL/min)': pull_flow_rate,
            'Push flow rate (mL/min)': push_flow_rate,
            'From port': from_port,
            'To port': to_port,
            'Mass (g)': mass,
            'Density (g/mL)': density,
        }
        df = update_df(df, data)
        save_csv(df, csv_output_path)
        return df

    def make_flow_rate_calibration_curve(self,
                                         target_volumes: List[float],
                                         pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int,
                                         density: float,
                                         replicates: int = 3,
                                         csv_output_path: Path = Path.cwd().joinpath(f"Volume calibration {datetime.now().strftime('%d-%m-%y %H-%m')}.csv"),
                                         prime_volume: int = 0,
                                         stock_index: str='A1'):
        """

        :param target_volumes: mL
        :param density:
        :param replicates:
        :param csv_output_path:
        :param prime_volume: seconds
        :return:
        """
        self.set_up(prime_volume)
        df = self.create_df()

        for target_volume in target_volumes:
            for replicate in range(replicates):
                replicate_number = replicate + 1
                print(f'Replicate {replicate_number} target volume {target_volume}')
                print("make_flow_rate_calibration_curve, stock_index= ", stock_index)
                df = self.dispense_weigh_record(replicate, target_volume, pull_flow_rate, push_flow_rate, from_port, to_port, density, df, csv_output_path, stock_index=stock_index)

        calibration = LinearRegression()
        volumes_calculated: List = df['Calculated volume (mL)'].tolist()
        target_volumes: List = df['Target volume (mL)'].tolist()
        volumes_calculated: List[float] = subset_means(volumes_calculated, replicates)
        target_volumes: List[float] = subset_means(target_volumes, replicates)
        # y is the Calculated volume, x is the target volume
        calibration.add(dict(zip(target_volumes, volumes_calculated)))
        data = {
            'Slope (Calculated volume vs target volume)': calibration.slope,
            'Y-int (Calculated volume vs target volume)': calibration.intercept,
            'R2 (Calculated volume vs target volume)': (calibration.r_value * calibration.r_value),
        }
        df = update_df(df, data)

        save_csv(df, csv_output_path)
        print(f'Finish making volume calibration curve. File saved to: {csv_output_path}')
        return calibration

    def test_calibration(self,
                         target_volumes: List[float],
                         pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int,
                         calibration_csv_path: Path,
                         csv_output_path: Path = Path.cwd().joinpath(f"Test volume calibration {datetime.now().strftime('%d-%m-%y %H-%m')}.csv"),
                         prime_volume: int = 0,
                         stock_index: str='A1'):
        """

        :param target_volumes: mL
        :param calibration_csv_path:
        :param csv_output_path:
        :param prime_volume: seconds
        :return:
        """
        calibration = LinearRegression()
        calibration_df = pd.read_csv(calibration_csv_path.absolute())
        volumes_calculated: List = calibration_df['Calculated volume (mL)'].tolist()[:-1]  # ignore last value bc its nan bc its used to store information about the calibration slope, intercept, and r2
        calibration_target_volumes: List = calibration_df['Target volume (mL)'].tolist()[:-1] # ignore last value bc its nan bc its used to store information about the calibration slope, intercept, and r2
        replicates: List = calibration_df['Replicate'].tolist()[:-1]  # ignore last value bc its nan bc its used to store information about the calibration slope, intercept, and r2
        n_replicates = int(max(replicates))
        densities: List = calibration_df['Density (g/mL)'].tolist()[:-1]  # ignore last value bc its nan bc its used to store information about the calibration slope, intercept, and r2
        density = densities[0]
        volumes_calculated: List[float] = subset_means(volumes_calculated, n_replicates)
        calibration_target_volumes: List[float] = subset_means(calibration_target_volumes, n_replicates)
        # y is the volume calculated, x is the target volume
        calibration.add(dict(zip(calibration_target_volumes, volumes_calculated)))

        self.set_up(prime_volume)
        df = self.create_test_df()

        for target_volume in target_volumes:
            print(f'Testing volume {target_volume}')
            df = self.test_dispense_weigh_record(target_volume, pull_flow_rate, push_flow_rate, from_port, to_port, density, calibration, df, csv_output_path, stock_index=stock_index)


        calibration = LinearRegression()
        volumes_calculated: List = df['Calculated volume (mL)'].tolist()
        calibration.add(dict(zip(target_volumes, volumes_calculated)))
        data = {
            'Slope (Calculated volume vs target volume)': calibration.slope,
            'Y-int (Calculated volume vs target volume)': calibration.intercept,
            'R2 (Calculated volume vs target volume)': (calibration.r_value * calibration.r_value),
        }
        df = update_df(df, data)
        save_csv(df, csv_output_path)
        print(f'Finish testing volume calibration curve. File saved to: {csv_output_path}')

    def test_dispense_weigh_record(self, target_volume, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int, density, calibration, df, csv_output_path, stock_index: str ='A1'):
        initial_mass = self.weigh()
        calibration_adjusted_volume = calibration.calculate_x(target_volume)  # - calibration.intercept
        self.dispense(calibration_adjusted_volume, pull_flow_rate, push_flow_rate, from_port, to_port,
                      stock_index=stock_index)
        time.sleep(1)
        final_mass = self.weigh()
        mass = final_mass - initial_mass

        volume_calculated = round(mass / density, 3)
        data = {
            'Calculated volume (mL)': volume_calculated,
            'Target volume calibration adjusted (mL)': calibration_adjusted_volume,
            'Target volume (mL)': target_volume,
            'Calculated volume - target volume (mL)': volume_calculated - target_volume,
            'Calculated volume - target volume calibration adjusted (mL)': volume_calculated - calibration_adjusted_volume,
            'Pull flow rate (mL/min)': pull_flow_rate,
            'Push flow rate (mL/min)': push_flow_rate,
            'From port': from_port,
            'To port': to_port,
            'Mass (g)': mass,
            'Density (g/mL)': density,
        }
        df = update_df(df, data)
        save_csv(df, csv_output_path)
        return df

    def prime_lines(self, prime_volume: int,  from_port: int, to_port: int,):
        """

        :param prime_volume: mL
        :return:
        """
        return NotImplementedError()

    def set_up(self, prime_volume: int = 0, from_port: int = 1, to_port: int = 2):
        raise NotImplementedError()


class TecanCavroCalibration(SyringePumpCalibration):
    def __init__(self,
                 pump: TecanCavro,
                 balance: Balance,
                 ):
        super(SyringePumpCalibration, self).__init__(pump, balance)

    def dispense(self, volume: float, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int, stock_index:str='A1') -> None:
        """

        :param volume: mL
        :return:
        """
        self.pump.dispense_ml(volume_ml=volume,
                              velocity_ml=pull_flow_rate,
                              dispense_velocity_ml=push_flow_rate,
                              from_port=from_port,
                              to_port=to_port,
                              stock_index = stock_index)

    def prime_lines(self, prime_volume, from_port: int, to_port: int):
        """

        :param prime_volume: mL
        :return:
        """
        self.pump.dispense_ml(prime_volume, from_port=from_port, to_port=to_port)

    def set_up(self, prime_volume: int = 0, from_port: int = 1, to_port: int = 2):
        self.pump.home()
        if prime_volume > 0:
            self.prime_lines(prime_volume, from_port, to_port)


class N9CavroCalibration(SyringePumpCalibration):
    def __init__(self,
                 arm: N9Robot,
                 controller: C9Controller,
                 pump: TecanCavro,
                 ):
        super().__init__(pump, None)
        self.arm: N9Robot = arm
        self.controller: C9Controller = controller
        self.vial_index = None
        self.previous_mass_g = None

    def dispense_weigh_record(self, replicate, target_volume, pull_flow_rate: float, push_flow_rate: float,
                              from_port: int, to_port: int, density, df, csv_output_path,stock_index:str='A1'):
        self.dispense(target_volume, pull_flow_rate, push_flow_rate, from_port, to_port, stock_index=stock_index)
        time.sleep(1)
        mass_after_dispense = self.weigh()
        mass = mass_after_dispense - self.previous_mass_g
        self.previous_mass_g = mass_after_dispense

        volume = round(mass / density, 3)
        data = {
            'Replicate': replicate + 1,
            'Calculated volume (mL)': volume,
            'Target volume (mL)': target_volume,
            'Calculated volume - target volume (mL)': volume - target_volume,
            'Pull flow rate (mL/min)': pull_flow_rate,
            'Push flow rate (mL/min)': push_flow_rate,
            'From port': from_port,
            'To port': to_port,
            'Mass (g)': mass,
            'Density (g/mL)': density,
        }
        df = update_df(df, data)
        save_csv(df, csv_output_path)
        return df

    def test_dispense_weigh_record(self, target_volume, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int, density, calibration, df, csv_output_path, stock_index: str ='A1'):
        initial_mass = self.weigh()
        calibration_adjusted_volume = calibration.calculate_x(target_volume)  # - calibration.intercept
        self.dispense(calibration_adjusted_volume, pull_flow_rate, push_flow_rate, from_port, to_port, stock_index=stock_index)
        time.sleep(1)
        mass_after_dispense = self.weigh()
        mass = mass_after_dispense - self.previous_mass_g
        self.previous_mass_g = mass_after_dispense

        volume_calculated = round(mass / density, 3)
        data = {
            'Calculated volume (mL)': volume_calculated,
            'Target volume calibration adjusted (mL)': calibration_adjusted_volume,
            'Target volume (mL)': target_volume,
            'Calculated volume - target volume (mL)': volume_calculated - target_volume,
            'Calculated volume - target volume calibration adjusted (mL)': volume_calculated - calibration_adjusted_volume,
            'Pull flow rate (mL/min)': pull_flow_rate,
            'Push flow rate (mL/min)': push_flow_rate,
            'From port': from_port,
            'To port': to_port,
            'Mass (g)': mass,
            'Density (g/mL)': density,
        }
        df = update_df(df, data)
        save_csv(df, csv_output_path)
        return df

    def dispense(self, volume: float, pull_flow_rate: float, push_flow_rate: float, from_port: int, to_port: int,stock_index: str = 'A1') -> None:
        """

        :param volume: mL
        :return:
        """
        # make the arm dispense. right before this balance will take an initial weight and after it will take a final weight
        # grab needle, uncap, dose, discard needle
        got_needle, path_to_save_image = reaction.get_needle_with_vision()
        # Eleanor modify
        # todo: change stock_index
        # use solvent from deck
        print('dispense, stock_index =', stock_index)
        reaction.bring_stock_in_line(stock_index=stock_index, sample_volume=volume)
        # inject it in gripper location
        self.arm.move(x=-184.5114013671875, y=-9.762683105468739, z=300, probe=True)
        #assume short needle
        self.arm.move(x=-184.5114013671875, y=-9.762683105468739, z=163.47930908203125, probe=True)
        self.pump.move_absolute_ml(0,velocity_ml=push_flow_rate)

        # move upA1
        self.arm.move(x=-184.5114013671875, y=-9.762683105468739, z=300, probe=True)
        # dump needle
        # dump_needle()
        needle_still_on, path_to_save_image = reaction.dump_needle_with_vision()

    def prime_lines(self, prime_volume, from_port: int, to_port: int):
        """

        :param prime_volume: mL
        :return:
        """
        reaction.wash_dosing_line(wash_volume=prime_volume)

    def set_up(self, prime_volume: int = 0, from_port: int = 1, to_port: int = 2):
        self.pump.home()
        if prime_volume > 0:
            self.prime_lines(prime_volume=prime_volume,from_port=from_port,to_port=to_port) # wash?
        # grab vial , bring to gripper, uncap
        vial_index, _ = deck_consumables.get_clean_vial()
        print(vial_index)
        self.vial_index = vial_index
        self.controller.output(0,False)
        self.controller.output(1,False)
        capping_uncapping.uncap_from_tray(rxn_index=vial_index,from_tray=True)
        station_robot_engagement.park_cap()
        initial_mass = self.weigh()
        self.previous_mass_g = initial_mass

    def weigh(self):
        # weigh and return weight in grams
        station_robot_engagement.vial_from_gripper(to_tray=False, capped=False)
        mass_in_g = station_robot_engagement.weigh_with_quantos()
        station_robot_engagement.vial_to_gripper(from_tray=False, capped=False)
        return mass_in_g

    def clean_up(self):
        station_robot_engagement.pickup_cap()
        capping_uncapping.recap_to_tray(to_tray=True,rxn_index=self.vial_index)




