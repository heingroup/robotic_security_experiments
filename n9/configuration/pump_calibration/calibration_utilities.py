from typing import Dict, List, Union
from pathlib import Path

import pandas as pd
import numpy as np
from scipy import stats


def calculate_x(*ys,
                m,
                b) -> Union[float, List[float]]:
    """
    Given one or more y values, calculate all the corresponding x values

    :param ys:
    :param m:
    :param b:
    :return:
    """
    xs = []
    for y in ys:
        x = (y - b) / m
        xs.append(x)
    if len(xs) == 1:
        return xs[0]
    else:
        return xs


def calculate_y(*xs,
                m,
                b) -> Union[float, List[float]]:
    """
    Given one or more x values, calculate all the corresponding y values

    :param xs:
    :param m:
    :param b:
    :return:
    """
    ys = []
    for x in xs:
        y = (m * x) + b
        ys.append(y)
    if len(ys) == 1:
        return ys[0]
    else:
        return ys


class LinearRegression:
    """
    Linear regression for calibration

    The slope is numerator/denominator = y/x
    """

    def __init__(self):
        self._data: Dict[float, float] = {}

    def add(self,
            data: Dict[float, float],
            ) -> Dict[float, float]:
        """
        Add data

        :param dict, data: where the key is the denominator (x) value, and value is the numerator (y)
        :return:
        """
        for key in list(data.keys()):
            self.data[key] = data[key]
        return self.data

    def remove(self,
               x: float,
               ) -> float:
        y = self.data.pop(x)
        return y

    def calculate_x(self,
                    *ys,
                    ) -> Union[float, List[float]]:
        """
        Given one or more y values, calculate all the corresponding x values using the linear regression from the data
        :param ys:
        :return:
        """
        m = self.slope
        b = self.intercept
        return calculate_x(*ys, m=m, b=b)

    def calculate_y(self,
                    *xs,
                    ) -> Union[float, List[float]]:
        """
        Given one or more x values, calculate all the corresponding y values using the linear regression from the data
        :param xs:
        :return:
        """
        m = self.slope
        b = self.intercept
        return calculate_y(*xs, m=m, b=b)

    @property
    def data(self) -> Dict[float, float]:
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @property
    def y(self) -> List[float]:
        y = list(self.data.values())
        return y

    @property
    def x(self) -> List[float]:
        x = list(self.data.keys())
        return x

    @property
    def slope(self) -> float:
        x = self.x
        y = self.y
        slope, _, _, _, _ = stats.linregress(x, y)
        return slope

    @property
    def intercept(self) -> float:
        x = self.x
        y = self.y
        _, intercept, _, _, _ = stats.linregress(x, y)
        return intercept

    @property
    def r_value(self) -> float:
        # r value is the correlation coefficient
        x = self.x
        y = self.y
        _, _, r_value, _, _ = stats.linregress(x, y)
        return r_value

    @property
    def std_error(self) -> float:
        x = self.x
        y = self.y
        _, _, _, _, std_err = stats.linregress(x, y)
        return std_err


def save_csv(df: pd.DataFrame, file_path: Path, accept_permission_error: bool = True):
    if accept_permission_error:
        try:
            df.to_csv(file_path, sep=',', index=False, mode='w')
        except PermissionError as e:
            print(f'failed to save {file_path.absolute()}')
    else:
        df.to_csv(file_path, sep=',', index=False, mode='w')


def update_df(df: pd.DataFrame, data: Dict):
    df = df.append(data, ignore_index=True)
    return df


def subset(data: List, subset_size: int) -> List[List]:
    """
    Return a nested list (a list of lists). The inner lists are subgroups of the data, where each subset is of
    length of the sample size

    Example:
    data =  [1, 2, 3, 4, 5, 6]
    sample_size = 2
    return: [[1, 2], [3, 4], [5, 6]]

    :param List[], data:
    :param int, subset_size: size of the subset of data to pass through to get a measurement
    :return:  List[List[]]
    """
    m = len(data)
    subgroups = []
    for index in range(0, m, subset_size):
        subset = data[index:index + subset_size]
        subgroups.append(subset)
    return subgroups


def subset_means(data: List[float], subset_size: int) -> List[float]:
    """
    For some data set, split it into subgroups based on the sample size, and return a list consisting of the
    mean of each subset

    :param List[float], data:
    :param int, subset_size: size of the subset of data to pass through to get a measurement
    :return:
    """
    means: List[float] = []
    subgroups: List[List[float]] = subset(data=data, subset_size=subset_size)
    for subgroup in subgroups:
        subgroup_mean = np.mean(subgroup)
        means.append(subgroup_mean)
    return means