from pathlib import Path
from datetime import datetime
from n9.configuration.pump_calibration.syringe_pump_calibration import N9CavroCalibration, get_slope_intercept_r2
from n9.configuration.pump_calibration.calibration_utilities import calculate_x
from n9.configuration import deck, deck_consumables
from n9.experiments.capping_uncapping import recap_to_tray
from n9.experiments.reaction import dump_needle_with_vision
from n9.experiments.station_robot_engagement import pickup_cap, vial_to_tray, vial_from_gripper

if __name__ == '__main__':
    arm = deck.n9
    controller = deck.c9
    pump = deck.dosing_pump
    calibration_maker = N9CavroCalibration(arm, controller, pump)

    #TODO: MTBE, DMSO, ethylAcetate, hexane
    solvent_name = 'MTBE'
    density, tray, stock_index, capped = deck_consumables.get_stock_info(solvent_name)
    volumes = [0.1, 0.2, 0.3, 0.4, 0.5]
    # density = 0.655 # hexane
    replicates = 1
    prime_volume = 2  # mL
    pull_flow_rate = deck.SOLVENT_DRAW_RATE  # mL/min
    push_flow_rate = deck.SOLVENT_PUSH_RATE  # mL/min
    from_port = deck.SOLVENT_PORT
    to_port = deck.SAMPLE_PORT


    date = datetime.now().strftime('%y-%m-%d %H-%M')

    calibration_folder = Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\pump_calibration')
    calibration_csv_output_path = calibration_folder.joinpath(f"Volume calibration {date} {solvent_name}.csv")
    test_calibration_csv_output_path = calibration_folder.joinpath(f"Volume test calibration {date} {solvent_name}.csv")

    # todo: uncomment to run
    calibration_maker.make_flow_rate_calibration_curve(volumes,
                                                       pull_flow_rate,
                                                       push_flow_rate,
                                                       from_port,
                                                       to_port,
                                                       density, replicates,
                                                       calibration_csv_output_path,
                                                       prime_volume,
                                                       stock_index =stock_index)
    calibration_maker.clean_up()
    slope, intercept, r2 = get_slope_intercept_r2(calibration_csv_output_path)
    print(f'slope: {slope}, intercept: {intercept}, r2: {r2}')
    # todo: comment to run
    # slope = 0.9709999999999998
    # intercept= -0.015099999999999891
    # r2= 0.9998017026005591

    # ---------------------------------------------------------------------------
    # # to load a calibration instead of making on
    # calibration_csv_output_path = Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\pump_calibration\Volume calibration 21-02-09 14-54.csv')
    # slope, intercept, r2 = get_slope_intercept_r2(calibration_csv_output_path)
    # print(f'slope: {slope}, intercept: {intercept}, r2: {r2}')
    # ---------------------------------------------------------------------------

    test_volumes = [0.1, 0.2, 0.3, 0.4, 0.5]
    calibration_maker.test_calibration(test_volumes,
                                       pull_flow_rate,
                                       push_flow_rate,
                                       from_port,
                                       to_port,
                                       calibration_csv_output_path,
                                       test_calibration_csv_output_path,
                                       prime_volume,
                                       stock_index = stock_index)
    calibration_maker.clean_up()

    print('done')


    # # # how to use calibration file
    # slope, intercept, r2 = get_slope_intercept_r2(Path(r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\configuration\pump_calibration\Volume calibration 21-02-09 14-54.csv'))
    # target_volume = 0.5  # mL
    # calibration_adjusted_volume = calculate_x(target_volume, m=slope, b=intercept)
    # # then use the calibration adjusted volume when telling the pump to dispense for on deck solvent
