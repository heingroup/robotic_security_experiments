import csv
import os
import re
import time
import logging
from datetime import datetime, timedelta
import threading
import shutil
import numpy as np
from typing import List

# from n9.experiments.pass_vial_gateway import kinova_deck_manager

from n9.configuration import deck, deck_consumables
from n9.configuration.deck_consumables import get_stock_info
from ur.configuration.ur_deck import quantos_guns
from n9.configuration.deck import turbidity_monitor_base_file_path_json, \
    annotated_monitor_turbidity_roi_path
from n9.configuration.deck import turbidity_monitor_base_file_path_json, \
    annotated_monitor_turbidity_roi_path
from north_robots.components import GridTray, Location
from n9.experiments.reaction import uncap_needle, move_for_needle_check, dump_needle, wash_dosing_line, \
    bring_stock_in_line, get_needle_with_vision, dump_needle_with_vision
from n9.experiments.capping_uncapping import uncap_from_tray, recap_to_tray
from n9.experiments.station_robot_engagement import vial_from_gripper, vial_to_gripper, \
    zero_quantos, weigh_with_quantos, dose_with_quantos, vial_from_quantos, vial_to_vision, vial_to_quantos, \
    vial_from_vision, vial_from_tray, vial_to_tray, park_cap, pickup_cap, open_vision_station, close_vision_station
from pathlib import Path
import pandas as pd
import cv2
from heinsight.heinsight_utilities.temporal_data import TemporalData
from heinsight.vision_utilities.camera import Camera
from heinsight.vision.turbidity import TurbidityMonitor
from heinsight.vision_utilities.video_utilities import folder_of_images_to_video
from hein_utilities.datetime_utilities import datetimeManager
from hein_utilities.slack_integration.parsing import ignore_bot_users
from hein_utilities.slack_integration.bots import WebClientOverride
from hein_utilities.slack_integration.slack_managers import RTMControlManager
from hein_utilities.runnable import Runnable
from typing import Dict
from heinsight.vision_utilities.roi import ROI
from n9.experiments import solubility_vision_slack_manager_info
from ur.components.vision_angle_measurement.measurement import _dot_center
from n9.experiments.solubility_liquid_addition_graph import graph_with_liquid_addition

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s :: %(levelname)s :: %(message)s',
                    filename=r'C:\Users\User\PycharmProjects\automated_solubility_h1\n9\experiments\solubility_log'
                             r'\solubility.log')


class ExperimentInformation:
    experiment_number = 1

    def __init__(self,
                 vial_index: str,
                 solvent: deck_consumables.Chemical,
                 solid: str,
                 target_initial_volume: float,
                 target_solid_mass: float,  # g
                 solvent_addition_volume: float,  # mL
                 parent_path: Path,  # folder to crease a subfolder to keep everything about the experiment in
                 base_experiment_name: str = 'exp',
                 ):
        self.vial_index = vial_index
        self.solvent = solvent
        self.solid = solid
        self._solubility: float = None  # g/mL
        self.target_initial_volume = target_initial_volume  # mL, initial volume of solvent to add in the code
        self.target_solid_mass = target_solid_mass  # g, solid mass to add in the code; might not be actual mass dispensed
        self.solid_mass: float = None  # g, measured mass of solid added
        self.initial_mass: float = None  # g, measured mass with quantos, vial with stir bar and solid added
        self.initial_mass_zetascale: float = None
        self.empty_vial_mass: float = None  # g, measured mass with quantos, vial with stir bar, no solid added
        self.empty_vial_mass_zetascale: float = None
        self.final_mass: float = None  # g, measured mass with quantos, vial after solubility experiment done
        self.final_mass_zetascale: float = None
        self.target_volume: float = target_initial_volume  # mL, volume of total solvent added in the code; might not be
        # actual volume dispensed
        self._volume: float = None
        self.maximum_volume = 1.2  # mL
        self.solvent_addition_volume = solvent_addition_volume  # mL
        self.stir_rate = None  # rpm
        self.solubility_accuracy_note: str = ''  # put here either "", "solubility should be higher", or "solubility should be lower". If
        # too much solvent was added from just the initial volume then the solubility is likely to be higher than what
        # was actually found, in this case set accuracy to 'solubility should be higher'. In another case, if the maximum safe
        # volume was added and the dissolved state was still not reached, set to 'solubility should be lower'

        self.dissolved = False
        self.vision_volume = 0  # mL
        self.start_time: datetime = None
        self.end_time: datetime = None

        self.base_experiment_name = base_experiment_name
        self.experiment_number: int = None
        self.experiment_name = f'{self.base_experiment_name} {self.experiment_number} - {self.solvent.name}'
        self.folder: Path = self.make_experiment_folder(parent_path)

    @property
    def volume(self) -> float:
        """mL, actual volume added calculated from mass and density"""
        solid_mass = self.solid_mass
        initial_mass = self.initial_mass
        final_mass = self.final_mass
        if solid_mass is None or initial_mass is None or final_mass is None:
            return 0
        mass_solvent = final_mass - initial_mass
        solvent_density = self.solvent.density
        volume_solvent = mass_solvent / solvent_density
        self._volume = volume_solvent
        return self._volume

    @property
    def solubility(self) -> float:  # g/mL
        solid_mass = self.solid_mass
        volume_solvent = self.volume
        if solid_mass is None or volume_solvent is None or volume_solvent == 0:
            self._solubility = None
            return self._solubility
        solubility = solid_mass / volume_solvent
        self._solubility = solubility
        return self._solubility

    @property
    def solid_percent_error(self):
        theoretical = self.target_solid_mass
        actual = self.solid_mass
        if theoretical is None or actual is None:
            return None
        p_error = abs((theoretical - actual) / actual) * 100
        return p_error

    @property
    def solvent_percent_error(self):
        theoretical = self.target_volume
        actual = self.volume
        if theoretical is None or actual is None or actual == 0:
            return None
        p_error = abs((theoretical - actual) / actual) * 100
        return p_error

    def make_experiment_folder(self, parent_path: Path) -> Path:
        self.experiment_number = ExperimentInformation.experiment_number
        ExperimentInformation.experiment_number += 1
        self.experiment_name = f'{self.base_experiment_name}_{self.experiment_number}'
        experiment_folder = Path.joinpath(parent_path, self.experiment_name)
        while True:
            if experiment_folder.exists():
                self.experiment_number = ExperimentInformation.experiment_number
                ExperimentInformation.experiment_number += 1
                self.experiment_name = f'{self.base_experiment_name}_{self.experiment_number}'
                experiment_folder = Path.joinpath(parent_path, self.experiment_name)
            else:
                Path.mkdir(experiment_folder)
                break
        return experiment_folder

    def can_add_more_solvent(self) -> bool:
        """Based on how much solvent is in the vial, whether the max volume is reached
        """
        if round(self.target_volume + self.solvent_addition_volume, 3) <= round(self.maximum_volume, 3):
            return True
        else:
            return False


class Results:
    datetime_format = '%I:%M %p, %d %b %Y'  # hour:min am/pm, day month year

    def __init__(self,
                 ):
        self.results = self.initialize_results_df()

    def initialize_results_df(self) -> pd.DataFrame:
        columns = ['Experiment number',
                   'Experiment name',
                   'Vial index',
                   'Dissolved',
                   'Solvent',
                   'Solid',
                   'Solubility (g/mL)',
                   'Mass solid (g)',
                   'Solid dispense percent error (%)',
                   'Volume solvent (mL)',
                   'Solvent dispense percent error (%)',
                   'Vision volume (mL)',
                   'Initial volume solvent (mL)',
                   'Stir rate (rpm)',
                   'Start time',
                   'End time',
                   'Assumed mass solid (g)',
                   'Assumed total volume solvent (mL)',
                   'Assumed solubility (g/mL)',
                   'Assumed volume solvent added (mL)',
                   'Empty vial weight (g)',
                   'Vial with solution weight (g)',
                   'Turbidity algorithm parameters',
                   ]
        results_df = pd.DataFrame(columns=columns)
        return results_df

    def add_data(self,
                 experiment_information: ExperimentInformation,
                 tm_parameters: Dict,
                 ):
        ei: ExperimentInformation = experiment_information
        tm_parameters_str = ''
        for (key, value) in list(tm_parameters.items()):
            key = str(key)
            value = str(value)
            tm_parameters_str += f'{key}: {value}\n'
        if ei.start_time is not None:
            start_time = ei.start_time.strftime(self.datetime_format)
        else:
            start_time = None
        if ei.end_time is not None:
            end_time = ei.end_time.strftime(self.datetime_format)
        else:
            end_time = None

        assumed_solubility = ei.target_solid_mass / ei.target_volume
        assumed_solvent_added = ei.target_volume - ei.target_initial_volume

        add_data = {'Experiment number': ei.experiment_number,
                    'Experiment name': ei.experiment_name,
                    'Vial index': ei.vial_index,
                    'Dissolved': ei.dissolved,
                    'Solubility accuracy notes': ei.solubility_accuracy_note,
                    'Solvent': ei.solvent.name,
                    'Solid': experiment_information.solid,
                    'Solubility (g/mL)': ei.solubility,
                    'Mass solid (g)': ei.solid_mass,
                    'Solid dispense percent error (%)': ei.solid_percent_error,
                    'Volume solvent (mL)': ei.volume,
                    'Solvent dispense percent error (%)': ei.solvent_percent_error,
                    'Vision volume (mL)': ei.vision_volume,
                    'Initial volume solvent (mL)': ei.target_initial_volume,
                    'Stir rate (rpm)': ei.stir_rate,
                    'Start time': start_time,
                    'End time': end_time,
                    'Assumed mass solid (g)': ei.target_solid_mass,
                    'Assumed total volume solvent (mL)': ei.target_volume,
                    'Assumed solubility (g/mL)': assumed_solubility,
                    'Assumed volume solvent added (mL)': assumed_solvent_added,
                    'vial weight with solid (g)': ei.initial_mass,
                    'vial weight with solid with zeta scale(g)': ei.initial_mass_zetascale,
                    'empty vial weight (g)': ei.empty_vial_mass,
                    'empty vial weight with zeta scale (g)': ei.empty_vial_mass_zetascale,
                    'Vial with solution weight (g)': ei.final_mass,
                    'Vial with solution weight with zeta scale(g)': ei.final_mass_zetascale,
                    'Turbidity algorithm parameters': tm_parameters_str,
                    }
        self.results = self.results.append(add_data, ignore_index=True)
        self.results: pd.DataFrame = self.results.drop_duplicates(subset='Experiment number',
                                                                  keep='last')
        return self.results

    def save(self, path: Path):
        path = str(path)
        if path[-4:] != '.csv':
            path += '.csv'
        while True:
            try:
                self.results.to_csv(path, sep=',', index=False, mode='w')
                break
            except PermissionError as e:
                time.sleep(1)
        return self.results


class TurbidityMonitorRunnable(Runnable):
    def __init__(self,
                 tm: TurbidityMonitor,
                 graph_path: str,
                 camera: Camera,
                 slack_manager,
                 experiment_information: ExperimentInformation,
                 ):
        Runnable.__init__(self, logger=logger)
        self.camera = camera
        self.tm = tm
        self.slack_manager = slack_manager
        self.graph_path = graph_path
        self.experiment_information = experiment_information
        self.time_out_ref: datetime = None
        self.time_out_mins = 10
        self.time_between_measurements = int(60 / tm_n_measurements_per_min)  # seconds between measurements

    def run(self):
        while self.running:
            time.sleep(self.time_between_measurements)  # seconds between measurements

            if pause_monitoring_bool:
                continue
            images = self.camera.take_photos(n=tm_n_images_per_measurement, save_photo=True)
            self.tm.add_measurement(*images)
            self.tm.save_data()
            self.save_current_graph()
            now_time = datetime.now()

            if way_below_dissolved(tm=self.tm):
                slack_manager.post_slack_message('Seems like the turbidity is way below the dissolved reference, '
                                                 'something might be wrong')
                pause_addition_monitoring()
                slack_manager.post_slack_message('If nothing is wrong, you can choose to resume by sending "resume", or'
                                                 'by sending both "reusme addition" and "resume monitoring"')

            if pause_addition_bool:
                continue

            # if > time out time since last addition
            if (now_time - self.time_out_ref).seconds > 60 * self.time_out_mins:
                if self.experiment_information.can_add_more_solvent():
                    # maximum solvent hasnt been added in yet, then add more solvent
                    added_solvent = add_solvent(solvent=solvent,
                                                ei=self.experiment_information)
                    if added_solvent is True:
                        try:
                            message = f'{self.time_out_mins} minutes passed, so I will dose in another' \
                                      f' {ei.solvent_addition_volume} mL anyways.\n' \
                                      f'Total volume: {ei.target_volume} mL'
                            slack_manager.post_slack_message(msg=message)
                        except Exception as e:
                            logger.error(e)
                        self.time_out_ref = now_time
                    else:
                        time.sleep(45)
                else:
                    message = f'{self.time_out_mins} minutes passed, but I reached the max volume and cannot add ' \
                              f'any more solvent'
                    slack_manager.post_slack_message(msg=message)
                    self.slack_current_graph()
                    self.slack_current_image()
                    clean_up_vial_fn()
                    time.sleep(1)
                    start_next_vial_fn()
                    time.sleep(60)

            if self.tm.state_changed_to_dissolved() or seems_dissolved(tm=self.tm):
                minutes_to_stir = 5
                minutes_to_check = 5
                message = f'I think the state has changed to dissolved, but to be sure, I will increase the stir rate ' \
                          f'to 900 for {minutes_to_stir} minutes, then I will check the state again, monitoring for {minutes_to_check} minutes'
                slack_manager.post_slack_message(msg=message)
                self.slack_current_graph()
                self.slack_current_image()
                deck.mini_heater_stirrer.target_stir_rate = 900
                time.sleep(minutes_to_stir * 60)
                deck.mini_heater_stirrer.target_stir_rate = self.experiment_information.stir_rate
                # then monitor for to check if it has dissolved
                start_dissolve_check_time = datetime.now()
                current_dissolve_check_time = datetime.now()
                while (current_dissolve_check_time - start_dissolve_check_time).seconds < (minutes_to_check * 60):
                    time.sleep(self.time_between_measurements / 2)
                    images = self.camera.take_photos(n=tm_n_images_per_measurement, save_photo=True)
                    self.tm.add_measurement(*images)
                    self.tm.save_data()
                    self.save_current_graph()
                    current_dissolve_check_time = datetime.now()
                if tm.state == TurbidityMonitor.dissolved_state or seems_dissolved(tm=self.tm):
                    message = f'I think the state has really changed to dissolved after 5 minutes of high speed stirring'
                    slack_manager.post_slack_message(msg=message)
                    self.slack_current_graph()
                    self.slack_current_image()
                    self.experiment_information.dissolved = True
                    time.sleep(60)
                else:
                    message = f'I dont think the state is acutally dissolved after all, going to continue monitoring turbidity'
                    slack_manager.post_slack_message(msg=message)
                    self.slack_current_graph()
                    self.slack_current_image()
                    now_time = datetime.now()
            elif tm.state_changed_to_stable() or (tm.check_state(n=int(tm.n * 1.3)) == tm.stable_state):
                # if state changed to stable, or based on tm.n*1.3 number of previous measurements if it looks
                # like the state is stable
                message = 'I think the state is steady.\n'
                self.time_out_ref = now_time
                if self.experiment_information.can_add_more_solvent():
                    added_solvent = add_solvent(solvent=solvent,
                                                ei=self.experiment_information)
                    if added_solvent:
                        message += f'I added another {ei.solvent_addition_volume} mL.\n' \
                                   f'Total volume: {ei.target_volume} mL'
                        slack_manager.post_slack_message(msg=message)
                    else:
                        time.sleep(45)
                else:
                    message = 'But I reached the max volume and cannot add any more solvent'
                    slack_manager.post_slack_message(msg=message)
                    self.slack_current_graph()
                    self.slack_current_image()
                    clean_up_vial_fn()
                    time.sleep(1)
                    start_next_vial_fn()
                    time.sleep(60)
            elif tm.state_changed_to_unstable():
                pass

    def start_background_monitoring(self):
        self.time_out_ref = datetime.now()
        self.start()

    def stop_background_monitoring(self):
        self.stop()

    def slack_current_image(self):
        try:
            last_image = camera.last_frame
            last_image_path = str(camera.save_folder.joinpath('last_image.jpg'))
            cv2.imwrite(last_image_path, last_image)
            time.sleep(1)
            self.slack_manager.post_slack_file(filepath=last_image_path,
                                               title='The last image taken',
                                               comment='The last image taken',
                                               )
        except Exception as e:
            print(e)

    def save_current_graph(self):
        try:
            graph_with_liquid_addition(turbidity_data_path=Path(self.tm._turbidity_monitor_data_json_save_path),
                                       liquid_addition_path=solvent_addition_data.csv_path,
                                       save_path=Path(self.graph_path),
                                       tm_parameters=tm_parameters,
                                       )
        except Exception as e:
            print('error saving current graph')
            print(e)

    def slack_current_graph(self):
        try:
            self.save_current_graph()
            time.sleep(1)
            slack_manager.post_slack_file(filepath=self.graph_path,
                                          title='Turbidity vs. time',
                                          comment='Turbidity vs. time graph, the green regions are stable '
                                                  'regions'
                                          )
        except Exception as e:
            print(e)


def clean_up_vial_fn():
    global clean_up_vial
    slack_manager.post_slack_message('I will clean up this vial if it needs to be cleaned but not start the next vial')
    clean_up_vial = True


def start_next_vial_fn():
    global start_next_vial
    start_next_vial = True
    slack_manager.post_slack_message('I will start the next vial after cleaning up the current vial if there is one')


def dont_start_next_fn():
    global start_next_vial
    start_next_vial = False
    slack_manager.post_slack_message('I will not start vial until you tell me to')


def change_stir_rate(new_stir_rate):
    global stir_rate
    ei.stir_rate = new_stir_rate
    stir_rate = new_stir_rate
    deck.mini_heater_stirrer.target_stir_rate = stir_rate
    slack_manager.post_slack_message(f'Changing the stir rate to {stir_rate}')


def change_solid_weight(new_solid_weight):
    global solid_weight
    solid_weight = new_solid_weight
    slack_manager.post_slack_message(f'Changing the solid mass to {new_solid_weight}')


def change_initial_volume(new_initial_volume):
    global initial_volume
    initial_volume = new_initial_volume
    slack_manager.post_slack_message(f'Changing the initialv {solvent.name} volume to {new_initial_volume}')


def slack_video():
    folder_of_images_to_video(str(camera_images_folder),
                              video_path,
                              fps=fps,
                              display_image_name=True)
    slack_manager.post_slack_file(video_path, 'Video of this run', 'Video of this run')


def set_dissolved_reference(new_dissolved_reference: float):
    if tm is not None:
        tm.turbidity_dissolved_reference = new_dissolved_reference

        temp_vision_selection_path = str(experiment_folder.joinpath('temp_vision_selection'))
        new_vision_selection_tm = TurbidityMonitor(turbidity_monitor_data_save_path=temp_vision_selection_path)
        new_vision_selection_tm.load_data(json_path=vision_selection_json_path)
        new_vision_selection_tm.turbidity_dissolved_reference = new_dissolved_reference
        new_vision_selection_tm._turbidity_monitor_data_save_path = vision_selection_json_path
        new_vision_selection_tm._turbidity_monitor_data_json_save_path = vision_selection_json_path
        new_vision_selection_tm.save_json_data()

        slack_manager.post_slack_message(f'Changing the dissolved reference to {new_dissolved_reference}')


def add_solvent(solvent: deck_consumables.ChemicalContainer,
                ei: ExperimentInformation) -> bool:
    if enough_solvent(ei.solvent_addition_volume) is False:
        slack_manager.post_slack_message(f'Only {solvent.current_volume} mL of {solvent.name} left, not enough to '
                                         f'dispense {ei.solvent_addition_volume} without going lower than the safe '
                                         f'volume of {solvent.safe_volume} mL in the stock vial. I will clean up '
                                         f'and start the next vial')
        clean_up_vial_fn()
        time.sleep(1)
        start_next_vial_fn()
        time.sleep(15)
        return False
    # return bool of if solvent was added
    global can_safely_add_solvent
    if can_safely_add_solvent is True:
        can_safely_add_solvent = False
        ei.target_volume += ei.solvent_addition_volume
        ei.target_volume = round(ei.target_volume, 3)
        update_solvent_tracking_data(solvent_name=solvent.name, volume_added=ei.solvent_addition_volume,
                                     total_volume=ei.target_volume)
        # N9 grabs next dose and adds it
        # get needle out of vision_needle_check vial
        location = deck.vision_grid_needle.locations[vision_station_monitor_index]  # todo double check is correct
        safe_location = location.copy(z=300)
        pierce_location = location.copy(z=187)
        approach_location = location.copy(z=200)
        deck.n9.move_to_location(safe_location, gripper=109, probe=True)
        # dump_needle()
        # deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=308, probe=True)
        ## # take needle
        # deck.needle_tray.pickup()
        #### # uncap needle
        # uncap_needle()
        # deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)
        # check if there is needle on
        # move_for_needle_check()
        # needle_on_probe, path_to_save_image = deck.vision_needle_check()
        # if needle_on_probe == False:
        #    global MAIN_PAUSE
        #    got_needle, path_to_save_image = get_needle_with_vision()
        #    if got_needle == False:
        #        MAIN_PAUSE = True
        #        needle_not_on_message()
        #        while MAIN_PAUSE:
        #            time.sleep(5)
        # else:
        # deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=308, probe=True)

        # grab the dose from stock
        bring_stock_in_line(stock_index=solvent.index, sample_volume=ei.solvent_addition_volume)
        # needle back to vision_needle_check
        deck.n9.move_to_location(safe_location, gripper=109, probe=True)
        # needle in
        deck.n9.move_to_location(approach_location, gripper=109, probe=True)
        deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
        deck.dosing_pump.move_absolute_ml(0, 3)
        solvent.current_volume -= ei.solvent_addition_volume
        deck_consumables.update_stock_file(solvent.name, 'current volume', solvent.current_volume)
        can_safely_add_solvent = True
        save_results()
        return True
    else:
        message = 'Cannot safely add solvent now, try again in a bit'
        slack_manager.post_slack_message(msg=message)
        return False


def solvent_didnt_change_message():
    main_pause_message()
    name = solvent.name
    slack_manager.post_slack_message(f'I tried adding {ei.solvent_addition_volume} mL of {name} but I dont '
                                     f'think the solvent level actually changed. You can use the "add solvent", "deck '
                                     f'image", and "vial image" commands to help troubleshoot. I think there is '
                                     f'{solvent.current_volume} mL left of {name} left in the stock vial I am using. '
                                     f'If you think I have run out, send the "use next solvent", "clean up vial", '
                                     f'"start next vial", and "main resume" commands in that order')


def use_next_solvent():
    slack_manager.post_slack_message(f'Current solvent: {test_info.current_solvent}')
    next_solvent = test_info.choose_next_solvent()
    if next_solvent is None:
        next_solvent = 'no next solvent; no more experiments will be run after the current one ends'
    slack_manager.post_slack_message(f'Solvent to use in the next run: {next_solvent}')


def use_next_solid():
    slack_manager.post_slack_message(f'Current solid: {test_info.current_solid}')
    next_solid = test_info.choose_next_solid()
    if next_solid is None:
        next_solid = 'no next solid; no more experiments will be run after the current one ends'
    slack_manager.post_slack_message(f'Solid to use in the next run: {next_solid}')


def enough_solvent(add_volume) -> bool:
    if solvent.current_volume - add_volume >= solvent.safe_volume:
        return True
    else:
        return False


def set_turbidity_monitor_parameters(tm):
    tm.n = tm_n
    tm.std_max = tm_std_max
    tm.sem_max = tm_sem_max
    tm.upper_limit = tm_upper_limit
    tm.lower_limit = tm_lower_limit
    tm.range_limit = tm_range_limit
    global tm_parameters
    tm_parameters = {
        'tm_n_images_per_measurement': tm_n_images_per_measurement,
        'tm_n_measurements_per_min': tm_n_measurements_per_min,
        'tm_n_minutes': tm_n_minutes,
        'tm_n': tm_n,
        'tm_std_max': tm_std_max,
        'tm_sem_max': tm_sem_max,
        'tm_upper_limit': tm_upper_limit,
        'tm_lower_limit': tm_lower_limit,
        'tm_range_limit': tm_range_limit,
        'tm_n_images_for_dissolved_reference': tm_n_images_for_dissolved_reference,
    }
    return tm


def pause_addition_monitoring():
    pause_addition()
    pause_monitoring()


def resume_addition_monitoring():
    resume_addition()
    resume_monitoring()


def pause_addition():
    global pause_addition_bool
    pause_addition_bool = True
    slack_manager.post_slack_message('Pausing solvent addition')


def resume_addition():
    global pause_addition_bool
    pause_addition_bool = False
    slack_manager.post_slack_message('Resuming solvent addition')


def pause_monitoring():
    global pause_monitoring_bool
    pause_monitoring_bool = True
    slack_manager.post_slack_message('Pausing turbidity monitoring')


def resume_monitoring():
    global pause_monitoring_bool
    pause_monitoring_bool = False
    slack_manager.post_slack_message('Resuming turbidity monitoring')


bot_token, bot_name, channel_name, channel_id = solubility_vision_slack_manager_info.bot_token, \
                                                solubility_vision_slack_manager_info.bot_name, \
                                                solubility_vision_slack_manager_info.channel_name, \
                                                solubility_vision_slack_manager_info.channel_id

slack_manager = RTMControlManager(
    token=bot_token,
    channel_name=channel_name,
    channel_id=channel_id,
)
logger.addHandler(slack_manager)


# add the event handler function
@slack_manager.run_on(event='message')
@ignore_bot_users
def catch_message(**payload):
    message = payload['data']
    text = str(message.get('text'))
    web_client = payload['web_client']
    message_channel_id = str(message.get('channel'))
    if slack_manager.channel_id is not None:
        if slack_manager.channel_id != message_channel_id:
            return
    try:
        # current image catch
        if re.search('vial image', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    if camera is None:
                        slack_manager.post_slack_message('Unable to do that now, try again in a while')
                        return
                    last_image = camera.take_photo(save_photo=False)
                    last_image_path = str(camera.save_folder.joinpath('last_image.jpg'))
                    cv2.imwrite(last_image_path, last_image)
                    time.sleep(1)
                    slack_manager.post_slack_file(filepath=last_image_path,
                                                  title='Vial camera',
                                                  comment='Vial camera',
                                                  )
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.search('deck image', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    slack_deck_image()
                except Exception as e:
                    logger.info(f'encountered error {e}')

        # turbidity data graph catch
        elif re.search('graph', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    if tm is None:
                        slack_manager.post_slack_message('Unable to do that now, try again in a while')
                        return
                    background_monitor.slack_current_graph()
                except Exception as e:
                    logger.info(f'encountered error {e}')

        # change stir rate catch
        elif re.match('change stir rate [\\d+]', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                numbers_in_slack_message = [float(s) for s in text.split() if s.isdigit()]
                # there should only be one number in the message to know what to change the stir rate to

                if len(numbers_in_slack_message) != 1:
                    slack_manager.post_slack_message(
                        msg=f"You must specify one number in the message to indicate what to change the stir rate to")
                    return
                else:
                    new_stir_rate = numbers_in_slack_message[0]
                    change_stir_rate(new_stir_rate)

        elif re.match('turbidity video', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    slack_video()
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.match('set dissolved reference [\\d+]', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                numbers_in_slack_message = [float(s) for s in text.split() if s.isdigit()]
                # there should only be one number in the message to know what to change the dissolved reference
                if len(numbers_in_slack_message) != 1:
                    slack_manager.post_slack_message(
                        msg=f"You must specify one number in the message to indicate what to change the dissolved reference to to")
                    return
                else:
                    new_dissolved_reference = numbers_in_slack_message[0]
                    set_dissolved_reference(new_dissolved_reference)

        elif re.match('change solid weight \d*', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                numbers_in_slack_message = [float(s) for s in text.split() if s.isdigit()]
                if len(numbers_in_slack_message) != 1:
                    slack_manager.post_slack_message(msg=f"You must specify one number in the message to indicate "
                                                         f"what to change the solid mass to")
                    return
                else:
                    new_solid_weight_mg = numbers_in_slack_message[0]
                    new_solid_weight_g = new_solid_weight_mg / 1000
                    change_solid_weight(new_solid_weight_g)

        elif re.match('change initial volume [\\d+]', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                slack_manager.post_slack_message('currently not working')
                #
                # numbers_in_slack_message = [float(s) for s in text.split() if s.isdigit()]
                # # there should only be one number in the message to know what to change the stir rate to
                #
                # if len(numbers_in_slack_message) != 1:
                #     slack_manager.post_slack_message(msg=f"You must specify one number in the message to"
                #                                          f" indicate what to change the inital {solvent.name} "
                #                                          f"volume to")
                #     return
                # else:
                #     new_initial_volume = numbers_in_slack_message[0]
                #     change_initial_volume(new_initial_volume)

        elif re.match('clean up vial', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                clean_up_vial_fn()

        elif re.match('start next vial', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                start_next_vial_fn()

        elif re.match('dont start next vial', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                dont_start_next_fn()

        # roi image catch
        elif re.search('roi', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    if tm is None or camera is None:
                        slack_manager.post_slack_message('Unable to do that now, try again in a while')
                        return
                    camera.take_photo(save_photo=False)
                    last_image = camera.last_frame
                    roi_image = tm.draw_monitor_region(last_image)
                    roi_image_path = str(camera.save_folder.joinpath('roi.jpg'))
                    cv2.imwrite(roi_image_path, roi_image)
                    slack_manager.post_slack_file(filepath=roi_image_path,
                                                  title='The ROI',
                                                  comment='The ROI',
                                                  )
                    time.sleep(1)
                    os.remove(roi_image_path)
                except Exception as e:
                    logger.info(f'encountered error {e}')

        # add solvent catch
        elif re.search('add solvent', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    added = add_solvent(ei=ei, solvent=solvent)
                    if added:
                        message = f'Added {ei.solvent_addition_volume} mL of {solvent.name}.\n' \
                                  f'Total volume: {ei.target_volume} mL'
                        slack_manager.post_slack_message(msg=message)

                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.search('use next solvent', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    use_next_solvent()
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.search('use next solid', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    use_next_solid()
                except Exception as e:
                    logger.info(f'encountered error {e}')

        # elif re.fullmatch('pause', text, re.IGNORECASE) is not None:
        #     with WebClientOverride(slack_manager, web_client):
        #         pause_addition_monitoring()
        #
        elif re.fullmatch('resume', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                resume_addition_monitoring()

        elif re.fullmatch('pause solvent addition', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                pause_addition()

        elif re.fullmatch('resume solvent addition', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                resume_addition()

        elif re.fullmatch('pause monitoring', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                pause_monitoring()

        elif re.fullmatch('resume monitoring', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                resume_monitoring()

        elif re.fullmatch('get needle', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    slack_manager.post_slack_message(f'Getting another needle now, I will send a photo back after I '
                                                     f'retrieve one')
                    got_needle, path_to_save_image = get_needle_with_vision()
                    slack_manager.post_slack_file(filepath=path_to_save_image,
                                                  title='Deck camera',
                                                  comment='Deck camera',
                                                  )
                    slack_manager.post_slack_message(f'Do I think there is a needle on the probe? {got_needle}')
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.fullmatch('dump needle', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    slack_manager.post_slack_message(
                        f'Dumping this needle now, I will send a photo back after I dump it')
                    needle_still_on, path_to_save_image = dump_needle_with_vision()
                    slack_manager.post_slack_file(filepath=path_to_save_image,
                                                  title='Deck camera',
                                                  comment='Deck camera',
                                                  )
                    slack_manager.post_slack_message(f'Do I think there is a needle on the probe? {needle_still_on}')
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.fullmatch('uncap needle', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                try:
                    slack_manager.post_slack_message(
                        f'Uncapping this needle now, I will send a photo back after Im done')
                    got_needle = uncap_needle()
                    slack_deck_image()
                    slack_manager.post_slack_message(f'Do I think there is a needle on the probe? {got_needle}')
                except Exception as e:
                    logger.info(f'encountered error {e}')

        elif re.fullmatch('main resume', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                global MAIN_PAUSE
                MAIN_PAUSE = False
                print('main_pause slack:', MAIN_PAUSE)
                slack_manager.post_slack_message('Going to resume now')

        elif re.search('help', text, re.IGNORECASE) is not None:
            with WebClientOverride(slack_manager, web_client):
                help_statement = f'Possible commands:\n' \
                                 f'*vial image* - current image from camera aimed at vials in the vision_needle_check station\n' \
                                 f'*deck image* - current image from camera aimed at the deck\n' \
                                 f'*get needle* - get and uncap a needle\n' \
                                 f'*uncap needle* - uncap a needle already on the probe\n' \
                                 f'*dump needle* - remove a needle\n' \
                                 f'*main resume* - if the run paused at a specific point, use this to resume the run\n' \
                                 f'*graph* - graph turbidity vs time\n' \
                                 f'*add solvent* - add {ei.solvent_addition_volume} mL of {ei.solvent.name}\n' \
                                 f'*clean up vial* - clean up the current vial at index {ei.vial_index}\n' \
                                 f'*start next vial* - start the next experiment; clean up must have occured before ' \
                                 f'running this command to \n' \
                                 f'*dont start next vial* - dont start the next experiment\n' \
                                 f'*roi* - current image from camera with roi to measure turbidity drawn\n' \
                                 f'*change stir rate [number]* - change the stir rate to the specified number\n' \
                                 f'*set dissolved reference [number]* - change the dissolved reference to the specified number\n' \
                                 f'*change solid weight [number]* - change the mass of solid to add to the ' \
                                 f'specified number in mg for the next test_tecan_cavro.py\n' \
                                 f'*turbidity video* - turbidity video of the current run' \
                                 f'*change initial volume [number]* - change the initial volume of {solvent.name} to ' \
                                 f'the specified number\n' \
                                 f'*use next solvent* - for the next run, use the next solvent in the sequence to test_tecan_cavro.py\n' \
                                 f'*use next solid* - for the next run, use the next solid in the sequence to test_tecan_cavro.py\n' \
                                 f'*resume* - resume solvent addition and turbidity monitoring\n' \
                                 f'*pause solvent addition* - pause solvent addition\n' \
                                 f'*resume solvent addition* - resume solvent addition\n' \
                                 f'*pause monitoring* - pause turbidity monitoring\n' \
                                 f'*resume monitoring* - resume turbidity monitoring'
                slack_manager.post_slack_message(msg=help_statement)
    except TypeError as e:
        logger.info(f'encountered error {e}')


threading.Thread(
    target=slack_manager.start_rtm_client).start()  # todo comment back to be able to send commands to the script from slack


def create_experiment_folder():
    dir_name = 'E:\mushu\experiments'
    experiment_number = 1
    experiment_name = f'study_{experiment_number}'
    experiment_folder = Path(os.path.join(dir_name, exp_type, experiment_name))
    while True:
        if experiment_folder.exists():
            experiment_number += 1
            experiment_name = f'study_{str(experiment_number)}'
            experiment_folder = Path(os.path.join(dir_name, exp_type, experiment_name))
        else:
            Path.mkdir(experiment_folder)
            break
    return experiment_folder


def initialize_turbidity_camera(camera_images_folder: Path):
    datetime_format = '%Y_%m_%d_%H_%M_%S_%f'
    c = None
    # todo comment back in
    c = Camera(port=deck.vision_station_camera_port,
               save_folder=camera_images_folder,
               datetime_string_format=datetime_format,
               )
    return c


# def make_vision_selections(vision_selection_tm: TurbidityMonitor, camera: Camera, annotated_regions_path: str):
#     """
#     Select region of interest and normalization region and dissolved reference for all experiments
#     :param tm_selection_path: path to json file to load selections for turbidity monitoring
#     """
#     image = camera.take_photo(save_photo=False)
#     vision_selection_tm.select_normalization_region(image)
#     vision_selection_tm.select_monitor_region(image)
#     # save image with selected regions drawn on it
#     selected_regions_image = vision_selection_tm.draw_regions(image=camera.last_frame)
#     cv2.imwrite(annotated_regions_path,
#                 selected_regions_image,
#                 )
#     # set a dissolved reference
#     dissolved_reference_images = camera.take_photos(n=tm_n_images_for_dissolved_reference,
#                                                     save_photo=False)
#     vision_selection_tm.set_dissolved_reference(*dissolved_reference_images, select_region=False)
#     vision_selection_tm.turbidity_dissolved_reference *= 1.1
#     vision_selection_tm.save_json_data()
#     return vision_selection_tm

def way_below_dissolved(tm: TurbidityMonitor, n_measurements=None) -> bool:
    """return true if the past n measures are way below the dissolved reference, return true"""
    if n_measurements is None:
        n_measurements = int(tm.n / 2)
    _, y_values = tm.get_turbidity_data_for_graphing()
    if len(y_values) <= n_measurements:
        return False
    last_n_measurements = y_values[-n_measurements:]
    dissolved_reference = tm.turbidity_dissolved_reference
    limit = 15
    way_below_limit = dissolved_reference - limit
    mean_last_n_measurements = np.mean(last_n_measurements)
    if mean_last_n_measurements <= way_below_limit:
        return True
    else:
        return False


def seems_dissolved(tm: TurbidityMonitor, n_measurements=None):
    """return true if the past n measurements of turbidity are beneath the dissolved reference, even if the
    measurements arent necessarily stable"""
    if n_measurements is None:
        n_measurements = int(tm.n * 1.2)
    _, y_values = tm.get_turbidity_data_for_graphing()
    if len(y_values) <= n_measurements:
        return False
    last_n_measurements = y_values[-n_measurements:]
    dissolved_reference = tm.turbidity_dissolved_reference
    n_measurements_below_dissolved_ref = [measurement < dissolved_reference for measurement in
                                          last_n_measurements].count(True)
    if n_measurements == n_measurements_below_dissolved_ref:
        return True
    else:
        return False


def add_solid(solid_weight: float = 1, vision_index: str = 'A1'):
    location = deck.vision_grid_needle.locations[vision_index]
    safe_location = location.copy(z=300)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True, probe_offset=43.5)
    global MAIN_PAUSE
    dump_needle()
    deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)
    vial_from_vision(vision_index)
    vial_to_gripper(capped=True, from_tray=False)
    # weigh the empty capped vial
    deck.c9.output(1, False)
    watch = datetime.now()
    deck.c9.output(1, True)
    uncap_from_tray(from_tray=False)
    park_cap()
    vial_from_gripper(capped=False, to_tray=False)
    logger.info('Dosing extra solid in vial')
    mass_no_units = dose_with_quantos(solid_weight=solid_weight)
    print('quantos is commanded to dose', mass_no_units)
    vial_to_gripper(from_tray=False, capped=False)
    pickup_cap()
    recap_to_tray(to_tray=False)
    vial_to_gripper(capped=True, from_tray=False)
    # weigh the empty capped vial with the solid added
    deck.c9.output(1, False)
    deck.c9.output(1, True)
    time.sleep(5)
    vial_from_gripper(capped=True, to_tray=False)
    # remove units
    # vial_to_gripper(from_tray=False, capped=False)
    # vial_from_gripper(to_tray=False,capped=True)
    mass_no_units = mass_no_units / 1000  # convert mg to g
    vial_to_vision(vision_index)
    # heat station modification
    close_vision_station()
    return mass_no_units


def add_solvent_no_tracking(index, volume):
    # get needle out of vision_needle_check vial
    location = deck.vision_grid_needle.locations['A1']  # todo double check is correct
    safe_location = location.copy(z=300)
    pierce_location = location.copy(z=187)
    approach_location = location.copy(z=200)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)

    # check if there is needle on
    # move_for_needle_check()
    # needle_on_probe, path_to_save_image = deck.vision_needle_check()
    # if needle_on_probe==False:
    #    global main_pause
    #    got_needle, path_to_save_image = get_needle_with_vision()
    #    if got_needle == False:
    #        main_pause = True
    #        needle_not_on_message()
    #        while main_pause:
    #            time.sleep(5)
    # else:
    #    deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=308, probe=True)

    # grab the dose from stock
    bring_stock_in_line(stock_index=index, sample_volume=volume)
    # needle back to vision_needle_check
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)
    # needle in
    deck.n9.move_to_location(approach_location, gripper=109, probe=True)

    deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
    deck.dosing_pump.move_absolute_ml(0, deck.SOLVENT_DRAW_RATE)


def prepare_for_vision(rxn_index: str = 'F4', solid_weight: float = 0, vision_index: str = 'A1', capped: bool = True):
    """
    prepares the vial for vision_needle_check by picking up from reaction block, powder dosing the amount with quantos and
    transferring to the desired vision_needle_check station
    :param rxn_index: what location on deck is the vial being taken from
    :param solid_weight: weight desired in mg
    :param vision_index: what location on the vision_needle_check grid us the vial being taken to
    :param capped: if the vial on tray has cap on
    :return:
    """

    # weigh vial with quantos first (with cap nd stir bar)
    vial_from_tray(rxn_index, capped=False)
    vial_to_gripper()
    vial_from_gripper()
    empty_vial_weight_quantos = weigh_with_quantos()
    vial_to_gripper(rxn_index, capped=False, from_tray=False)
    # weigh the empty capped vial
    deck.c9.output(1, False)
    watch = datetime.now()

    deck.c9.output(1, True)

    # if it is capped, uncap and park the cap
    if capped:
        uncap_from_tray(from_tray=False)
        park_cap()

    vial_from_gripper(rxn_index, capped=False, to_tray=False)
    print('solid weight =', solid_weight)
    logger.info('Does main solid')
    mass_no_units = dose_with_quantos(solid_weight=solid_weight)
    vial_to_gripper(from_tray=False, capped=False)

    # cap it again
    if capped:
        pickup_cap()
        recap_to_tray(to_tray=False)

    vial_to_gripper(capped=capped, from_tray=False)
    # weigh the empty capped vial with the solid added
    deck.c9.output(1, False)

    deck.c9.output(1, True)
    time.sleep(5)
    vial_from_gripper(rxn_index=rxn_index, capped=False, to_tray=False)
    # weigh with quantos
    vial_with_solid_weight_quantos = weigh_with_quantos()
    # remove units
    vial_to_gripper(from_tray=False, capped=False)
    open_vision_station()
    vial_from_gripper(capped=capped, to_tray=False)
    mass_no_units = mass_no_units / 1000  # convert mg to g
    vial_to_vision(vision_index, drop_times=0, capped=capped)
    close_vision_station()
    return mass_no_units, empty_vial_weight_quantos, vial_with_solid_weight_quantos


def prepare_to_dose(from_stock: bool = False, stock_index: str = 'A1', vision_index: str = 'A1',
                    vial_start_volume: float = 0.3, wash_with_stock: bool = False):
    """

    :param from_stock: if true, the solvent in line will be chosen from the stock
    :param stock_index: which location on the stock grid is the vial being chosen from (if from_stock)
    :param vision_index: which location on the vision_needle_check station grid is solvent being dosed too
    :param vial_start_volume: the initial volume where the image for vision_needle_check study will be selected from
    :param wash_with_stock: if true, will wash a line with the stock solvent
    :return:
    """

    # poke the station close
    # poke_vision_top_close()
    # if vision_index == 'A1':
    # check if vial is in the postion
    # temp_camera = Camera(port=1,
    #                     datetime_string_format='%Y_%m_%d_%H_%M_%S',
    #                     )
    # if deck.vision_cap_detection(camera=temp_camera) is None:
    #    input('no vial detected')
    # # take needle
    # deck.needle_tray.pickup()
    #### # uncap needle
    # uncap_needle()
    # deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)
    # deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=
    global VISION_NEEDLE_DETECTION
    if VISION_NEEDLE_DETECTION is not True:
        logger.info('Attemps picking up needle')
        deck.needle_tray.pickup()
        uncap_needle()
        deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)

    else:
        global MAIN_PAUSE
        got_needle, path_to_save_image = get_needle_with_vision()
        if got_needle == False:
            MAIN_PAUSE = True
            needle_not_on_message()
            while MAIN_PAUSE:
                print('main pause:', )
                logger.info(f'needle check fails, main pause: {MAIN_PAUSE}')
                time.sleep(5)
    if from_stock:
        logger.info('Attempt bring stock in line')
        if wash_with_stock:
            bring_stock_in_line(stock_index, sample_volume=0.95)
            wash_dosing_line(by_tap_solvent=False)
        # todo need to break down this initial dispense of the start volume
        bring_stock_in_line(stock_index=stock_index, sample_volume=vial_start_volume)
        logger.info('bring stock in line success')
    else:
        #  line to get rid of inline bubbles
        wash_dosing_line()
        wash_dosing_line()

    location = deck.vision_grid_needle.locations[vision_index]
    safe_location = location.copy(z=300)
    # 187
    pierce_location = location.copy(z=187)
    approach_location = location.copy(z=205)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)
    deck.n9.move_to_location(approach_location, gripper=109, probe=True)
    deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
    logger.info('needle pierce finished')
    if from_stock:
        # deck.dosing_pump.move_relative_ml(-0.1,deck.SOLVENT_DRAW_RATE)
        deck.dosing_pump.move_absolute_ml(0, deck.SAMPLE_FLUSH_RATE)
    else:
        print(vial_start_volume)
        deck.dosing_pump.dispense_ml(volume_ml=vial_start_volume,
                                     from_port=deck.SOLVENT_PORT,
                                     to_port=deck.SAMPLE_PORT,
                                     velocity_ml=deck.SOLVENT_DRAW_RATE,
                                     dispense_velocity_ml=deck.SOLVENT_DRAW_RATE,
                                     )
    logger.info('solvent dispense finished')
    ##################################################


def vision_clean_up(vision_index: str = 'A1', rxn_index: str = 'A1', capped: bool = False,
                    wash_by_on_tap_solvent: bool = False):
    """
    will wash the line, dump the needle and transfer vial from the vision_needle_check station back to tray
    :param vision_index:
    :param rxn_index:
    :param capped: whether the vial being cleaned up is capped or uncapped
    :param wash_by_on_tap_solvent: if false, will spit out rest of what was previously aspirated by dosing pump
    :return:
    """
    location = deck.vision_grid_needle.locations[vision_index]
    safe_location = location.copy(z=300)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True, probe_offset=43.5)
    if not wash_by_on_tap_solvent:
        wash_dosing_line(by_tap_solvent=False)
    global VISION_NEEDLE_DETECTION
    if VISION_NEEDLE_DETECTION is not True:
        dump_needle()
        deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)
    else:
        global MAIN_PAUSE
        needle_still_on, path_to_save_image = dump_needle_with_vision()
        if needle_still_on == True:
            MAIN_PAUSE = True
            needle_still_on_message()
            while MAIN_PAUSE:
                time.sleep(5)
    vial_from_vision(vision_index, capped=capped)
    # take to gripper to record final weight
    vial_to_gripper(from_tray=False, capped=False)
    deck.c9.output(1, False)
    time.sleep(4)
    full_vial_mass = None
    # full_vial_mass = deck.scale.settled_weigh(matching=2)
    # watch=datetime.now()
    # while full_vial_mass<2.5:
    #    full_vial_mass = deck.scale.settled_weigh(matching=2)
    #    if full_vial_mass == 0:
    #        logging.info("seems like you missed the vial, gonna move on to next experiment")
    #        break
    #    if datetime.now()>watch+timedelta(0,120):
    #        logging.info("seems like the gripper is not empty, check for shards")
    deck.c9.output(1, True)
    vial_from_gripper(rxn_index, capped=False, to_tray=False)
    # record final weight with quantos
    full_vial_mass_quantos = weigh_with_quantos()
    vial_to_tray(rxn_index)

    return full_vial_mass, full_vial_mass_quantos


def dose_and_weigh(vision_index: str = 'A1', rxn_index: str = 'A1',
                   dispense_vol: float = 0, solvent_density=0.79, wash_line: bool = False, initial_mass: float = 0,
                   from_tray: bool = False):
    """will dose a specified volume in vision_needle_check station location and calculate the actual volume dosed via quantos, vial
     will be back to that vision_needle_check location after, ready for imaging etc"""
    global VISION_NEEDLE_DETECTION
    # weigh with quantos
    if initial_mass == 0:
        if from_tray:
            vial_from_tray(rxn_index=rxn_index, capped=False)
        else:
            vial_from_vision(vision_index)
            vial_to_gripper(from_tray=False, capped=True)
            vial_from_gripper(to_tray=False, capped=False)
        empty_vial_w_quantos = weigh_with_quantos()
        vial_to_gripper(capped=False)
        vial_from_gripper(capped=True)
        # take it to vision_needle_check station where reference photo needs to be taken
        vial_to_vision(vision_index=vision_index)

    else:
        empty_vial_w_quantos = initial_mass

    # pick up a needle and fill that vial up
    # poke_vision_top_close()

    global VISION_NEEDLE_DETECTION
    if VISION_NEEDLE_DETECTION is not True:
        deck.needle_tray.pickup()
        uncap_needle()
        deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)

    else:
        global MAIN_PAUSE
        got_needle, path_to_save_image = get_needle_with_vision()
        if got_needle == False:
            MAIN_PAUSE = True
            needle_not_on_message()
            while MAIN_PAUSE:
                time.sleep(5)
    if wash_line:
        wash_dosing_line(wash_volume=3)
    location = deck.vision_grid_needle.locations[vision_index]
    safe_location = location.copy(z=300)
    pierce_location = location.copy(z=189)
    approach_location = location.copy(z=200)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)
    deck.n9.move_to_location(approach_location, gripper=109, probe=True)
    deck.n9.move_to_location(pierce_location, gripper=109, probe=True)

    deck.dosing_pump.dispense_ml(volume_ml=dispense_vol,
                                 from_port=deck.SOLVENT_PORT,
                                 to_port=deck.SAMPLE_PORT,
                                 velocity_ml=deck.SOLVENT_DRAW_RATE
                                 )
    # go up and dump needle
    deck.n9.move_to_location(safe_location, gripper=109, probe=True, probe_offset=43.5)
    if VISION_NEEDLE_DETECTION is not True:
        dump_needle()
        deck.n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)

    # wash_dosing_line()
    else:
        needle_still_on, path_to_save_image = dump_needle_with_vision()
        if needle_still_on == True:
            MAIN_PAUSE = True
            needle_still_on_message()
            while MAIN_PAUSE:
                time.sleep(5)
    vial_from_vision(vision_index)
    vial_to_gripper(capped=True)
    vial_from_gripper(capped=False)
    full_vial_w_quantos = weigh_with_quantos()
    vial_to_gripper(capped=False)
    vial_from_gripper(capped=True)
    vial_to_vision(vision_index)
    solvent_mass = full_vial_w_quantos - empty_vial_w_quantos
    # convert to volume
    solvent_volume = solvent_mass / solvent_density  # L
    solvent_volume = solvent_volume * 1000  # volume in mL
    return solvent_mass, solvent_volume, full_vial_w_quantos


# def choose_roi():
#     roi_cam = initialize_turbidity_camera(camera_images_folder=camera_images_folder)
#     vision_selection_tm = TurbidityMonitor(turbidity_monitor_data_save_path=vision_selection_json_path)
#     deck.mini_heater_stirrer.target_stir_rate = ei.stir_rate
#     vision_selection_tm = make_vision_selections(vision_selection_tm=vision_selection_tm,
#                                                  camera=roi_cam,
#                                                  annotated_regions_path=annotated_regions_path,
#                                                  )
#     roi_cam.disconnect()

# def make_reference(rxn_index: str = 'A1',reference_solvent: str= 'water',vision_index: str = 'A1', volume: float=0.1):
#     vial_from_tray(rxn_index=rxn_index,capped=True)
#     deck.mini_heater_stirrer.start_stirring()
#     vial_to_vision(vision_index=vision_index)
#     get_needle_with_vision()
#     location = deck.vision_grid_needle.locations[vision_index]  # todo double check is correct
#     safe_location = location.copy(z=300)
#     pierce_location = location.copy(z=187)
#     # needle back to vision_needle_check
#     deck.n9.move_to_location(safe_location, gripper=109, probe=True)
#     # needle in
#     deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
#     deck.dosing_pump.dispense_ml(volume_ml=volume,
#                                  from_port=deck.SOLVENT_PORT,
#                                  to_port=deck.SAMPLE_PORT,
#                                  velocity_ml=deck.SOLVENT_DRAW_RATE,
#                                  dispense_velocity_ml=deck.SOLVENT_DRAW_RATE,
#                                  )
#     deck.n9.move_to_location(safe_location, gripper=109, probe=True)
#     dump_needle_with_vision()
#     # choose_roi()
#     # set a dissolved reference
#     dissolved_reference_images = camera.take_photos(n=tm_n_images_for_dissolved_reference,
#                                                     save_photo=False)
#     vial_from_vision(vision_index=vision_index)
#     vial_to_tray(rxn_index=rxn_index,capped=True)
#     return dissolved_reference_images


# def make_reference_calibration(rxn_index: str = 'A1', vision_index: str = 'A2', solvent_density: float = 0.79):
#     """will do two rounds of dose to a vial from tray to find two liquid level lines"""
#
#     #pick up a vial from tray
#     solvent_mass1, solvent_vol1, final_mass = dose_and_weigh(vision_index=vision_index,rxn_index=rxn_index,
#                                                              dispense_vol=first_solvent_initial_volume,from_tray=True,
#                                                              solvent_density=solvent_density)
#     choose_roi()


def main_pause_message():
    slack_manager.post_slack_message('Something might have gone wrong, pausing until the "main resume" command is sent')


def needle_still_on_message():
    main_pause_message()
    slack_deck_image()
    slack_manager.post_slack_message('I think the needle is still on the probe after trying to dump it. You can use '
                                     'the "get needle", "uncap needle", and "dump needle" commands to take control '
                                     'until the issue is solved. I require no needle on the probe to safely continue. '
                                     'Once there is no needle on the probe, send the "main resume" command')


def needle_not_on_message():
    main_pause_message()
    slack_deck_image()
    slack_manager.post_slack_message('I think the needle is not on the probe after trying to get one. You can use '
                                     'the "get needle", "uncap needle" and "dump needle" commands to take control until '
                                     'the issue is solved. I require a needle on the probe to safely continue. Once '
                                     'there is a needle on the probe, send the "main resume" command')


def slack_deck_image():
    last_image = deck.deck_camera.take_photo(save_photo=False)
    last_image_path = str(deck.deck_camera.save_folder.joinpath('last_image.jpg'))
    cv2.imwrite(last_image_path, last_image)
    time.sleep(1)
    slack_manager.post_slack_file(filepath=last_image_path,
                                  title='Deck camera',
                                  comment='Deck camera',
                                  )


def transfer_reference(from_station_index: str = 'A1', to_station_index: str = 'A2', capped: bool = True):
    # pick up from
    # poke_vision_top_open()
    vial_from_vision(from_station_index, capped=capped)
    vial_to_vision(to_station_index, drop_times=0, capped=capped)


def save_results():
    results.add_data(experiment_information=ei,
                     tm_parameters=tm_parameters)
    results.save(path=results_path)
    save_solvent_tracking_data()


def set_first_solvent_tracking_data(solvent_name, volume_added, total_volume, start_time):
    solvent_addition_data.add_data({'Solvent': solvent_name,
                                    'Volume added': volume_added,
                                    'Total volume (mL)': total_volume},
                                   t=start_time)
    save_solvent_tracking_data()


def update_solvent_tracking_data(solvent_name, volume_added, total_volume):
    solvent_addition_data.add_data({'Solvent': solvent_name,
                                    'Volume added': volume_added,
                                    'Total volume (mL)': total_volume})


def save_solvent_tracking_data():
    solvent_addition_data.save_csv()


def choose_solvent():
    def choose():
        global solvent_initial_volume
        global solid_weight
        global solvent_addition_volume
        solvent_name = test_info.choose_solvent()
        if solvent_name is None:
            # done all tests with all solvents
            solid = test_info.choose_next_solid()
            if solid is None:
                return None
            else:
                solvent_name = test_info.choose_solvent()
                assert (solvent_name is not None)
        s = deck_consumables.get_stock(solvent_name)
        if previous_solvent_name != solvent_name and previous_solvent_name != 'first':
            # choosing a different solvent, then go back to using initial testing parameters
            solvent_initial_volume = first_solvent_initial_volume  # mL
            solid_weight = first_solid_weight  # mg
            solvent_addition_volume = first_solvent_addition_volume  # mL
        return s

    global solvent
    solvent = choose()
    if solvent is None:
        return None
    while enough_solvent(solvent_initial_volume) is False:
        slack_manager.post_slack_message(f'I only have {solvent.current_volume} mL left of {solvent.name}, not enough '
                                         f'to add an initial volume of {solvent_initial_volume} without going under '
                                         f'the stock safe volume of {solvent.safe_volume}. Going to choose another '
                                         f'solvent')
        test_info.choose_next_solvent()
        solvent = choose()
        if solvent is None:
            return None
    return solvent


def choose_solid():
    solid_name = test_info.choose_solid()
    if solid_name is None:
        # done all tests with all solvents
        return None
    solid_index = deck_consumables.get_solid_info(solid_name)
    return solid_index


class TestInformation:
    def __init__(self):
        # dictionary of solvent name and number of tests done with that solvent that have succeeded
        self._solvents: Dict[str, int] = {}
        self._solids: Dict[str, int] = {}
        self.n_per_solvent = 3  # number of successful tests to do per solvent
        self._current_solvent = None
        self._next_solvent = None
        self._current_solid = None
        self._next_solid = None

    @property
    def solvents(self):
        return self._solvents

    @property
    def solids(self):
        return self._solids

    @property
    def solid_names(self) -> List:
        return list(self.solids.keys())

    @property
    def solvent_names(self) -> List:
        return list(self.solvents.keys())

    @solvents.setter
    def solvents(self, value: List[str]):
        zeros = [0 for i in value]
        self._solvents = dict(zip(value, zeros))
        self.current_solvent = self.solvent_names[0]

    @solids.setter
    def solids(self, value: List[str]):
        zeros = [0 for i in value]
        self._solids = dict(zip(value, zeros))
        self.current_solid = self.solid_names[0]

    @property
    def current_solvent(self) -> str:
        return self._current_solvent

    @property
    def current_solid(self) -> str:
        return self._current_solid

    @current_solvent.setter
    def current_solvent(self, value):
        if value is None:
            self._current_solvent = value
            self.next_solvent = None
        if value in self.solvent_names:
            self._current_solvent = value
            index = self.solvent_names.index(value)
            try:
                self.next_solvent = self.solvent_names[index + 1]
            except IndexError:
                self.next_solvent = None

    @current_solid.setter
    def current_solid(self, value):
        if value is None:
            self._current_solid = value
            self.next_solid = None
        if value in self.solid_names:
            self._current_solid = value
            index = self.solid_names.index(value)
            try:
                self.next_solid = self.solid_names[index + 1]
            except IndexError:
                self.next_solid = None

    @property
    def next_solvent(self) -> str:
        return self._next_solvent

    @property
    def next_solid(self) -> str:
        return self._next_solid

    @next_solvent.setter
    def next_solvent(self, value):
        if value is None:
            self._next_solvent = None
        if value in self.solvent_names:
            try:
                self._next_solvent = value
            except IndexError:
                self._next_solvent = None

    @next_solid.setter
    def next_solid(self, value):
        if value is None:
            self._next_solid = None
        if value in self.solid_names:
            try:
                self._next_solid = value
            except IndexError:
                self._next_solid = None

    def add_test(self, solvent_name):
        # increase number of tests done with a solvent by 1
        self.solvents[solvent_name] += 1

    def n_tests(self, solvent_name):
        # return number of tests done with a solvent
        return self.solvents[solvent_name]

    def reset_n_tests_for_solvents(self):
        solvent_names = self.solvent_names
        for name in solvent_names:
            self.solvents[name] = 0

    def choose_next_solvent(self) -> str:
        next = self.next_solvent
        self.current_solvent = next
        return next

    def choose_next_solid(self) -> str:
        next = self.next_solid
        self.current_solid = next
        # reset number of tests done for each solvent since a new solid will be used, and set the current solvent to
        # the first solvent to test_tecan_cavro.py
        test_info.reset_n_tests_for_solvents()
        test_info.current_solvent = test_info.solvent_names[0]
        return next

    def choose_solvent(self) -> str:
        # select current solvent based on if the number of valid tests run with the solvent has been achieved
        # return a solvent name
        current = self.current_solvent
        if current is None:
            return None
        elif self.n_tests(current) < self.n_per_solvent:
            return current
        else:
            current = self.choose_next_solvent()
            return current

    def choose_solid(self) -> str:
        current = self.current_solid
        return current


if __name__ == '__main__':
    #vial_from_quantos()
    #vial_to_gripper(capped=False)
    #pickup_cap()
    #recap_to_tray(to_tray=True,rxn_index='B5')

    # dump_needle_with_vision()
    #deck.needle_tray.reset()
    record = {}
    exp_type = 'solubility with open vision station'
    MAIN_PAUSE = False  # if true, some parts in this script may be paused until variables are false again
    VISION_NEEDLE_DETECTION = False
    vision_station_monitor_index = 'A1'

    # ----------------------------------------------------------------------
    # todo  UPDATE EXPERIMENT PARAMETERS HERE
    first_solvent_initial_volume = 0.5  # mL
    first_solid_weight = 0.010  # g
    first_solvent_addition_volume = 0.03  # ml

    solid_weight_increment = 0.002  # g
    solvent_addition_increment = -0.02  # mL
    stir_rate = 400
    test_info = TestInformation()
    test_info.solvents = [
        # 'water'
        #'acetonitrile',
        #'DCM',
        #'IPAc',
        # 'THF',
         'DMF',
        # 'DMSO',
        # 'hexane'
        # 'THF'
        # 'ethylAcetate'
        # 'DMAc',
        # 'DCM',
        # 'heptane',
        # 'toluene',
        # 'dioxane'
        # 'isopropylacetate',

    ]

    test_info.solids = ['ICE']  # 'CSTI', 'MD1-204' 'saccharin'

    # todo order the scv list based on the order of the solvent list above
    # ----------------------------------------------------------------------
    # keep track of the initial volumes and weights used for starting new experiments with different solvents
    solvent_initial_volume = first_solvent_initial_volume  # mL
    solid_weight = first_solid_weight  # g
    solvent_addition_volume = first_solvent_addition_volume  # mL

    camera: Camera = None
    tm: TurbidityMonitor = None
    graph_path: str = None
    video_path: str = None
    pause_addition_bool = False
    pause_monitoring_bool = False
    start_next_vial = True  # first time, must be true
    clean_up_vial = True
    ei: ExperimentInformation = None
    can_safely_add_solvent = False
    solvent = None
    solid = None

    # turbidity monitor parameters
    tm_n_images_per_measurement = 15  # every turbidity measurement is actually the average of this number of measurements
    tm_n_measurements_per_min = 12
    fps = (tm_n_images_per_measurement * tm_n_measurements_per_min) / 60
    fps = 10 * fps  # to speed things up, multiply by 10
    tm_n_minutes = 4  # number of minutes the data needs to be stable in order to be determined as stable
    tm_n = tm_n_measurements_per_min * tm_n_minutes  # don't touch this parameter
    tm_std_max = 3
    tm_sem_max = 3
    tm_upper_limit = 4
    tm_lower_limit = 10
    tm_range_limit = 7
    tm_n_images_for_dissolved_reference = 200
    tm_parameters = {
        'tm_n_images_per_measurement': tm_n_images_per_measurement,
        'tm_n_measurements_per_min': tm_n_measurements_per_min,
        'tm_n_minutes': tm_n_minutes,
        'tm_n': tm_n,
        'tm_std_max': tm_std_max,
        'tm_sem_max': tm_sem_max,
        'tm_upper_limit': tm_upper_limit,
        'tm_lower_limit': tm_lower_limit,
        'tm_range_limit': tm_range_limit,
        'tm_n_images_for_dissolved_reference': tm_n_images_for_dissolved_reference,
    }

    # for the excel file
    datetime_format = '%d-%b-%Y %I:%M %p'  # day-month-year hour:min am/pm,
    d = datetimeManager(string_format=datetime_format)

    # for saving images and raw turbidity data
    datetime_format = '%Y_%m_%d_%H_%M_%S_%f'

    # create main folder for an experiment and paths for overall experiment files
    experiment_folder: Path = create_experiment_folder()
    results = Results()
    vision_selection_json_path = turbidity_monitor_base_file_path_json
    shutil.copy2(annotated_monitor_turbidity_roi_path, experiment_folder)
    results_path = experiment_folder.joinpath('results.csv')
    annotated_regions_path = str(experiment_folder.joinpath(f'vision_selection.png'))
    roi_name = 'vial'

    while True:
        if start_next_vial:
            logger.info('Experiment starts')
            if solvent is not None:
                previous_solvent_name = solvent.name
            else:
                previous_solvent_name = 'first'  # only true in the case if its the first experiment
            solvent = choose_solvent()
            solid = test_info.current_solid
            if solvent is None or solid is None:
                # done all tests with all solvents, exit out of while loop
                slack_manager.post_slack_message('Finished testing all solvent and solid combinations. Ending now')
                logger.info('Script end')
                break
            slack_manager.post_slack_message(f'I will use {solvent.name} and {solid} for the next experiment; there is '
                                             f'{solvent.current_volume} mL left in the stock vial')
            logger.info(f'test starts: {solid}, {solvent}')
            start_next_vial = False  # set to false to wait until things have finished or slack command to do the next vial
            clean_up_vial = False

            vial_index, vial_cap_status = deck_consumables.get_clean_vial()
            if vial_index is None:
                # no more vials, exit out of while loop
                slack_manager.post_slack_message('Out of vials. Ending now')
                break

            #current_solid_on_quantos = quantos_guns.current_solid_name()
            #if solid != current_solid_on_quantos:
            #    # quantos_guns.switch_gun(solid)
            #    logger.error('Wrong solid loaded on quantos')
            #    logger.info('Script ends')
            #    break

            solid_weight_g = solid_weight
            ei = ExperimentInformation(vial_index=vial_index,
                                       solvent=solvent,
                                       solid=solid,
                                       target_initial_volume=solvent_initial_volume,  # mL
                                       target_solid_mass=solid_weight_g,  # g
                                       solvent_addition_volume=solvent_addition_volume,  # mL
                                       parent_path=experiment_folder)
            ei.stir_rate = stir_rate

            solvent_addition_data_save_path: Path = ei.folder.joinpath('solvent addition data')
            solvent_addition_data = TemporalData(save_path=solvent_addition_data_save_path)

            camera_images_folder = ei.folder.joinpath(f'{ei.experiment_name}_images')
            camera = initialize_turbidity_camera(camera_images_folder=camera_images_folder)

            # if ei.experiment_number == 1:
            #    make_reference(rxn_index='A1', vision_index=vision_station_monitor_index, volume=first_solvent_initial_volume)  # todo right now hard coded density of acetonitirile.
            #    # todo instead should make the dose and weigh function modular so it can choose either tap solvent or deck solvent and be able to get density from that

            turbidity_save_path = str(ei.folder.joinpath(f'{ei.experiment_name}_turbidity data'))
            graph_name = f'{ei.experiment_name} turbidity graph.png'
            graph_path = str(ei.folder.joinpath(graph_name))
            video_name = f'{ei.experiment_name} turbidity video.mp4'
            video_path = str(ei.folder.joinpath(video_name))

            ## get dissolved reference
            # if ei.experiment_number == 1:
            #    dissolved_reference_images = make_reference(rxn_index='A1', vision_index=vision_station_monitor_index, volume=first_solvent_initial_volume)
            #    tm = TurbidityMonitor(turbidity_monitor_data_save_path=turbidity_save_path,
            #                          datetime_format=datetime_format)
            #    tm = set_turbidity_monitor_parameters(tm=tm)
            #    tm.set_dissolved_reference(*dissolved_reference_images, select_region=False)
            #    tm.turbidity_dissolved_reference *= 1.1
            # tm.save_json_data()
            # if ei.experiment_number == 1:
            # transfer_reference(from_station_index=vision_station_reference_index,
            # to_station_index=vision_station_monitor_index)

            # lc.setup(camera.last_frame, roi_name)  # set up
            # transfer_reference(from_station_index=vision_station_monitor_index,
            #                   to_station_index=vision_station_reference_index)
            # record positions of joints
            # position_record()

            # todo: comment back in
            solid_mass, empty_vial_weight_quantos, vial_with_solid_mass_quantos = \
                prepare_for_vision(rxn_index=vial_index,
                                   solid_weight=ei.target_solid_mass * 1000,  # g -> mg
                                   vision_index=vision_station_monitor_index)
            ei.solid_mass = solid_mass
            ei.initial_mass = vial_with_solid_mass_quantos
            ei.initial_mass_zetascale = None
            ei.empty_vial_mass = empty_vial_weight_quantos
            ei.empty_vial_mass_zetascale = None
            save_results()
            # todo: comment out
            # fake numbers
            # ei.solid_mass = 0.08059  # g
            # ei.initial_mass = 2.77025  # g
            # ei.initial_mass_zetascale = None
            # ei.empty_vial_mass = 2.68994  # g
            # ei.empty_vial_mass_zetascale = None
            # save_results()
            ################################

            prepare_to_dose(from_stock=True, stock_index=solvent.index,
                            vision_index=vision_station_monitor_index,
                            vial_start_volume=ei.target_initial_volume)
            solvent.current_volume -= ei.target_initial_volume
            deck_consumables.update_stock_file(solvent.name, 'current volume', solvent.current_volume)

            ei.start_time = datetime.now()
            logger.info('stir starts')
            start_time = ei.start_time.strftime(d.string_format)
            message = f'Running experiment {ei.experiment_name} with parameters:\n' \
                      f'Vial index: {ei.vial_index}\n' \
                      f'{solvent.name} start volume: {ei.target_initial_volume} mL\n' \
                      f'Stir rate: {ei.stir_rate} rpm\n' \
                      f'solvent addition volume: {ei.solvent_addition_volume} mL\n' \
                      f'Mass {solid}: {ei.solid_mass} g'
            slack_manager.post_slack_message(message)

            # previous_dispense = camera.take_photos(30, False)

            tm = TurbidityMonitor(turbidity_monitor_data_save_path=turbidity_save_path,
                                  datetime_format=datetime_format)
            tm = set_turbidity_monitor_parameters(tm=tm)
            # load in vision_needle_check region selections
            tm.load_data(json_path=vision_selection_json_path)

            background_monitor = TurbidityMonitorRunnable(tm=tm,
                                                          camera=camera,
                                                          slack_manager=slack_manager,
                                                          graph_path=graph_path,
                                                          experiment_information=ei,
                                                          )
            can_safely_add_solvent = True
            background_monitor.start_background_monitoring()
            deck.mini_heater_stirrer.target_stir_rate = ei.stir_rate
            deck.mini_heater_stirrer.start_stirring()
            time.sleep(20)
            turbidity_monitoring_start_time = background_monitor.tm.turbidity_monitoring_start_time
            set_first_solvent_tracking_data(solvent_name=solvent.name,
                                            volume_added=ei.target_initial_volume,
                                            total_volume=ei.target_volume,
                                            start_time=turbidity_monitoring_start_time)
            save_results()
            while True:
                # wait until vial becomes dissolved or start_next_vial is changed through slack
                if clean_up_vial is False:
                    clean_up_vial = ei.dissolved
                    start_next_vial = ei.dissolved
                    time.sleep(30)
                else:
                    if background_monitor.running:
                        background_monitor.stop_background_monitoring()
                        #check if not enough solid were added
                        # if ei.dissolved and ei.target_volume <= (
                        #         first_solvent_initial_volume + ei.solvent_addition_volume):
                        #     slack_manager.post_slack_message(
                        #         f'the initial solid dissolved in the initial solvent '
                        #         f'{solvent.current_volume} mL left in the stock vial')
                        #     can_safely_add_solvent = False
                        #     adjust_solid_ratio=1.5
                        #     solid_added= add_solid(ei.target_solid_mass*(adjust_solid_ratio-1)*1000)
                            # slack_manager.post_slack_message(
                            #     f'{solid_added} g more was added')
                            #update in results
                            # ei.solid_mass+=solid_added
                            # ei.target_solid_mass = ei.target_solid_mass*adjust_solid_ratio
                            # solid_weight = solid_weight * adjust_solid_ratio  # g
                            # solid_weight_g = solid_weight
                            # save_results()
                            # can_safely_add_solvent = True
                            # clean_up_vial=False
                            # background_monitor.start_background_monitoring()
                            # background_monitor.experiment_information.dissolved = False
                            # pause_addition_bool = True
                            # time.sleep(180)
                            # pause_addition_bool = False
                            # background_monitor.tm.state = TurbidityMonitor.unstable_state  # one of self.states
                            # background_monitor.time_out_ref = datetime.now()
                            # continue
                    can_safely_add_solvent = False

                    save_results()

                    # clean up

                    slack_manager.post_slack_message('Cleaning up this vial now')
                    deck.mini_heater_stirrer.stop_stirring()

                    # measure final volume with vision_needle_check, save image in liquid level folder, add calculated volume to results

                    save_results()

                    if ei.dissolved is True:
                        logger.info('solid dissolved')
                        slack_manager.post_slack_message(
                            'Finished testing solubility with vision, transferring sample to Kinova deck')
                        full_vial_mass, full_vial_mass_quantos = vision_clean_up(
                            vision_index=vision_station_monitor_index,
                            rxn_index=vial_index)
                        row, column = deck_consumables.get_kinova_hplc_tray_info(solid=ei.solid,
                                                                                 solvent=ei.solvent.name,
                                                                                 solubility=ei.solubility,
                                                                                 index=vial_index)
                        # print(row, column)
                        # threading.Thread(target=kinova_deck_manager.take_hplc_sample_from_n9,
                        #                  args=(int(row), int(column))).start()
                        # Parisa please check
                        # new_data = {}
                        # new_data['solute'] = ei.solid
                        # new_data['solvent'] = ei.solvent
                        # new_data['solubility'] = ei.solubility
                        # record['injection number '+str(injection_number)] = new_data
                        # injection_number += 1
                        # slack_manager.post_slack_message(str(record))
                        # w = csv.writer(open('record.csv','w'))
                        # for key, val in record.items():
                        #     w.writerow([key,val])

                        # next_empty_column -= 1
                        # if next_empty_column == 0:
                        #    next_empty_column = 6
                        #    next_empty_row -= 1
                    else:
                        full_vial_mass, full_vial_mass_quantos = vision_clean_up(
                            vision_index=vision_station_monitor_index,
                            rxn_index=vial_index)

                    ei.final_mass = full_vial_mass_quantos
                    ei.final_mass_zetascale = full_vial_mass

                    save_results()

                    # update data
                    ei.end_time = datetime.now()
                    end_time = ei.end_time.strftime(d.string_format)
                    logger.info(f'update data? end? end_time: {end_time}')

                    # save data
                    save_results()
                    # slack_manager.post_slack_file(str(results_path), "Results so far", "Results so far")

                    # creates video from images
                    folder_of_images_to_video(str(camera_images_folder),
                                              video_path,
                                              fps=fps,
                                              display_image_name=True)
                    # send slack message update
                    solvent_name = solvent.name

                    # update parameters for next experiment
                    camera.disconnect()
                    del camera
                    del tm
                    del graph_path
                    camera = None
                    tm = None
                    graph_path = None

                    print('target_solid_mass: ', ei.target_solid_mass)
                    print('first solvent initial volume: ', first_solvent_initial_volume)
                    if ei.dissolved is False and ei.target_volume >= (ei.maximum_volume - ei.solvent_addition_volume):
                        # did not dissolve, so decrease solid in the next run, and increase solvent initial volume. in
                        # other words, the solubility calculated is the too high
                        adjust_solid_ratio = 0.5  # what fraction of the solid weight should be used for the next run
                        solid_weight = solid_weight * adjust_solid_ratio
                        if solid_weight < 0.003:
                            ei.solubility_accuracy_note = "insoluble"
                            use_next_solvent()

                        # set the note that the solubility should be lower than what is recorded
                        ei.solubility_accuracy_note = "solubility should be lower"
                    # elif ei.dissolved and ei.target_volume <= (first_solvent_initial_volume + ei.solvent_addition_volume):
                    # not enough solid was added, should add more solid in the next run (everything dissolved within
                    # the initial volume of solvent and a single step addition), in other words, the solubility
                    # calculated is too low
                    #   adjust_solid_ratio = 1.4  # what fraction of the solid weight should be used for the next run
                    #  solid_weight = solid_weight * adjust_solid_ratio  # g
                    # solid_weight_g = solid_weight
                    # # adjust the initial solvent volume so that with the new solid weight the initial solvent volume
                    # # will make it so that the solubility achieved in this run will be reached in the initial
                    # # solvent dispense (as the actual solubility should be higher than what was found)
                    # assumed_solubility = ei.target_solid_mass / ei.target_volume
                    # solvent_initial_volume = round(solid_weight / assumed_solubility, 2)
                    # set the note that the solubility should be higher than what is recorded
                    # ei.solubility_accuracy_note = "solubility should be higher"
                    elif ei.dissolved and ei.target_volume <= (
                            first_solvent_initial_volume + ei.solvent_addition_volume):
                        if ei.target_solid_mass >= (0.5 * first_solvent_initial_volume):
                            print('yeah! im in case 1')
                            test_info.add_test(solvent_name)
                            # solubility exceeds 0.5g/ml, mark as 'very soluble, stop adding more solid", stop increase solid mass
                            # adjust nothing
                            ei.solubility_accuracy_note = "solubility exceeds 0.5g/ml, very soluble"
                            # vial_from_tray(rxn_index=vial_index,capped=True)
                            # vial_to_tray(rxn_index='A1',capped=True)
                            # threading.Thread(target=take_hplc_sample, args=(next_empty_row, next_empty_column)).start()
                        else:
                            # not enough solid was added, should add more solid in the next run (everything dissolved within
                            # the initial volume of solvent and a single step addition), in other words, the solubility
                            # calculated is too low
                            adjust_solid_ratio = 1.4  # what fraction of the solid weight should be used for the next run
                            solid_weight = solid_weight * adjust_solid_ratio  # g
                            # # # NEW
                            # if solid_weight > (0.5*first_solvent_initial_volume):
                            #     solid_weight = 0.5*first_solvent_initial_volume
                            solid_weight_g = solid_weight
                            # # adjust the initial solvent volume so that with the new solid weight the initial solvent volume
                            # # will make it so that the solubility achieved in this run will be reached in the initial
                            # # solvent dispense (as the actual solubility should be higher than what was found)
                            # assumed_solubility = ei.target_solid_mass / ei.target_volume
                            # solvent_initial_volume = round(solid_weight / assumed_solubility, 2)
                            # set the note that the solubility should be higher than what is recorded
                            ei.solubility_accuracy_note = "solubility should be higher"
                    elif ei.dissolved and ei.target_solid_mass >= (0.5 * first_solvent_initial_volume):
                        print('yeah! im in case 2')
                        test_info.add_test(solvent_name)
                        # solubility exceeds 0.5g/ml, mark as 'very soluble, stop adding more solid", stop increase solid mass
                        # adjust nothing
                        ei.solubility_accuracy_note = "solubility exceeds 0.5g/ml, very soluble"
                        # vial_from_tray(rxn_index=vial_index,capped=True)
                        # vial_to_tray(rxn_index='A1',capped=True)
                        # threading.Thread(target=take_hplc_sample, args=(next_empty_row, next_empty_column)).start()
                    elif ei.dissolved is True:
                        test_info.add_test(solvent_name)
                        # vial_from_tray(rxn_index=vial_index,capped=True)
                        # vial_to_tray(rxn_index='A1',capped=True)
                        # threading.Thread(target=take_hplc_sample, args=(next_empty_row, next_empty_column)).start()
                        # if dissolved adjust solvent addition parameters to get accurate solubility
                        solvent_initial_volume = round(ei.target_volume - (1.6 * ei.solvent_addition_volume), 2)

                        solvent_addition_volume += solvent_addition_increment
                        if solvent_addition_volume <= 0.01:
                            solvent_addition_volume = 0.01
                        solvent_addition_volume = round(solvent_addition_volume, 3)
                    elif ei.dissolved is False:
                        # note, if forced to clean up vial, dont update any experiment parameters for next experiment
                        ei.solubility_accuracy_note = 'solubility is inaccurate, run was ended early'

                    save_results()
                    if solvent_initial_volume > 0.85:
                        solvent_initial_volume = 0.85
                    if solvent_initial_volume < first_solvent_initial_volume:
                        solvent_initial_volume = first_solvent_initial_volume

                    assumed_solubility = ei.target_solid_mass / ei.target_volume
                    message = f'For experiment {ei.experiment_name}:\n' \
                              f'Vial index: {ei.vial_index}\n' \
                              f'Dissolved: {ei.dissolved} \n' \
                              f'Solubility accuracy notes: {ei.solubility_accuracy_note},\n' \
                              f'Solubility (g/mL): {ei.solubility}\n' \
                              f'Mass solid (g): {ei.solid_mass}\n' \
                              f'Solid dispense percent error (%) {ei.solid_percent_error}\n' \
                              f'{solvent_name} volume (mL): {ei.volume}\n' \
                              f'Solvent dispense percent error (%) {ei.solvent_percent_error}\n' \
                              f'{solvent_name} initial volume (mL): {ei.target_initial_volume}\n' \
                              f'{solvent_name} target volume (mL): {ei.target_volume}\n' \
                              f'Target mass solid (g): {ei.target_solid_mass}\n'
                    slack_manager.post_slack_message(message)

                    # note, if forced to clean up vial, dont update any experiment parameters for next experiment
                    s = choose_solvent()
                    if s is not None:
                        next_name = s.name
                    else:
                        next = '-- no more solvents to test_tecan_cavro.py --'
                    stir_rate = ei.stir_rate
                    slack_manager.post_slack_message(
                        f'The next mass of solid to test_tecan_cavro.py is {solid_weight} g, and the '
                        f'next initial volume of {next_name} is '
                        f'{solvent_initial_volume} mL. The volume of solvent to be added '
                        f'every subsequent dispense in {solvent_addition_volume} mL')
                    solvent_percent_error_limit = 20
                    if ei.solvent_percent_error >= solvent_percent_error_limit:
                        start_next_vial = False
                        slack_manager.post_slack_message(f'For the last vial, the solvent percent error, '
                                                         f'{ei.solvent_percent_error} is greater than the set limit of '
                                                         f'{solvent_percent_error_limit} so i will not start the next '
                                                         f'vial. If you want to try with another solvent, send the "use next solvent" '
                                                         f'command, and then send the "start next vial" command')

                    if start_next_vial:
                        rest_t = 2
                        slack_manager.post_slack_message(
                            f'I will start the next vial in {rest_t} minutes. If you do not want me '
                            f'to send "dont start next vial"')

                        time.sleep(60 * rest_t)
                    else:
                        slack_manager.post_slack_message(f'I will not start the next vial until you tell me to')

                    break

        else:
            # dont start the next vial yet classes to
            time.sleep(60)

    slack_manager.post_slack_file(str(results_path), "Results", "Results")

    # move separate classes to new file
    # organize methods to classes
    # which parts can be used for other workflows?
    # managers: classes that use decks and sequences and components for high level methods
    # experiment class: will interact with the data/ties all necessary classes
    # campaign: iterations of experiment
