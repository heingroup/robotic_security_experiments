import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server
from niraapad.shared.utils import MO
from niraapad.shared.utils import BACKENDS
from ftdi_serial import Serial
NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})


from n9.configuration import deck_consumables
from n9.experiments.crystal_solubility_profiling.crystal_profiling_preparation_A import SolutionPreparation, \
    reload_for_next_tube, TubeVialSolutionTransfer, prep_centrifugation, cleanup_filter, weigh_filter
from n9.experiments.crystal_solubility_profiling.experiment_info_saving import ExperimentInformation, \
    create_experiment_folder, Results

# logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.INFO)
# from n9.experiments.pass_vial_gateway import kinova_deck_manager

if __name__ == '__main__':
    """
    - make sure vision station is open
    - update tube_info.csv # update tube_info.csv, make sure have total_tubes+2*total_trials tubes available
    - update vial_info.csv # update vial_info.csv, make sure have total_tube + total_trials available
    - update parameters if necessary
    """
    ###############################################################
    # Parameters
    SOLID = 'NACL'#'MD204'  # 'AJK2098'
    SOLVENT = "ethylAcetate"  # 'ethyl acetate'
    FINAL_SOLVENT = 'THF'
    WASHES_PER_TRAIL = 1
    DISPENSE_VOLUME = 0.1  # mL

    SOLID_WEIGHT = 10  # mg
    STIR_RATE = 1500
    TARGETED_STIR_DURATION = 10  # s
    CENTRIFUGE_DURATION = 5 # s
    TOTAL_TRAILS = 1

    ################################################################
    EXP_TYPE = 'crystal dissolution'
    SOLID_TYPE = SOLID

    ###############################################################
    PATH = create_experiment_folder(EXP_TYPE, SOLID_TYPE)
    ei = ExperimentInformation(solid=SOLID,
                               solvent=SOLVENT,
                               final_solvent=FINAL_SOLVENT,
                               target_solid_mass=SOLID_WEIGHT,
                               target_solvent_volume=DISPENSE_VOLUME,
                               stir_rate=STIR_RATE,
                               target_stir_duration=TARGETED_STIR_DURATION,
                               path=PATH)
    results = Results()
    prep = SolutionPreparation(ei)

    #################################################################
    for CURRENT_TRIAL in range(TOTAL_TRAILS):
        ei.trial = CURRENT_TRIAL + 1
        tube_index = prep.prepare_filter()
        # tube_index = 'A1'
        for CURRENT_WASH in range(WASHES_PER_TRAIL):
            ei.wash = CURRENT_WASH
            prep.prepare_tubes()

            if CURRENT_WASH > 0:
                clean_up = TubeVialSolutionTransfer(ei, tube_index)
                clean_up.transfer_solution_tube_to_vial()
                results.save_results(ei)
                # row, column = deck_consumables.get_kinova_hplc_tray_info(solid=ei.solid,
                #                                                          solvent=ei.solvent,
                #                                                          solubility=999,
                #                                                          index=ei.vial_index)
                #
                # # print(row, column + 1)
                # threading.Thread(target=kinova_deck_manager.take_hplc_sample_from_n9,
                #                  args=(int(row), int(column + 1))).start()

            ei.stir_duration = prep.count_to_stop_stirring()
            prep_centrifugation(CENTRIFUGE_DURATION)

            # If last of the trial
            if CURRENT_WASH == (WASHES_PER_TRAIL - 1):
                # cleanup_filter()
                weigh_filter(ei)
                reload_for_next_tube(clean_up_from_centrifuge=False)
            else:
                tube_index = reload_for_next_tube()

        prep.prepare_tubes(last=True)

        clean_up = TubeVialSolutionTransfer(ei, tube_index)
        clean_up.transfer_solution_tube_to_vial()
        ei.wash = ei.wash + 1
        results.save_results(ei)
        # row, column = deck_consumables.get_kinova_hplc_tray_info(solid=ei.solid,
        #                                                          solvent=ei.solvent,
        #                                                          solubility=999,
        #                                                          index=ei.vial_index)
        #
        # # print(row, column + 1)
        # threading.Thread(target=kinova_deck_manager.take_hplc_sample_from_n9,
        #                  args=(int(row), int(column + 1))).start()
        # ei.clear_data()
        ei.wash = None

        ei.stir_duration = prep.count_to_stop_stirring()
        prep_centrifugation(CENTRIFUGE_DURATION)

        cleanup_filter()
        clean_up = TubeVialSolutionTransfer(ei, tube_index)
        clean_up.transfer_solution_tube_to_vial()
        # row, column = deck_consumables.get_kinova_hplc_tray_info(solid=ei.solid,
        #                                                          solvent=ei.solvent,
        #                                                          solubility=999,
        #                                                          index=ei.vial_index)
        #
        # # print(row, column + 1)
        # threading.Thread(target=kinova_deck_manager.take_hplc_sample_from_n9,
        #                  args=(int(row), int(column+1))).start()
        results.save_results(ei)
    print('Complete')
