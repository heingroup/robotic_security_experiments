from ur.components.ur3_centrifuge import ur3_centrifuge
from n9.configuration import deck
from ur.configuration import ur_deck
from n9.experiments.reaction import dump_needle,uncap_needle,wash_dosing_line
# remove cover
deck.needle_tray.reset()
cover_status = ur_deck.ur3_centrifuge.pickup_cover_from_centrifuge()
if cover_status is not "off":
    ur_deck.ur3_centrifuge.place_cover_in_safe_location()
# rotate centrifuge to correct location
ur_deck.ur3_centrifuge.rotate_centrifuge()
# move vial to centrifuge
deck.mini_heater_stirrer.target_stir_rate=500
deck.mini_heater_stirrer.start_stirring()
deck.needle_tray.pickup()
uncap_needle()
deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
# go to vision safe height
location = deck.vision_grid_needle.locations['A1']
safe_location = location.copy(z=300)
pierce_location = location.copy(z=154)
deck.n9.move_to_location(safe_location, gripper=109, probe=True)
#deck.dosing_pump.move_relative_ml(0.1,velocity_ml=5)
#draw air
#draw slurry
deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
deck.dosing_pump.move_relative_ml(0.05,velocity_ml=0.2)
#safeheigh
deck.n9.move_to_location(safe_location, gripper=109, probe=True)
# cent tube safe height
deck.n9.move(x=-291.0, y=72.6, z=300, gripper=88.96199999999999, probe=True)

# cent tube down
deck.n9.move(x=-290.9917358398437, y=72.62027587890626, z=121.32337646484376, gripper=88.971, probe=True)

#dispense
deck.dosing_pump.move_absolute_ml(0,velocity_ml=5)
#safe
deck.n9.move(x=-291.0, y=72.6, z=300, gripper=88.96199999999999, probe=True)

dump_needle()
deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
ur_deck.ur3_centrifuge.pickup_vial_from_transfer_location()
ur_deck.ur3_centrifuge.place_vial_in_centrifuge()
# place cover on centrifuge
ur_deck.ur3_centrifuge.pickup_cover_from_safe_location()
ur_deck.ur3_centrifuge.place_cover_on_centrifuge()
# run centrifuge
ur_deck.ur3_centrifuge.spin_centrifuge(20)
# remove cover
ur_deck.ur3_centrifuge.pickup_cover_from_centrifuge()
ur_deck.ur3_centrifuge.place_cover_in_safe_location()
# rotate centrifuge to correct location
ur_deck.ur3_centrifuge.rotate_centrifuge()
# move vial to transfer location
ur_deck.ur3_centrifuge.pickup_vial_from_centrifuge()
ur_deck.ur3_centrifuge.place_vial_in_transfer_location()
#