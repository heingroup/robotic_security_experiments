import os

bot_token = os.environ.get('SLACK_BOT_TOKEN')  # lucky bot token
bot_name = os.environ.get('SLACK_BOT_NAME', 'Lucky')
channel_name = os.environ.get('SLACK_BOT_CHANNEL', '#n9_solubility_updates')
channel_id = 'C013VCLQ0KH'  # todo where to place?