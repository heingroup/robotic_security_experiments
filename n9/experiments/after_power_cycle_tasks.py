#import niraapad.backends # Required so that Niraapad's classes are used instead
#from niraapad.lab_computer.niraapad_client import NiraapadClient
#NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server
#from niraapad.shared.utils import MO
#from niraapad.shared.utils import BACKENDS
#NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})
#

from n9.configuration import deck
import logging
import time
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info(f'Homing pumps...')
#deck.sample_pump.home()
deck.dosing_pump.home()
#deck.push_pump.home()
logger.warning(f'PRE-FLIGHT CHECKS: MAKE SURE GRIPPER IS CLEAN AND EMPTY')
time.sleep(0.5)
#input('Press any k1ey to continue and tare the scale')
#deck.scale.tare()
print('done')
