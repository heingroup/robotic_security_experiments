import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server
from niraapad.shared.utils import MO
from niraapad.shared.utils import BACKENDS
NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})
import time
import logging
from north_robots.components import Location, GridTray
from north_c9.controller import C9Controller
from north_robots.n9 import N9Robot
logging.basicConfig(level=logging.WARN)



c9 = C9Controller(device_serial='AM006376',use_joystick=True, debug_protocol=True)
n9 = N9Robot(c9)
c9.home()
#c9.home(0,1,2,3)


def position_and_gripper():
    while True:
        pos = c9.joystick.record_position('Move the N9 to a position and press the OPTIONS button',
                                               use_probe=False
                                          )
        print(f'Position: x={pos[0]}, y={pos[1]}, z={pos[2]}, gripper={pos[3]}')

def position_and_position_difference():
    last_pos = None
    while True:
        pos_vector = c9.joystick.record_position_vector('Move the N9 to a position and press the OPTIONS button',
                                                             use_probe=False)
        print(f'Position: {pos_vector}')

        if last_pos is not None:
            location = Location.location_from_origin_and_x_vector(pos_vector, last_pos)
            print(f'Location: {location}')

        last_pos = pos_vector


if __name__ == "__main__":
    position_and_gripper()

#engage pick up from tray
#Position: x=-199.4, y=258.4, z=131.46093994140625, gripper=161.26599999999996

#engage gripper passive
#Position: x=-241.77572021484374, y=117.01318359375, z=110.822265625, gripper=52.64699999999999


#cap engage gripper passive
#Position: x=-243.06419677734374, y=118.0561767578125, z=116.28228759765625, gripper=150.57800000000003

#push cap down
#Position: x=-243.1, y=120.55281982421874, z=123.32718017578125, gripper=150.60699999999997

#needle in probe true
#Position: x=-238.6516845703125, y=117.71470947265624, z=162.5815673828125, gripper=150.62199999999996

#unfilter, put filter in a location, both engage
#Position: x=-187.92928466796874, y=-13.78116455078125, z=112.7120361328125, gripper=150.664
#Position: x=-181.07071533203126, y=115.65532226562499, z=103.40244384765623, gripper=150.63400000000001
