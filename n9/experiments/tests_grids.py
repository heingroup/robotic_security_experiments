
from n9.configuration import deck
from heinsight.vision_utilities.camera import Camera
from n9.experiments.station_robot_engagement import vial_to_gripper, vial_from_gripper


import time
import datetime
tube_positions=stock_vial_positions = ['A1', 'A2', 'A3', 'A4','A5','A6'
                            'B1', 'B2', 'B3', 'B4','B5','B6']


stock_vial_positions = ['A1', 'A2',
                        'B1', 'B2',
                        'C1', 'C2',
                        'D1', 'D2',
                        'E1', 'E2',]

reaction_vial_positions = [
                            'A1', 'A2', 'A3', 'A4','A5',
                            'B1', 'B2', 'B3', 'B4','B5',
                            'C1', 'C2', 'C3', 'C4','C5',
                          'D1', 'D2', 'D3', 'D4','D5']

vision_vial_positions = ['A1', 'B1']

#camera = Camera(cam=1,
#                save_folder_bool=True,
#                save_folder_location='D:/experiments/',
#                save_folder_name='hi',
#                )


def take_photo_from_to(file_path : str = 'D:/experiments', rxn_index: str = 'A1', from_gripper: bool = True,
                       to_gripper: bool = True):
    """
    this function brings the vial from gripper or reaction tray to vision_needle_check station, takes photo and takes it back
     to gripper or reaction tray
    :param file_path: where the photos will be saved
    :param rxn_index: in case moving to reaction tray, which location on it the vial is/ will be placed
    :param from_gripper: if true, the vial is taken to vision_needle_check station from the gripper, if false, taken from the tray
                            (from rxn_index location)
    :param to_gripper: if true, the vial is taken back from vision_needle_check station to the gripper after photo, if false, taken
                        back to the tray (to rxn_index location)
    :return:
    """
    #cam = Camera(cam=1,
    #            save_folder_bool=True,
    #            save_folder_location=file_path,
    #            save_folder_name='hi',
    #        )

    location = deck.reaction_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    grip_cap_location = location.copy(z=148.1)

    if from_gripper:
        # open scale gripper
        deck.c9.output(1, False)
        # open arm gripper
        deck.c9.output(0, False)
        # go halfway to avoid bumping to Quantos
        deck.n9.move(x=-167.73187255859375, y=88.71137695312501, z=300, gripper=109)
        # # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62099999999998)
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=120.05, gripper=150.62099999999998)
        # arm gripper closes around the cap
        deck.c9.output(0, True)
        # scale gripper open around vial
        deck.c9.output(1, False)
        time.sleep(4)
        # gripper goes up to safe height
        deck.n9.move(x=-186.6, y=-13.9, z=250, gripper=150.62099999999998)
        # go halfway to avoid bumping to Quantos
        deck.n9.move(x=-167.73187255859375, y=88.71137695312501, z=300, gripper=109)
    else:

        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=False)

        deck.n9.move_to_location(grip_cap_location)
        deck.c9.output(0, True)
        deck.n9.move_to_location(safe_location, probe=False)
    # to black background at safe height
    deck.n9.move(x=-265.7755981445313, y=211.92225341796876, z=300, gripper=-3.38900000000001)
    # down to black background
    deck.n9.move(x=-265.7755981445313, y=211.92225341796876, z=159, gripper=-3.38900000000001)
    # stabilize the movement before taking photo
    time.sleep(2)
    #cam.take_picture()
    # to black background at safe height
    deck.n9.move(x=-265.7755981445313, y=211.92225341796876, z=300, gripper=-3.38900000000001)
    if to_gripper:
        # go halfway to avoid bumping to Quantos
        deck.n9.move(x=-167.73187255859375, y=88.71137695312501, z=250, gripper=109)
        # # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=186.005, gripper=150.62099999999998)
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=120.05, gripper=150.62099999999998)
        # # scale gripper closes around vial
        deck.c9.output(1, True)
        # arm gripper opens around the cap
        deck.c9.output(0, False)
        # gripper goes up to safe height
        deck.n9.move(x=-186.6, y=-13.9, z=250, gripper=150.62099999999998)
    else:
        deck.n9.move_to_location(safe_location, probe=False)
        deck.n9.move_to_location(grip_cap_location)
        deck.c9.output(0, False)
        deck.n9.move_to_location(safe_location, probe=False)


def test_stock_grid():
    for i, val in enumerate(stock_vial_positions):
        location = deck.stock_grid.locations[val]
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=True,gripper=-90)
        deck.n9.move_to_location(location, probe=True,gripper=-90)
        deck.n9.move_to_location(safe_location, probe=True,gripper=-90)


def test_station_grid():
    for i, val in enumerate(vision_vial_positions):
        location = deck.vision_grid.locations[val]
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=False)
        deck.n9.move_to_location(location, probe=False)
        deck.c9.output(0,True)
        deck.n9.move_to_location(safe_location, probe=False)
        deck.n9.move_to_location(location, probe=False)
        deck.c9.output(0,False)
        deck.n9.move_to_location(safe_location, probe=False)


def test_reaction_grid():
    for i, val in enumerate(reaction_vial_positions):
        location = deck.reaction_grid_gripper.locations[val]
        pierce_location=location.copy(z=220)
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=True, probe_offset=41.5)
        deck.n9.move_to_location(pierce_location, probe=True, probe_offset=41.5)
        deck.n9.move_to_location(safe_location, probe=True, probe_offset=41.5)


def test_reaction_grid_gripper(capped: bool = True, to_quantos: bool= False,
                               to_vision_station: bool = False, back_to_tray: bool = True,filter_cup:bool=True):

    for i, val in enumerate(reaction_vial_positions):
        location = deck.hplc_vial_tray.locations[val]
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=False)
        if capped:
            #185 for epindorf
            #171 cap
            grip_cap_location = location.copy(z=143)
        if filter_cup:
            grip_cap_location = location.copy(z=103)
        else:
            grip_cap_location = location.copy(z=153)
        # z = 148.1
        deck.n9.move_to_location(grip_cap_location)
        deck.c9.output(0, True)
        deck.n9.move_to_location(safe_location, probe=False)

        if to_quantos:
            # go to safe middle at safe height
            deck.n9.move(x=-225.7, y=41.04, z=156.6, gripper=52.61)
            #go to middle safe location lower
            deck.n9.move(x=-225.7, y=41.04, z=156.6, gripper=52.61)
            #goinside quntos and above
            deck.n9.move(x=-225.7, y=-186.36, z=156.6, gripper=52.613)
            #go inside quantos and inside vial holder
            deck.n9.move(x=-225.7, y=-186.36, z=140, gripper=52.613)
            #ungrip vial
            deck.c9.output(0,False)
            #go up
            deck.n9.move(x=-225.7, y=-186.36, z=156.6, gripper=52.613)
            # go to safe middle
            deck.n9.move(x=-225.7, y=41.04, z=156.6, gripper=52.61)
            #go up on that location to safe height
            deck.n9.move(x=-225.7, y=41.04, z=270, gripper=52.61)

        if to_vision_station:
            #go to vision_needle_check station at safe height
            deck.n9.move(x=-198.87, y=179.91, z=270, gripper=-91.24)

            # down to station
            deck.n9.move(x=-198.87, y=179.91, z=137.05, gripper=-91.24)

            # ungrip vial
            deck.c9.output(0,False)

            #go up at safe height
            deck.n9.move(x=-198.87, y=179.91, z=270, gripper=-91.24)

        if back_to_tray:
            deck.n9.move_to_location(safe_location, probe=False)
            deck.n9.move_to_location(grip_cap_location)
            deck.c9.output(0, False)
            deck.n9.move_to_location(safe_location, probe=False)

def test_relocate_vial(capped: bool = True, to_quantos: bool= False,
                               to_vision_station: bool = False, back_to_tray: bool = True):
    A1 = deck.reaction_grid_gripper.locations['A1']
    safe_A1 = A1.copy(z=300)
    column = 6
    row = 4
    for i, val in enumerate(reaction_vial_positions):
        location = deck.reaction_grid_gripper.locations[val]
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location, probe=False)
        if capped:
            #185 for epindorf
            #171 cap
            grip_cap_location = location.copy(z=143)
        else:
            grip_cap_location = location.copy(z=137)
        # z = 148.1
        deck.n9.move_to_location(grip_cap_location)
        deck.c9.output(0, True)
        deck.n9.move_to_location(safe_location, probe=False)



        if back_to_tray:
            deck.n9.move_to_location(safe_A1, probe=False)
            deck.n9.move_to_location(A1.copy(z=143))
            deck.c9.output(0, False)
            deck.n9.move_to_location(safe_A1, probe=False)
        from kinova.experiments.methods import take_hplc_sample

        take_hplc_sample(row,column)
        column -= 1
        if column == 0:
            column = 6
            row -= 1

def test_sampleomatic_reaaction_grid():
    deck.c9.output(0, False)
    # goes to sampleomatic head
    deck.n9.move(x=128.2, y=305.2, z=300, gripper=90, probe=False)
    deck.n9.move(x=129.8, y=305.5, z=171.4, gripper=90, probe=False)
    # grips the sampleomatic head
    deck.c9.output(0, True)
    # bring the sampleomatic head up
    deck.n9.move(x=129.8, y=305.5, z=270, gripper=90, probe=False)
    deck.n9.gripper.move_degrees(180)
    for i, val in enumerate(reaction_vial_positions):
        location = deck.reaction_grid_sampleomatic.locations[val]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, probe=True, probe_offset=43.5)
        deck.n9.move_to_location(location, probe=True,probe_offset=43.5)
        deck.n9.move_to_location(safe_location, probe=True,probe_offset=43.5)
    # brings the sampleomatic back to stage
    deck.n9.move(x=128.2, y=305.2, z=275, gripper=90)
    deck.n9.move(x=129.8, y=305.5, z=171.4, gripper=90)
    # ungrips the sampleomatic head
    deck.c9.output(0, False)
    deck.n9.move(x=128.2, y=305.2, z=275, gripper=90)

def test_tube_grid_gripper():

    for i, val in enumerate(tube_positions):
        location = deck.tube_grid_gripper.locations[val]
        safe_location = location.copy(z=300)
        # deck.n9.gripper.move_degrees(180)
        deck.n9.move_to_location(safe_location,gripper=-136.9, probe=False)
        deck.n9.move_to_location(location)
        deck.c9.output(0, True)
        deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)
        deck.n9.move_to_location(location,gripper=-136.9, probe=False)
        deck.c9.output(0, False)
        deck.n9.move_to_location(safe_location,gripper=-136.9, probe=False)


if __name__ == '__main__':
    deck.c9.output(0, False)
    test_reaction_grid_gripper()
    #test_stock_grid()
    #deck.c9.output(0,False)
    #test_stock_grid()
    #test_reaction_grid_gripper(filter_cup=True)
    #test_station_grid()
    #test_reaction_grid()
    #camera.take_picture()
    #deck.c9.output(0,False)
    #test_relocate_vial()

