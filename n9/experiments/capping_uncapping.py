from datetime import datetime, timedelta

from n9.configuration import deck
from north_c9.controller import C9Controller
from north_robots.components import Location
import time
import logging
logger = logging.getLogger(__name__)


def is_vial_on_scale(scale,
                     vial_mass: float):
    """
    Check if there is a vial on the scale
    :param scale: an object of weighing station
    :param vial_mass: an expected mass of a vial
    :return:

    """

    logger.info('PRE-FLIGHT CHECKS: Checking the scale')
    while True:
        scale_reading = deck.scale.weigh()
        if not (scale_reading <= vial_mass):
            logger.warning(f'PRE-FLIGHT CHECKS: There is something on the scale - '
                           f'please remove before continuing')
            logger.warning(f'PRE-FLIGHT CHECKS: SCALE IS EMPTY [FAILED]')
            time.sleep(0.5)
            input('Press any key to continue')
        else:
            logger.info(f'PRE-FLIGHT CHECKS: SCALE IS EMPTY (reading {scale_reading}) [PASSED]')
            break


def is_something_on_scale():
    """
    Check if there is a vial on the scale
    :param scale: an object of weighing station
    :return:
    """
    logger.info('PRE-FLIGHT CHECKS: Checking the scale')
    # open scale gripper
    deck.c9.output(1, False)
    scale_reading = deck.scale.settled_weigh(matching=2)
    if not (scale_reading <= 0.05):
        logger.warning(f'PRE-FLIGHT CHECKS: There is something on the scale - '
                       f'please remove before continuing')
        logger.warning(f'PRE-FLIGHT CHECKS: SCALE IS EMPTY [FAILED]')
        time.sleep(0.5)
        input('CLEAR SCALE TOP before Pressing any key to continue')
    else:
        logger.info(f'PRE-FLIGHT CHECKS: SCALE IS EMPTY (reading {scale_reading}) [PASSED]')


def current_mass():
    """
    Outputs the current stable mass read out
    a direct reading is a str in the form 'mass units OK'
    OK is at end of string if weight is stable
    Example: '0.0268 g OK' where 0.0268 is the mass, 'g' is grams
    """
    final_weight = 0
    logger.info('WEIGH SCALE: begin weigh')
    data_list = []
    test_count = 0
    while test_count < 10:  # will weigh up to 10 times to get two similar weights
        test_count += 1
        weight = deck.scale.weigh()

        logger.info(f'WEIGH SCALE: weigh #{test_count}  - scale returned: {weight} g')
        if weight == 'OL':
            return 1000

        if len(data_list) > 0:  # need at least two values to compare
            found_match = False
            for data in data_list:
                if abs(data - weight) < 0.005:
                    found_match = True
                    data_list.append(weight)
                    logger.info('WEIGH SCALE: weights agree within 0.005 g - proceeding')
                    break
            if found_match:
                break
            else:
                logger.warning(f'WEIGH SCALE: measured values ({data_list})do not agree - remeasuring after 2 secs')
                time.sleep(2)
        data_list.append(weight)
    final_weight = data_list[-1]
    return final_weight


def uncap(controller: C9Controller, pitch_mm=2.0, rotations=1.5,velocity:int=None,acc:int=None):
    controller.move({0: -rotations * 360, 3: pitch_mm * rotations}, relative=True, units=True,velocity=velocity,acceleration=acc)
    return controller.cartesian_position()


def recap(controller: C9Controller, pitch_mm=2.0, rotations=1.5):
    controller.move({0: rotations * 360, 3: -pitch_mm * rotations}, relative=True, units=True)


def uncap_from_tray(rxn_index='A1',from_tray: bool=False, scale_check= False):
    # deck.n9.velocity_counts=40000
    # deck.n9.aceration_countscel = 80000
    if from_tray:
        # open scale gripper
        deck.c9.output(1, False)
        #open arm gripper too
        deck.c9.output(0, False)
        if scale_check:
            if deck.scale.settled_weigh(matching=2) > 0.05:
                action = input(print("gripper not empty, stop!"))
        # go to first vial on the trey in safe height Position: x=-193.2, y=260.3, z=121.505, gripper=150.62099999999998
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location)
        grip_cap_location = location.copy(z=143.84)
        deck.n9.move_to_location(grip_cap_location)
        # # gripper closes around vial's cap
        deck.c9.output(0, True)
        # # bring it to safe height Position: x=-193.2, y=260.3, z=186.005, gripper=150.61699999999996
        deck.n9.move_to_location(safe_location)
    # # go to scale still on safe height
    location = deck.BALANCE_GRIPPER
    safe_location = Location(location.x, location.y, 310)
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)
    # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
    deck.n9.move_to_location(location, gripper=150.6, probe=False)
    # Eleanor todo: delete if needed
    deck.c9.output(0, True)
    # time.sleep(2)
    deck.c9.output(1, False)
    # release gripper on arm
    # time.sleep(2)
    deck.c9.output(0, False)
    # time.sleep(2)
    # grab by gripper on arm
    deck.c9.output(0, True)
    time.sleep(1)
    ### END Eleanor edit
    # # scale gripper closes around vial
    deck.c9.output(1, True)
    if not from_tray:
        # arm grip around cap
        deck.c9.output(0,True)
    time.sleep(1)
    print("before uncap",deck.c9.axis_current(0))
    # # uncap the vial
    uncap(deck.c9, pitch_mm=1.1, rotations=2.5)
    print("after uncap",deck.c9.axis_current(0))
    # # bring the cap up
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)

    #check if the vial is still on
    # open scale with gripper
    deck.c9.output(1,False)
    time.sleep(5)
    # read weight
    if scale_check:
        if deck.scale.settled_weigh(matching=3)<1:
            print("no vial on scale!")
    # close scale with the gripper
    deck.c9.output(1,True)


def recap_to_tray(to_tray: bool = False, rxn_index='A1',threshold_force: float = 800, scale_check=False):
    # before closing gripper, record vial mass
    deck.c9.output(1, False)
    if scale_check:
        vial_weight = deck.scale.settled_weigh(matching=3)
        while vial_weight<0.1:
            vial_weight = deck.scale.settled_weigh(matching=3)
    # make sure scale gripper is close
    deck.c9.output(1, True)
    # safe height above gripper
    location = deck.BALANCE_GRIPPER
    safe_location = Location(location.x,location.y,310)
    deck.n9.move_to_location(safe_location, gripper=-209.03, probe=False)
    # go down to where the arm exactly ended up right after uncapping, to get ready to recap
    deck.n9.move_to_location(location, gripper=-209.3, probe=False)
    current=deck.c9.axis_current(0)
    print("before recap",deck.c9.axis_current(0))
    with open('gripper_read_data.csv', 'a+') as f:
        f.write(f'{current}, before recap\n')
    # put the cap back on 2.5 for tight enough corresponds to 1000
    recap(deck.c9, pitch_mm=1.1, rotations=2.2)
    # arm gripper force feedback 1000 is the average current value for tighten caps
    current=deck.c9.axis_current(0)
    print("after recap",current)
    watch=datetime.now()
    while abs(current) < threshold_force:
        if datetime.now()>watch+timedelta(0,150):
            logging.info("can't reach threshold")
            break
        recap(controller=deck.c9, pitch_mm=2, rotations=1/360)
        current = deck.c9.axis_current(0)
        time.sleep(0.01)
        print("current now is",current)
        with open('gripper_read_data.csv', 'a+') as f:
            f.write(f'{current}, after recap\n')
        if current == 0:
            print("home gripper!")
            break

    # open the scale gripper
    deck.c9.output(1, False)
    time.sleep(3)
    # bring the vial up
    deck.n9.move_to_location(safe_location, probe=False)
    time.sleep(6)
    if scale_check:
        print(deck.scale.settled_weigh(matching=2))
        # scale feedback check to make sure vial was picked up
        while deck.scale.settled_weigh(matching=3) > vial_weight-0.01:
            time.sleep(2)
            print(deck.scale.settled_weigh(matching=2))
            # make sure arm gripper is open
            deck.c9.output(0, False)
            # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
            deck.n9.move_to_location(location, gripper=150.6, probe=False)
            # arm gripper closes around vial
            deck.c9.output(0, True)
            # bring the vial up
            deck.n9.move_to_location(safe_location, gripper=-209.3, probe=False)
            time.sleep(3)
    if to_tray:
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, gripper=109, probe=False)
        grip_cap_location = location.copy(z=143.84)
        deck.n9.move_to_location(grip_cap_location, gripper=109, probe=False)
        # arm gripper opens around the vial cap
        deck.c9.output(0, False)
        deck.n9.move_to_location(safe_location, gripper=109, probe=False)


if __name__ == "__main__":
    deck.c9.output(1, False)
    deck.c9.output(0, False)
    reaction_vial_positions = ['A1', 'A2', 'A3', 'A4',
                               'B1', 'B2', 'B3', 'B4',
                               'C1', 'C2', 'C3', 'C4',
                               'D1', 'D2', 'D3', 'D4',
                               'E1', 'E2', 'E3', 'E4',
                               'F1', 'F2', 'F3', 'F4']
    for i, val in enumerate(reaction_vial_positions):
        uncap_from_tray(rxn_index=val, from_tray=True)
        park_cap()
        pickup_cap()
        deck.c9.output(1, False)
        time.sleep(5)
        deck.c9.output(1, True)
        recap_to_tray(rxn_index=val, to_tray=True)

