"""
For plotting a turbidity vs time graph with annotations of the graph based on when a liquid addition was made - for
robotic solubility/turbidity workflow. There will be a top x axes with units of estimated total liquid added so far
based on the liquid addition data

The data for when liquid additions were made is a csv file similar to the turbidity data csv file
"""
import os
from typing import Dict
from pathlib import Path
import json
import seaborn as sns
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
from heinsight.vision.turbidity import TurbidityMonitor
from heinsight.heinsight_utilities.temporal_data import TemporalData
from heinsight.heinsight_utilities.data_analysis_visualization import _colour_4_rgb, _colour_8_rgb

_ls_dash = '--'
_ls_dot = ':'

sns.set_style(
    style="ticks",
    rc={
        'axes.spines.right': False,
        'axes.spines.top': True,
    },
)

def graph_with_liquid_addition(turbidity_data_path: Path,
                               liquid_addition_path: Path,
                               save_path=Path.cwd().joinpath('turbidity liquid addition annotated graph.png'),
                               tm_parameters: Dict = None):
    """

    :param turbidity_data_path: path to either csv or json file with turbidity data. if csv then no reference line
        will be added to the graph. if json then reference lines will be added to the graph; '.csv' or '.json'
        must be included in the path
    :param liquid_addition_path: path to liquid addition csv file; '.csv' must be included in the path
    :param save_path: path of where to save final output graph as a png; '.png' must be included in the path
    :param tm_parameters: Dictionary with the following keys to set up a Turbidity monitor instance. Only required if
        you want to be able to graph stable regions on the turbidity vs time graph with specific parameters for what
        stable looks like. Dictionary should look like:
            tm_parameters = {
                'tm_n': tm_n,
                'tm_std_max': tm_std_max,
                'tm_sem_max': tm_sem_max,
                'tm_upper_limit': tm_upper_limit,
                'tm_lower_limit': tm_lower_limit,
                'tm_range_limit': tm_range_limit,
            }
    :return:
    """
    tm_path = Path.cwd().joinpath('turbidity liquid addition')
    tm = TurbidityMonitor(turbidity_monitor_data_save_path=str(tm_path),
                          datetime_format='%Y_%m_%d_%H_%M_%S_%f')
    if tm_parameters is not None:
        tm.n = tm_parameters['tm_n']
        tm.std_max = tm_parameters['tm_std_max']
        tm.sem_max = tm_parameters['tm_sem_max']
        tm.upper_limit = tm_parameters['tm_upper_limit']
        tm.lower_limit = tm_parameters['tm_lower_limit']
        tm.range_limit = tm_parameters['tm_range_limit']

    # getting turbidity data
    if turbidity_data_path.name[-3:] == 'csv':
        x_column_heading = 'Time (min)'
        y_column_heading = 'Turbidity'
        df = pd.read_csv(turbidity_data_path.absolute())
        x_data = df[x_column_heading].tolist()
        y_data = df[y_column_heading].tolist()
        tm.turbidity_data = dict(zip(x_data, y_data))
    elif turbidity_data_path.name[-4:] == 'json':
        with open(str(turbidity_data_path)) as file:
            data = json.load(file)
            turbidity_data = data['turbidity_data']
            turbidity_dissolved_reference = data['turbidity_dissolved_reference']
            turbidity_saturated_reference = data['turbidity_saturated_reference']
        tm.turbidity_data = turbidity_data
        if turbidity_dissolved_reference is not None:
            tm.turbidity_dissolved_reference = turbidity_dissolved_reference
        if turbidity_saturated_reference is not None:
            tm.turbidity_saturated_reference = turbidity_saturated_reference
    else:
        raise Exception('Turbidity data path must be .csv or .json')

    # make initial turbidity vs time graph
    x_values, y_values = tm.get_turbidity_data_for_graphing()
    last_turbidity_x = x_values[-1]
    # todo choose to graph either with or without stabilization
    # turbidity_fig: Figure = tm.make_turbidity_over_time_graph(
    #     x_values=x_values,
    #     y_values=y_values,
    #     x_axis_units='minutes',
    # )
    turbidity_fig: Figure = tm.make_turbidity_over_time_graph_with_stable_visualization()

    turbidity_axes: Axes = turbidity_fig.axes[0]
    # turbidity_axes.set_ylim(bottom=y_min, top=y_max)
    turbidity_axes.set_xlim(left=0)
    ax1_handles, ax1_labels = turbidity_axes.get_legend_handles_labels()
    if tm.turbidity_dissolved_reference is not None and ax1_labels.count('Dissolved reference') == 0:
        turbidity_axes.axhline(tm.turbidity_dissolved_reference,
                               ls=_ls_dash,
                               color=_colour_4_rgb,
                               label='Dissolved reference',
                               )
    if tm.turbidity_saturated_reference is not None and ax1_labels.count('Saturated reference') == 0:
        turbidity_axes.axhline(tm.turbidity_saturated_reference,
                               ls=_ls_dash,
                               color=_colour_4_rgb,
                               label='Saturated reference',
                               )

    # add vertical lines for liquid additions
    td = TemporalData()
    td.data = pd.read_csv(liquid_addition_path.absolute())
    liquid_x = td.data['Time (min)'].tolist()
    last_liquid_x = liquid_x[-1]
    total_volume_data = td.data['Total volume (mL)'].tolist()
    x_tick_labels = [round(x, 4) for x in total_volume_data]  # round to 4 decimal places
    turbidity_axes_2: Axes = turbidity_axes.twiny()
    # make sure the x axes on both graphs align
    max_x = max(last_liquid_x, last_turbidity_x)
    max_x = max_x + (max_x * 0.05)
    turbidity_axes.set_xlim(right=max_x)
    turbidity_axes_2.set_xlim(right=max_x)
    # adjust axes tick labels to be volume instead of time
    turbidity_axes_2.set_xticks(liquid_x)
    turbidity_axes_2.set_xticklabels(x_tick_labels)
    turbidity_axes_2.set_xlabel('Code total volume (mL)')
    for index, _ in enumerate(liquid_x):
        if index == 0:
            # dont need to add a line at the first time (which is time 0)
            continue
        x = liquid_x[index]
        total_volume = total_volume_data[index]
        # only add the label for the first index, so the legend of the graph doesnt get messed up
        if index == 1:
            turbidity_axes_2.axvline(x,
                                     ls=_ls_dot,
                                     color=_colour_8_rgb,
                                     label='Code total volume (mL)',
                                     )
        else:
            turbidity_axes_2.axvline(x,
                                     ls=_ls_dot,
                                     color=_colour_8_rgb,
                                     )

    # minor things to make things look nice
    turbidity_axes.legend().remove()
    turbidity_fig.autofmt_xdate(ha='center')  # rotate x axes labels if there is any overlap
    turbidity_fig.subplots_adjust(top=0.8)
    left, bottom, right, top = 0, 0.03, 1, 0.95
    turbidity_fig.tight_layout(rect=[left, bottom, right, top])
    ax1_handles, ax1_labels = turbidity_axes.get_legend_handles_labels()
    ax2_handles, ax2_labels = turbidity_axes_2.get_legend_handles_labels()
    turbidity_axes.legend(handles=(*ax1_handles, *ax2_handles),
                          labels=(*ax1_labels, *ax2_labels),
                          bbox_to_anchor=(1.04, 0.5), loc="center left")

    # save graph as png
    turbidity_fig.savefig(save_path.absolute(), bbox_inches='tight')
    turbidity_fig.clf()
    plt.clf()
    plt.close()

    # delete unneeded turbidity json file that was only used to be able to create the graph
    os.remove(tm._turbidity_monitor_data_json_save_path)
    plt.close('all')
    return turbidity_fig

if __name__ == '__main__':
    graph_with_liquid_addition(Path(r'turbidity data\exp_1_turbidity data.json'),
                               Path(r'turbidity data\liquid addition data.csv'),
                               Path(r'turbidity data\turbidity liquid addition annotated '
                                    r'graph.png'),
                               )

