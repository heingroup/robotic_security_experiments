import os
import time
from pathlib import Path

import pandas as pd


def create_experiment_folder(exp_type: str, solid_type: str):
    dir_name = 'E:\mushu\experiments'
    experiment_number = 1
    experiment_name = f'study_{experiment_number}'
    experiment_folder = Path(os.path.join(dir_name, exp_type, solid_type, experiment_name))
    while True:
        if experiment_folder.exists():
            experiment_number += 1
            experiment_name = f'study_{str(experiment_number)}'
            experiment_folder = Path(os.path.join(dir_name, exp_type, solid_type, experiment_name))
        else:
            Path.mkdir(experiment_folder)
            break
    return experiment_folder


class ExperimentInformation:
    experiment_number = 1

    # SOLVENT, DISPENSE_VOLUME, STIR_DURATION, SOLID_WEIGHT,SOLID
    def __init__(self,
                 solid: str,
                 solvent: str,
                 final_solvent: str,
                 target_solid_mass: float,  # g
                 target_solvent_volume: float,  # mL
                 stir_rate: int,
                 target_stir_duration: int,  # s
                 path: Path,
                 base_experiment_name: str = 'exp',
                 ):
        # self.experiment_number: int = None
        self.trial: int = None
        self.wash: int = None
        # self.experiment_name,
        self.solvent = solvent
        self.final_solvent = final_solvent
        self.solid = solid
        self.initial_solid_mass: float = None  # mg
        self.final_solid_mass: float = None  # mg
        self.target_solid_mass = target_solid_mass
        self.target_solvent_volume = target_solvent_volume  # fixed
        self.vial_index: str = None
        self.solution_volume: float = None
        self.volume_after_dilution: float = None
        # self.volume_of_dilution: float = None
        self.solid_dispense_error: float = None
        # self.solvent_dispense_error: float = None
        self.stir_rate = stir_rate
        self.target_stir_duration = target_stir_duration
        self.stir_duration: float = None
        self.empty_vial_weight: float = None
        # self.final_weight_of_solution: float = None
        self.folder: Path = path

        self.empty_filter: float = None
        self.final_solid_mass_in_filter: float = None
        #
        # self.start_time: datetime = None
        # self.end_time: datetime = None
        # ei.start_time = datetime.now()

        # self.experiment_number: int = None
        # self.base_experiment_name = base_experiment_name
        # self.experiment_name = f'{self.base_experiment_name} {self.experiment_number} - {self.solvent.name}'

    # @property
    # def volume(self) -> float:
    #     """mL, actual volume added calculated from mass and density"""
    #     solid_mass = self.solid_mass
    #     initial_mass = self.initial_mass
    #     final_mass = self.final_mass
    #     if solid_mass is None or initial_mass is None or final_mass is None:
    #         return 0
    #     mass_solvent = final_mass - initial_mass
    #     solvent_density = self.solvent.density
    #     volume_solvent = mass_solvent / solvent_density
    #     self._volume = volume_solvent
    #     return self._volume

    # @property
    # def solubility(self) -> float:  # g/mL
    #     solid_mass = self.solid_mass
    #     volume_solvent = self.volume
    #     if solid_mass is None or volume_solvent is None or volume_solvent == 0:
    #         self._solubility = None
    #         return self._solubility
    #     solubility = solid_mass / volume_solvent
    #     self._solubility = solubility
    #     return self._solubility

    @property
    def solid_percent_error(self):
        theoretical = self.target_solid_mass
        actual = self.initial_solid_mass
        if theoretical is None or actual is None:
            return None
        p_error = abs((theoretical - actual) / actual) * 100
        return p_error

    # @property
    # def solvent_percent_error(self):
    #     theoretical = self.target_solvent_volume
    #     actual = self.volume
    #     if theoretical is None or actual is None or actual == 0:
    #         return None
    #     p_error = abs((theoretical - actual) / actual) * 100
    #     return p_error

    # def make_experiment_folder(self, parent_path: Path) -> Path:
    #     self.experiment_number = ExperimentInformation.experiment_number
    #     ExperimentInformation.experiment_number += 1
    #     self.experiment_name = f'{self.base_experiment_name}_{self.experiment_number}'
    #     experiment_folder = Path.joinpath(parent_path, self.experiment_name)
    #     while True:
    #         if experiment_folder.exists():
    #             self.experiment_number = ExperimentInformation.experiment_number
    #             ExperimentInformation.experiment_number += 1
    #             self.experiment_name = f'{self.base_experiment_name}_{self.experiment_number}'
    #             experiment_folder = Path.joinpath(parent_path, self.experiment_name)
    #         else:
    #             Path.mkdir(experiment_folder)
    #             break
    #     return experiment_folder
    def clear_data(self):
        self.wash = None
        self.solvent = None


class Results:
    datetime_format = '%I:%M %p, %d %b %Y'  # hour:min am/pm, day month year

    def __init__(self,
                 ):
        self.results = self.initialize_results_df()

    def initialize_results_df(self) -> pd.DataFrame:
        columns = [  # 'Experiment number',
            'Trial number',
            'Wash number',
            # 'Experiment name',
            'Solvent',
            'Solid',
            'Final solvent',
            'Initial solid mass (mg)',
            'Final solid mass (mg)'
            'Volume solvent (mL)',  # fixed
            'Vial index',
            'Volume solution (mL)',
            'Volume after dilution (mL)',
            # 'Volume of dilution (mL)'
            # 'Solid dispense percent error (%)',
            # 'Solvent dispense percent error (%)',
            'Stir duration (min)',
            'Target stir duration (min)',
            'Stir rate (rpm)',
            'Empty vial weight (g)',
            'Empty filter weigh (mg)',
            # 'Solution final weight (g)',
            # 'Start time',
            # 'End time',
        ]
        results_df = pd.DataFrame(columns=columns)
        return results_df

    def add_data(self,
                 experiment_information: ExperimentInformation,
                 # tm_parameters: Dict,
                 ):
        ei: ExperimentInformation = experiment_information
        # tm_parameters_str = ''
        # for (key, value) in list(tm_parameters.items()):
        #     key = str(key)
        #     value = str(value)
        # tm_parameters_str += f'{key}: {value}\n'
        # if ei.start_time is not None:
        #     start_time = ei.start_time.strftime(self.datetime_format)
        # else:
        #     start_time = None
        # if ei.end_time is not None:
        #     end_time = ei.end_time.strftime(self.datetime_format)
        # else:
        #     end_time = None

        # assumed_solubility = ei.target_solid_mass / ei.target_volume
        # assumed_solvent_added = ei.target_volume - ei.target_initial_volume

        add_data = {  # 'Experiment number': ei.experiment_number,
            'Trial number': ei.trial,
            'Wash number': ei.wash,
            # 'Experiment name': ei.experiment_name,
            'Final Solvent': ei.final_solvent,
            'Solvent': ei.solvent,
            'Solid': ei.solid,
            'Initial solid mass (mg)': ei.initial_solid_mass,
            'Final solid mass (mg)': ei.final_solid_mass,
            'Volume solvent (mL)': ei.target_solvent_volume,  # fixed
            'Vial index': ei.vial_index,
            'Volume solution (mL)': ei.solution_volume,
            'Volume after dilution (mL)': ei.volume_after_dilution,
            # 'Volume of dilution (mL)': ei.volume_of_dilution,
            # 'Solid dispense percent error (%)': ei.solid_dispense_error,
            # 'Solvent dispense percent error (%)': ei.solvent_dispense_error,
            'Stir duration (min)': ei.stir_duration,
            'Target stir duration (min)': ei.target_stir_duration,
            'Stir rate (rpm)': ei.stir_rate,
            'Empty vial weight (g)': ei.empty_vial_weight,
            'Empty filter weigh (mg)': ei.empty_filter,
            # 'Solution final weight (g)': ei.final_weight_of_solution,
            # 'Start time': ei.start_time,
            # 'End time': ei.end_time,
        }
        self.results = self.results.append(add_data, ignore_index=True)
        # self.results: pd.DataFrame = self.results.drop_duplicates(subset='Experiment number',
        #                                                           keep='last')
        self.results: pd.DataFrame = self.results.drop_duplicates(subset='Vial index',
                                                                  keep='last')
        return self.results

    def save(self, path: Path):
        path = str(path)
        if path[-4:] != '.csv':
            path += '.csv'
        while True:
            try:
                self.results.to_csv(path, sep=',', index=False, mode='w')
                break
            except PermissionError as e:
                time.sleep(1)
        return self.results

    def save_results(self, ei: ExperimentInformation):
        self.add_data(experiment_information=ei)
        self.save(path=ei.folder.joinpath('results.csv'))
