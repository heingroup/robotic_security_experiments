from n9.configuration import deck_consumables
from n9.experiments.capping_uncapping import recap_to_tray, uncap_from_tray
from n9.experiments.crystal_solubility_profiling.experiment_info_saving import ExperimentInformation
from n9.experiments.reaction import get_needle_with_vision, bring_stock_in_line, wash_dosing_line, \
    dump_needle_with_vision
from n9.experiments.station_robot_engagement import *
from ur.configuration import ur_deck
from ur.configuration.ur_deck import quantos_guns


class SolutionPreparation:
    def __init__(self, ei: ExperimentInformation):
        self.ei = ei
        self.solvent = ei.solvent
        # self.stock_index, self.solvent_current_volume = deck_consumables.get_stock_data(ei.solvent)
        self.dispense_volume = ei.target_solvent_volume
        self.stir_duration = ei.target_stir_duration
        self.solid_weight = ei.target_solid_mass
        self.solid = ei.solid

    def prepare_tubes(self, last: bool = False) -> None:
        # add solvent
        if last:
            self.solvent = self.ei.final_solvent
            self.stir_duration = 300 #s
        else:
            self.solvent = self.ei.solvent
        self.stock_index, self.solvent_current_volume = deck_consumables.get_stock_data(self.solvent)
        self.prepare_to_dispense(stock_index=self.stock_index,
                                 # vial_start_volume=self.dispense_volume)
                                 vial_start_volume=self.ei.target_solvent_volume)
        self.update_stock_info()

        # Transfer tube to vision station
        cap_tube()
        tube_from_capstation()
        tube_to_vision('A2')
        self.count_to_stir()

    # REQUIRES: correct solid is loaded
    # EFFECTS: dose solid in filter, uncapped filter at cap station
    def prepare_filter(self):
        # Check if correct solid is loaded
        current_solid_on_quantos = quantos_guns.current_solid_name()
        if self.solid != current_solid_on_quantos:
            quantos_guns.switch_gun(self.solid)
        # Load new tube
        tube_index, tube_cap_status = deck_consumables.get_clean_tube()
        if tube_index is None:
            print('Out of tubes')
        tube_from_tray(tube_index)
        # tube to capstation
        tube_to_capstation()
        # uncap
        uncap_tube()
        # transfer filter
        filter_from(deck.CAP_STATION)
        filter_to(deck.FILTER_HOLDER)
        # filter holder
        vial_from_tray('B6')
        # vial_to_gripper(capped=False)
        # vial_from_gripper()
        # weigh mass of empty filter
        # self.ei.empty_filter = weigh_with_quantos()
        # self.ei.empty_filter = float(deck.quan.weight_immediately)
        # dose
        # self.ei.initial_solid_mass = dose_with_quantos(solid_weight=self.solid_weight)

        zero_quantos()
        vial_to_quantos()
        deck.quan.home_z_stage()
        # read weight
        deck.quan.front_door_position = "close"
        time.sleep(5)
        self.ei.empty_filter = float(deck.quan.weight_immediately)

        deck.quan.home_z_stage()
        time.sleep(1)

        deck.quan.move_z_stage(steps=8600)

        deck.quan.target_mass = self.solid_weight  # mg

        deck.quan.start_dosing(wait_for=True)
        x = deck.quan.sample_data.quantity

        self.ei.initial_solid_mass = float(x) * 1000  # convert mass to g
        print('dosed ', self.ei.initial_solid_mass, 'mg')

        deck.quan.home_z_stage()
        # open door
        deck.quan.front_door_position = "open"

        vial_from_quantos()
        # return mass solid dosed in g

        # TEST end

        # place filter in tube
        vial_to_tray('B6')
        filter_from(deck.FILTER_HOLDER)
        filter_to(deck.CAP_STATION)
        # tube_from_filterstation()
        # tube_to_miniTray('A2')
        # print(mass_solid)
        # self.ei.initial_solid_mass = mass_solid
        return tube_index

    # Effect: withdraw and dispense solvent to tubes at capstation
    def prepare_to_dispense(self: bool = True, stock_index: str = 'A2',
                            vial_start_volume: float = 0.5, wash_with_stock: bool = False) -> None:
        get_needle_with_vision()
        deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
        # deck.needle_tray.pickup()
        # reaction.uncap_needle()
        # deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
        # n9.move(x=273.1489013671875, y=-159.43560791015625, z=300, probe=True)
        if self:
            if wash_with_stock:
                bring_stock_in_line(stock_index, sample_volume=0.95)
                wash_dosing_line(by_tap_solvent=False)
            bring_stock_in_line(stock_index=stock_index, sample_volume=vial_start_volume)
        else:
            #  line to get rid of inline bubbles
            wash_dosing_line()
            wash_dosing_line()
        # go to dispense location
        location = deck.CAP_STATION
        deck.n9.move(location.x, location.y, z=300, probe=True)
        deck.n9.move(location.x, location.y, z=154, probe=True)

        # dispense
        if self:
            # dispense all that was withdrawn
            deck.dosing_pump.move_absolute_ml(0, deck.SOLVENT_DRAW_RATE)
        else:
            print(vial_start_volume)
            deck.dosing_pump.dispense_ml(volume_ml=vial_start_volume,
                                         from_port=deck.SOLVENT_PORT,
                                         to_port=deck.SAMPLE_PORT,
                                         velocity_ml=deck.SOLVENT_DRAW_RATE,
                                         dispense_velocity_ml=deck.SOLVENT_DRAW_RATE,
                                         )
        # lift needle above the pierce location
        deck.n9.move(location.x, location.y, z=300, probe=True)
        dump_needle_with_vision()
        deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)

        # reaction.dump_needle()

    # EFFECTS: update the stock volume
    # MODIFIES: 'current volume' in stock_info.csv
    def update_stock_info(self):
        self.solvent_current_volume -= self.dispense_volume
        deck_consumables.update_stock_file(self.solvent, 'current volume', self.solvent_current_volume)

    # EFFECTS: start the stirring plate
    #           record start time, calculate end time
    def count_to_stir(self):
        deck.mini_heater_stirrer.target_stir_rate = 1500
        deck.mini_heater_stirrer.start_stirring()
        self.start_time = time.time()
        self.end_time = self.start_time + self.stir_duration

    # EFFECTS: stop the stirring plate after the end time
    #           print count down prior to the end time
    def count_to_stop_stirring(self):
        keep_going = False
        while not keep_going:
            if time.time() > self.end_time:
                keep_going = True
            else:
                time_left_to_wait = self.end_time - time.time()
                if time_left_to_wait > 60:
                    print(round(time_left_to_wait / 60), 'minuets left')
                    time.sleep(60)
                elif 60 > time_left_to_wait > 10:
                    print(round(self.end_time - time.time()), ' seconds left')
                    time.sleep(10)
                else:
                    print(round(self.end_time - time.time()), ' seconds left')
                    time.sleep(1)
        deck.mini_heater_stirrer.stop_stirring()
        return (time.time() - self.start_time) / 60


#
# class CentrifugePreparation:
#     def __init__(self,
#                  centrifuge_duration: 180):
#         self.centrifuge_duration = centrifuge_duration
#
#     def prep_centrifugation(self):
#         # Transfer tube to mini holder next to UR
#         tube_from_vision('A2')
#         tube_to_miniTray('A2')
#         # move n9 out of the way
#         deck.n9.move(x=77.5, y=322.6, z=300)
#         # Centrifuge it (3 min)
#         ur_deck.ur3_centrifuge.run_centrifuge(self.centrifuge_duration)
def prep_centrifugation(centrifuge_duration: int = 180) -> None:
    # Transfer tube to mini holder next to UR
    tube_from_vision('A2')
    tube_to_miniTray('A2')
    # move n9 out of the way
    deck.n9.move(x=77.5, y=322.6, z=300)
    # Centrifuge it (3 min)
    ur_deck.ur3_centrifuge.run_centrifuge(centrifuge_duration)


def reload_for_next_tube(clean_up_from_centrifuge: bool = True):
    if clean_up_from_centrifuge:
        tube_from_miniTray('A2')
        tube_to_capstation()
        uncap_tube()
        tube_from_capstation()
        tube_to_filterstation()
    # reload the mini tray for the next run
    tube_index, tube_cap_status = deck_consumables.get_clean_tube()
    if tube_index is None:
        print('Out of tubes')
    tube_from_tray(tube_index)
    tube_to_capstation()
    uncap_tube()
    # Remove filter and place filter into new tube (for next round)
    # tube_from_miniTray('A2')
    # tube_to_filterstation()
    filter_from(deck.FILTER_STATION)
    filter_to(deck.CAP_STATION)
    return tube_index


# def cleanup_filter(ei: ExperimentInformation):
#     tube_from_miniTray('A2')
#     tube_to_capstation()
#     uncap_tube()
#     tube_from_capstation()
#     tube_to_filterstation()
#     # weigh filter with final solid
#     filter_from(deck.FILTER_STATION)
#     filter_to(deck.FILTER_HOLDER)
#     vial_from_tray('B6')
#     # vial_to_gripper(capped=False)
#     # vial_from_gripper()
#     # weigh mass of empty filter
#     ei.final_solid_mass_in_filter = weigh_with_quantos()
#     ei.final_solid_mass = ei.final_solid_mass_in_filter - ei.empty_filter
#     ### MAKE FINAL SOLID SOLUTION
#     # dispose filter
#     vial_from_quantos()
#     vial_to_tray('B6')
#     filter_from(deck.FILTER_HOLDER)
#     filter_to(deck.SOLID_WASTE)

def weigh_filter(ei: ExperimentInformation):
    tube_from_miniTray('A2')
    tube_to_capstation()
    uncap_tube()
    tube_from_capstation()
    tube_to_filterstation()
    # weigh filter with final solid
    filter_from(deck.FILTER_STATION)
    filter_to(deck.FILTER_HOLDER)
    vial_from_tray('B6')
    # vial_to_gripper(capped=False)
    # vial_from_gripper()
    # weigh mass of empty filter
    ei.final_solid_mass_in_filter = weigh_with_quantos()
    ei.final_solid_mass = ei.final_solid_mass_in_filter - ei.empty_filter
    ### MAKE FINAL SOLID SOLUTION
    # dispose filter
    vial_to_tray('B6')
    filter_from(deck.FILTER_HOLDER)
    filter_to(deck.FILTER_STATION)


def cleanup_filter():
    # Remove filter and place filter into new tube (for next round)
    tube_from_miniTray('A2')
    tube_to_capstation()
    uncap_tube()
    tube_from_capstation()
    tube_to_filterstation()
    filter_from(deck.FILTER_STATION)
    # filter_from(deck.CAP_STATION)
    filter_to(deck.SOLID_WASTE)


class TubeVialSolutionTransfer:
    def __init__(self, ei: ExperimentInformation, final_solvent: bool = False, tube_index: str = 'A1', ):
        self.tube_index = tube_index
        self.ei = ei
        if final_solvent:
            self.stock_index, self.solvent_current_volume = deck_consumables.get_stock_data(ei.final_solvent)
        else:
            self.stock_index, self.solvent_current_volume = deck_consumables.get_stock_data(ei.solvent)

    # Effects: transfer 0.5 mL of solution from tube to vial
    #          add calculated amount of acetone to make it a total of 1 ml of solution in the vial
    def transfer_solution_tube_to_vial(self):
        # df = self.create_test_df()

        # vial_mass = self.prepare_for_transfer_solutions()
        self.ei.empty_vial_weight = self.prepare_for_transfer_solutions()

        # transfer solution from tube to vial
        self.dispense(0.6, deck.SOLVENT_DRAW_RATE, deck.SOLVENT_PUSH_RATE, deck.SOLVENT_PORT, deck.SAMPLE_PORT)
        # volume transferred from tube
        # volume = self.get_tube_solution_volume(vial_mass=self.ei.empty_vial_weight)
        self.ei.solution_volume = self.get_tube_solution_volume(vial_mass=self.ei.empty_vial_weight)

        # dispense acetone to make it a total of 1ml
        self.ei.volume_after_dilution = self.dispense_to_1ml(volume=self.ei.solution_volume,
                                                             vial_mass=self.ei.empty_vial_weight)
        # print('total volume in the vial: ', total_volume)
        # print('total volume in the vial: ',volume)

        self.clean_up_vial_tube()
        self.ei.vial_index = self.vial_index
        # return vial_mass, volume, total_volume, self.vial_index

    # def create_test_df(self):
    #     columns = ['Wash number',
    #                'Vial index',
    #                'Final total volume (g)'
    #                'Final total volume (mL)'
    #                'Solution volume (g)',
    #                'Solution volume (mL)',
    #                'Acetone - target volume calibration adjusted (mL)',
    #                'Acetone - target volume (mL)',
    #                'Vial mass (g)'
    #                ]
    #     df = pd.DataFrame(columns=columns)
    #     return df

    # EFFECT: prepare vial, weigh vial, ready to dispense solution
    def prepare_for_transfer_solutions(self):
        self.set_up_new_vial()
        vial_mass = self.weigh()
        uncap_from_tray(from_tray=False)
        park_cap()
        return vial_mass

    # EFFECT: grab vial , bring to gripper
    def set_up_new_vial(self):
        vial_index, _ = deck_consumables.get_clean_vial()
        self.vial_index = vial_index
        deck.c9.output(0, False)
        deck.c9.output(1, False)
        vial_from_tray(rxn_index=vial_index, capped=True)
        vial_to_gripper(capped=True)
        # capping_uncapping.uncap_from_tray(rxn_index=vial_index, from_tray=True)
        # park_cap()

    # Effect: weigh and return weight in grams
    def weigh(self):
        vial_from_gripper(to_tray=False, capped=False)
        mass_in_g = weigh_with_quantos()
        vial_to_gripper(from_tray=False, capped=False)
        return mass_in_g

    # Effect: transfer solution from tube to vial
    # Require: empty uncapped vial at gripper
    def dispense(self, volume: float, pull_flow_rate: float, push_flow_rate: float,
                 from_port: int, to_port: int, from_stock: bool = False) -> None:
        # grab needle, uncap, dose, discard needle
        get_needle_with_vision()
        deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
        # deck.needle_tray.pickup()
        # reaction.uncap_needle()
        if from_stock:
            # withdraw solvent from stock
            # reaction.bring_stock_in_line(stock_index='B1', sample_volume=volume)  # Acetone
            reaction.bring_stock_in_line(stock_index=self.stock_index, sample_volume=volume)  # Acetone
        else:
            # withdraw solution from the tube
            self.bring_solution_in_line(sample_volume=volume)
        # inject it in gripper location
        deck.n9.move(x=-184.5114013671875, y=-9.762683105468739, z=300, probe=True)
        # assume short needle
        deck.n9.move(x=-184.5114013671875, y=-9.762683105468739, z=163.47930908203125, probe=True)
        # self.pump.move_absolute_ml(0, velocity_ml=push_flow_rate)
        deck.dosing_pump.move_absolute_ml(0, velocity_ml=push_flow_rate)
        # move up
        deck.n9.move(x=-184.5114013671875, y=-9.762683105468739, z=300, probe=True)
        dump_needle_with_vision()
        # reaction.dump_needle()
        deck.n9.move(x=158.2, y=-158.89, z=302.2, gripper=109.173, probe=True)
        time.sleep(1)

    # Effect: draw solution from tube
    def bring_solution_in_line(self, airgap_volume: float = 0.5, sample_volume: float = 0.6,
                               long_needle: bool = False):
        # go to filter station @ safe  height
        deck.n9.move(x=-156.1, y=194.3, z=300)
        # change port to sample port and draws air gap
        deck.dosing_pump.switch_valve(deck.SAMPLE_PORT)
        deck.dosing_pump.move_absolute_ml(airgap_volume + 0.2, deck.STOCK_DRAW_RATE)
        # go inside tube and draw solution
        if long_needle:
            print('location update needed prior to running, please stop the program from running no')
            time.sleep(120)
            deck.n9.move(x=-203.6, y=200.9, z=306)  # Z UNSURE
        else:
            deck.n9.move(x=-156.1, y=194.3, z=210)
            deck.n9.move(x=-156.1, y=194.3, z=156.37)
        # withdraw solution
        deck.dosing_pump.move_relative_ml(sample_volume, deck.STOCK_DRAW_RATE)
        # come out and up to safe height Position
        deck.n9.move(x=-156.1, y=194.3, z=300)
        # draw  air
        # deck.dosing_pump.move_relative_ml(0.1, deck.STOCK_DRAW_RATE)

    # EFFECT: weigh, calculate and return the volume of the solution transferred from the tube to vial
    def get_tube_solution_volume(self, vial_mass: float):
        pickup_cap()
        recap_to_tray(to_tray=False)
        vial_to_gripper()
        mass_after_tube_dispense = self.weigh()
        tube_solution_mass = mass_after_tube_dispense - vial_mass
        volume = round(tube_solution_mass / 0.79, 3)
        return volume

    # EFFECT: dispense acetone to make it a total of 1ml
    def dispense_to_1ml(self, volume: float, vial_mass: float):
        uncap_from_tray(from_tray=False)
        park_cap()
        self.dispense(1 - volume, deck.SOLVENT_DRAW_RATE, deck.SOLVENT_PUSH_RATE,
                      deck.SOLVENT_PORT, deck.SAMPLE_PORT, from_stock=True)
        time.sleep(1)
        pickup_cap()
        recap_to_tray(to_tray=False)
        vial_to_gripper()
        mass_after_dispense = self.weigh()
        solution_mass = mass_after_dispense - vial_mass
        total_volume = round(solution_mass / 0.784, 3)
        return total_volume

    # EFFECTS: put tube from filter station back to tube tray
    #           put vial from gripping station back to vial tray
    def clean_up_vial_tube(self):
        # return vial to tray
        vial_from_gripper(capped=True)
        # vial_to_tray(rxn_index=self.vial_index, capped=True)
        vial_to_tray('A1', capped=True)
        # return tube to tray
        tube_from_filterstation()
        tube_to_tray(self.tube_index)
