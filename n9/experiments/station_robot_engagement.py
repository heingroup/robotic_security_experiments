import logging
import time
from north_robots.components import Location
from n9.configuration import deck
from n9.experiments.capping_uncapping import recap, uncap
from n9.experiments import reaction
from n9.experiments import capping_uncapping
from hein_robots.grids import Grid
logging.basicConfig(level=logging.WARN)

def zero_quantos():
    # tare
    deck.quan.front_door_position = "close"
    deck.quan.zero()


def weigh_with_quantos(take_out:bool=True,filter:bool=False):
    """
    INVOLVES N9 MOVEMENTS: tares, takes vial in, weighs, takes vial out
    :return: weight in g

    """
    zero_quantos()
    vial_to_quantos(filter)
    #deck.quan.home_z_stage()
    # #read weight
    #deck.quan.front_door_position = "close"
    time.sleep(5)
    weight = float(deck.quan.weight_immediately)
    if weight<0.001:
        input("no object is on")
    #deck.quan.front_door_position = "open"
    if take_out:
        vial_from_quantos(filter)
    return weight  # in g


def dose_with_quantos(solid_weight: float = 0, filter: bool =False, bring_in:bool=True):
    """

    :param solid_weight: in mg
    :return:
    """
    if bring_in:
        vial_to_quantos(filter=filter)
    # deck.vision_gripper_vial_check()
    deck.quan.home_z_stage()
    time.sleep(1)
    # check only dose if cap is off
    # if deck.vision_cap_detection() is True:
    #    print('cap detected, cant dose')
    #    while deck.vision_cap_detection() is True:
    #        vial_from_quantos()
    #        vial_to_gripper(capped=False)
    #        capping_uncapping.uncap_from_tray(from_tray=False)
    #        park_cap()
    #        vial_to_quantos()
    # else:
    # z stage down
    if filter:
        deck.quan.move_z_stage(steps=8380)
    else:
        deck.quan.move_z_stage(steps=8850)
    # close door
    deck.quan.front_door_position = "close"
    # dose
    deck.quan.target_mass = solid_weight  # mg
    # deck.quan.start_dosing(wait_for=True)
    # Quantos TEST
    # try:
    deck.quan.start_dosing(wait_for=True)
    x = deck.quan.sample_data.quantity
    # except ValueError:
    #     x = deck.quan.weight_immediately
    #     deck.quan.wait_for()  # sleep a bit to wait for it\

    # x0=deck.quan.weight_immediately
    # x = float(x)
    # print(solid_weight-x)
    # loop to fix dosing error
    # while solid_weight-(x*1000) > 5:
    #    deck.quan.target_mass = float(solid_weight-(x*1000)) # mg
    #    deck.quan.start_dosing(wait_for=True)
    #    y = deck.quan.sample_data.quantity
    #    x += float(y)
    mass_no_units = float(x) * 1000  # convert mass to mg
    #todo try this second attempt condition
    if mass_no_units<0.001: # if dosed nothing on the first try , try dosing again
        deck.quan.target_mass = solid_weight  # mg
        deck.quan.start_dosing(wait_for=True)
        x = deck.quan.sample_data.quantity
        mass_no_units = float(x) * 1000  # convert mass to mg
        print("second attempt because of no dosing on the first attempt")

    print('dosed ', mass_no_units, 'mg')

    deck.quan.home_z_stage()
    # open door
    deck.quan.front_door_position = "open"

    vial_from_quantos()
    # return mass solid dosed in g
    return mass_no_units

    #raise ValueError('Unable to dose Quantos after 3 retries')
    # TEST end


def poke_vision_top_open():
    # safe height
    deck.n9.move(x=122.38, y=111.45, z=300, gripper=91.286)
    # down ready to open
    deck.n9.move(x=122.38, y=111.45, z=139.7, gripper=91.286)
    # poke open
    deck.n9.move(x=122.38, y=98.9, z=139.7, gripper=91.286)
    # down ready to open
    deck.n9.move(x=122.38, y=111.45, z=139.7, gripper=91.286)
    # safe height
    deck.n9.move(x=122.38, y=111.45, z=300, gripper=91.286)


def poke_vision_top_close():
    deck.c9.output(0, False)
    # go to safe height
    deck.n9.move(x=125.9, y=33, z=300, gripper=91.23325)
    # down ready to close
    deck.n9.move(x=125.9, y=33, z=138, gripper=91.23325)
    # poke close
    deck.n9.move(x=125.9, y=58, z=138, gripper=91.23325)
    # down ready to close
    deck.n9.move(x=125.9, y=33, z=138, gripper=91.23325)
    # safe height
    deck.n9.move(x=125.9, y=33, z=300, gripper=91.23325)


def park_cap():
    # park cap
    location = deck.CAP_PARKING
    safe_location = Location(location.x, location.y, 310)
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)
    deck.n9.move_to_location(location, gripper=150.6, probe=False)
    time.sleep(1)
    recap(controller=deck.c9, pitch_mm=2, rotations=1)
    time.sleep(1)
    # park cap
    deck.c9.output(0, False)
    # up at safe height
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)


def pickup_cap(filter:bool = False):
    location = deck.CAP_PARKING
    safe_location = Location(location.x, location.y, 310)
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)
    if filter:
        cap_pickup=  Location(location.x, location.y, location.z-10.5)
        deck.n9.move_to_location(cap_pickup, gripper=150.6, probe=False)

    else:
        deck.n9.move_to_location(location, gripper=150.6, probe=False)

    # grab cap
    deck.c9.output(0, True)
    uncap(controller=deck.c9, pitch_mm=2, rotations=1)
    # up at safe height
    deck.n9.move_to_location(safe_location, gripper=150.6, probe=False)


def take_arm_out_of_quantos():
    # go up on the vial
    deck.n9.move(x=-225.2, y=-188.3, z=156.6, gripper=52.613)
    # go to safe middle
    deck.n9.move(x=-225.2, y=41.04, z=156.6, gripper=52.61)
    # go up on that location to safe height
    deck.n9.move(x=-225.2, y=41.04, z=300, gripper=52.61)


def vial_from_quantos(filter:bool=False):
    location = deck.QUANTOS_HOLDER  #engage location
    safe_location_inside = Location(location.x, location.y,156 )
    safe_location_ouside_down = Location(location.x, 41,155)
    safe_location_ouside_up = Location(location.x, 41, 300)

    # open quantos door first
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    # go to safe middle location at safe height
    deck.n9.move_to_location(safe_location_ouside_up, gripper=52.61)
    # go to middle safe location lower
    deck.n9.move_to_location(safe_location_ouside_down, gripper=52.61)
    # go inside quntos and above
    deck.n9.move(x=-223.8, y=-170, z=156, gripper=-190)
    deck.n9.move_to_location(safe_location_inside, gripper=-190)
    # deck.n9.move(x=-224.0, y=-165.7, z=154.6, gripper=-180)
    # deck.n9.move(x=-224.247310791015626, y=-174.2943603515625, z=154.6, gripper=-180)
    #time.sleep(2)
    #deck.c9.speed(velocity=8000,acceleration=16000)

    # go down and grab vial
    if filter:
        deck.n9.move(x=-223.8, y=-181.3, z=153.2, gripper=-180)
        #todo lower it
        deck.n9.move(x=-223.8, y=-181.3, z=137, gripper=-180)
    else:
        deck.n9.move_to_location(location, gripper=-180)
    deck.c9.output(0, True)
    # go up
    deck.n9.move_to_location(safe_location_inside, gripper=-180)
    # go to safe middle
    time.sleep(2)
    deck.c9.speed(velocity=20000,acceleration=40000)
    deck.n9.move_to_location(safe_location_ouside_down, gripper=52.6)
    # go up on that location to safe height
    deck.n9.move_to_location(safe_location_ouside_up, gripper=52.61)
    deck.quan.front_door_position = "close"


def vial_to_quantos(filter:bool=False):
    locationn = deck.QUANTOS_HOLDER  #engage location
    safe_location_ouside_up = Location(locationn.x, 41, 300)
    safe_location_ouside_down = Location(locationn.x, 41,155.5)
    safe_location_inside = Location(locationn.x, locationn.y,156 )
    drop_location = Location(locationn.x, locationn.y, 138)

    # go to safe middle at safe height
    deck.n9.move_to_location(safe_location_ouside_up, gripper=-180)
    # drive z stage up (home)
    deck.quan.home_z_stage()
    # open door
    deck.quan.front_door_position = "open"
    # go to middle safe location lower
    deck.n9.move_to_location(safe_location_ouside_down, gripper=-180) # z=151.6
    # go inside quntos and above
    deck.n9.move_to_location(safe_location_inside, gripper=-180)
    # go inside quantos and inside vial holder z 145
    if filter:
        deck.n9.move(x=-223.8, y=-181.3, z=142, gripper=-180)
        # deck.n9.move(x=-223.8, y=-180.3, z=146.8, gripper=-180)

    else:
        deck.n9.move_to_location(drop_location, gripper=-180)
    time.sleep(1)
    # ungrip vial
    deck.c9.output(0, False)
    #if filter:
    #    deck.c9.output(0, True)
    #    time.sleep(2)
    #    deck.c9.output(0, False)
    #    deck.n9.move(-223.4, y=-184.2, z=150, gripper=-180)
        #deck.n9.gripper.move_degrees(140, relative=True)
    # go up
    deck.n9.move_to_location(safe_location_inside, gripper=-190) #z=156.6
    deck.n9.move(x=-223.8, y=-170, z=156, gripper=-190)
    # go to safe middle
    #deck.n9.move(x = -223.5373291015624, y = -169.0876953125, z = 154, gripper = -179.84483105468752)
    deck.n9.move_to_location(safe_location_ouside_down, gripper=-180)
    # go up on that location to safe height
    deck.n9.move_to_location(safe_location_ouside_up, gripper=-180)


def vial_to_common_zone(filter:bool=False):
    deck.n9.move(x=-241.97572021484374, y=117.01318359375, z=300.822265625, gripper=175.6)
    if filter:
        deck.n9.move(x=-241.5277572021484374, y=117.01318359375, z=115, gripper=175.6)
        deck.n9.move(x=-241.5277572021484374, y=117.01318359375, z=98, gripper=175.6)

    else:
        deck.n9.move(x=-241.5277572021484374, y=118.01318359375, z=115, gripper=175.6)
        deck.n9.move(x=-241.5277572021484374, y=118.01318359375, z=96.2, gripper=175.6)
    deck.c9.output(0,False)
    deck.n9.move(x=-241.9277572021484374, y=117.01318359375, z=300.822265625, gripper=175.6)
    # away for UR clearance
    deck.n9.move(x=-108.62624511718752, y=122.08123779296875, z=300.0, gripper=175.6)

def tube_to_common_zone():
    deck.n9.move(x=-242.6603759765625, y=116.229833984375,z=300.822265625, gripper=52.64699999999999)
    #slow
    deck.c9.speed(velocity=8000,acceleration=16000)
    deck.n9.move(x=-242.0603759765625, y=118.229833984375, z=123, gripper=52.64699999999999, velocity=8000,
                 acceleration=16000)
    deck.n9.move(x=-242.0603759765625, y=118.229833984375, z=118, gripper=52.64699999999999,velocity=8000,acceleration=16000)
    recap(controller=deck.c9,pitch_mm=2.5,rotations=3)
    deck.c9.speed(velocity=20000,acceleration=40000)

    deck.c9.output(0,False)
    deck.n9.move(x=-243.6603759765625, y=118.229833984375, z=300.595458984375, gripper=52.64699999999999)
    # away for UR clearance
    deck.n9.move(x=-108.62624511718752, y=122.08123779296875, z=300.0, gripper=52.64699999999999)


def push_tube_in(cup:bool=False):
    deck.n9.move(x=-243.0603759765625, y=118.229833984375, z=300, gripper=52.64699999999999,velocity=8000,acceleration=16000)
    deck.c9.output(0,True)
    if cup:
        deck.n9.move(x=-243.0603759765625, y=118.229833984375, z=99, gripper=52.64699999999999,velocity=8000,acceleration=16000)
    else:
        deck.n9.move(x=-243.0603759765625, y=118.229833984375, z=111, gripper=52.64699999999999,velocity=8000,acceleration=16000)
    deck.n9.move(x=-243.0603759765625, y=118.229833984375, z=300, gripper=52.64699999999999,velocity=8000,acceleration=16000)
    deck.c9.output(0,False)
    # away for UR clearance
    deck.n9.move(x=-108.62624511718752, y=122.08123779296875, z=300.0, gripper=52.64699999999999)


def vial_from_common_zone(filter:bool = False):
    deck.c9.output(0, False)
    deck.n9.move(x=-241.9, y=117.01318359375, z=300.822265625, gripper=175.6)
    # todo check z for pickup from common zone
    if filter:
        deck.n9.move(x=-241.9, y=117.01318359375, z=98, gripper=175.6)
    else:
        deck.n9.move(x=-241.9, y=117.01318359375, z=96.2, gripper=175.6)

    deck.c9.output(0, True)
    deck.n9.move(x=-241.9, y=117.01318359375, z=300.822265625, gripper=175.6)

def press_filter_to_right_height(rxn_index: str = 'A1'):
    # arm gripper should be open
    deck.c9.output(0, True)
    deck.n9.move(x=-241.77572021484374, y=117.01318359375, z=300.822265625, gripper=52.64699999999999)
    # todo check z for pickup from common zone
    deck.n9.move(x=-241.77572021484374, y=117.01318359375, z=114, gripper=52.64699999999999)
    deck.n9.move(x=-241.77572021484374, y=117.01318359375, z=300.822265625, gripper=52.64699999999999)
    deck.c9.output(0, False)


def vial_from_tray(rxn_index: str = 'A1', capped: bool = False, tray_type: str = 'cup'):
    """
    """
    # arm gripper should be open
    deck.c9.output(0, False)
    if tray_type is 'cup':
        location = deck.reaction_grid_gripper.locations[rxn_index]
    if tray_type is 'hplc':
        location = deck.hplc_vial_tray.locations[rxn_index]

    safe_location = location.copy(z=300)
    # deck.n9.gripper.move_degrees(180)
    deck.n9.move_to_location(safe_location, probe=False)
    if tray_type is 'cup':
        if capped:
            grip_cap_location = location.copy(z=143.84)
        else:
            grip_cap_location = location.copy(z=133.85)
    if tray_type is 'hplc':
        if capped:
            grip_cap_location = location.copy(z=103)
        else:
            grip_cap_location = location.copy(z=94)
    deck.n9.move_to_location(grip_cap_location)
    deck.c9.output(0, True)
    deck.n9.move_to_location(safe_location, probe=False)


def vial_to_tray(rxn_index: str = 'A1', capped: bool = False, tray_type : str = 'cup'):
    if tray_type is 'cup':
        location = deck.reaction_grid_gripper.locations[rxn_index]
    if tray_type is 'hplc':
        location = deck.hplc_vial_tray.locations[rxn_index]
    safe_location = location.copy(z=300)
    # deck.n9.gripper.move_degrees(180)
    deck.n9.move_to_location(safe_location, probe=False)
    if tray_type is 'cup':
        if capped:
            grip_cap_location = location.copy(z=143.84)

        else:
            grip_cap_location = location.copy(z=134.84)
    if tray_type is 'hplc':
        if capped:
            grip_cap_location = location.copy(z=103)

        else:
            grip_cap_location = location.copy(z=94)
    deck.n9.move_to_location(grip_cap_location)
    deck.c9.output(0, False)
    deck.n9.move_to_location(safe_location, probe=False)


def tube_from_tray(rxn_index: str = 'A1'):
    deck.c9.output(0, False)
    location = deck.tube_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    approach_location = location.copy(z=110)
    grip_location = location.copy(z=104.5)
    deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)
    time.sleep(0.5)
    deck.n9.move_to_location(grip_location, gripper=-136.9, probe=False)
    deck.c9.output(0, True)
    deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)

def tube_to_tray(rxn_index: str = 'A1'):
    location = deck.tube_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    approach_location = location.copy(z=110)
    grip_location = location.copy(z=104.5)
    deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)
    time.sleep(0.5)
    deck.n9.move_to_location(grip_location, gripper=-136.9, probe=False)
    deck.c9.output(0, False)
    deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)

def tube_to_tray(rxn_index: str = 'A1'):
    location = deck.tube_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    deck.n9.move_to_location(safe_location, gripper=-136.9, probe=False)
    grip_location = location.copy(z=111)
    deck.n9.move_to_location(grip_location,gripper=-136.9, probe=False)
    deck.c9.output(0, False)
    time.sleep(0.5)
    deck.n9.move_to_location(safe_location,gripper=-136.9, probe=False)


def tube_from_miniTray(rxn_index: str = 'A1'):
    deck.c9.output(0, False)
    location = deck.tube_mini_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    approach_location = location.copy(z=95)
    # grip_location = location
    deck.n9.move_to_location(safe_location, gripper=176, probe=False)
    deck.n9.move_to_location(approach_location, probe=False)
    time.sleep(0.5)
    deck.n9.move_to_location(location)
    deck.c9.output(0, True)
    deck.n9.move_to_location(safe_location, probe=False)


def tube_to_miniTray(rxn_index: str = 'A1'):
    location = deck.tube_mini_grid_gripper.locations[rxn_index]
    safe_location = location.copy(z=300)
    deck.n9.move_to_location(safe_location, probe=False, gripper=176)
    grip_location = location.copy(z=103)
    deck.n9.move_to_location(grip_location)
    deck.c9.output(0, False)
    time.sleep(0.5)
    deck.n9.move_to_location(safe_location, probe=False)


def tubeMiniTray_from_kinova():
    # go to safe middle location at safe height
    # gripper=52.61
    deck.n9.move(x=78.5, y=325.1, z=300, gripper=108)
    # go down and grab vial
    deck.c9.output(0, False)
    deck.n9.move(x=78.5, y=325.1, z=211)
    deck.c9.output(0, True)
    # go up to safe middle
    deck.n9.move(x=78.5, y=325.1, z=300)


def tubeMiniTray_to_kinova():
    # go to safe middle location at safe height
    # gripper=52.61
    deck.n9.move(x=78.5, y=325.1, z=300, gripper=108)
    time.sleep(1)
    # go down and release vial
    # deck.c9.output(0, False)
    deck.n9.move(x=78.5, y=325.1, z=213)
    time.sleep(1)
    deck.c9.output(0, False)
    # go up to safe middle
    deck.n9.move(x=78.5, y=325.1, z=300)


def tubeMiniTray_from_ur():
    # go to safe middle location at safe height
    # gripper=52.61
    deck.n9.move(x=-299.9, y=99.1, z=300, gripper=109.68)
    # go down and grab station
    deck.c9.output(0, False)
    deck.n9.move(x=-299.9, y=99.1, z=95)
    deck.c9.output(0, True)
    # go up to safe middle
    deck.n9.move(x=-299.9, y=99.1, z=300)


def tubeMiniTray_to_ur():
    # go to safe middle location at safe height
    # gripper=141.256, gripper=143.56471142578124
    deck.n9.move(x=-300.4, y=97.6, z=300, gripper=109.68)
    time.sleep(1)
    # go down and release station
    deck.n9.move(x=-300.4, y=97.6, z=102)
    deck.n9.move(x=-300.4, y=97.6, z=98)
    time.sleep(1)
    deck.c9.output(0, False)
    # go up to safe middle
    deck.n9.move(x=-300.4, y=97.6, z=300)


def tube_from_filterstation():
    # gripper=-73.678
    # go to safe  height
    deck.n9.move(x=-201.1, y=199.4, z=300)
    # open gripper
    deck.c9.output(0, False)
    # go down and pick up tube
    deck.n9.move(x=-201.1, y=199.4, z=147)
    deck.n9.move(x=-201.1, y=199.4, z=138)
    deck.c9.output(0, True)
    # go up to safe height
    deck.n9.move(x=-201.1, y=199.4, z=300)


def tube_to_filterstation():
    # go to safe  height
    # deck.n9.move(x=-203.6, y=200.9, z=300)
    deck.n9.move(x=-199.6, y=199.4, z=300)
    # go down and drop the tube
    deck.n9.move(x=-199.6, y=199.4, z=140)
    deck.c9.output(0, False)
    # go up to safe height
    deck.n9.move(x=-199.6, y=199.4, z=300)


def remove_filter():
    # # go to safe middle location at safe height
    # deck.n9.move(x=-199.6, y=199.4, z=300)
    # # open gripper
    # deck.c9.output(0, False)
    # # go down and pick up tube
    # deck.n9.move(x=-199.6, y=199.4, z=155)
    # deck.n9.move(x=-199.6, y=199.4, z=148.867)
    # deck.c9.output(0, True)
    # # go up to safe height
    # deck.n9.move(x=-199.6, y=199.4, z=300)
    filter_from(deck.FILTER_STATION)
    # move to A1
    # x=-273.4, y=82.0
    deck.n9.move(x=-288.0, y=116.0, z=170, gripper=149.691)
    # go down and drop filter at 'A1'
    deck.n9.move(x=-288.0, y=116.0, z=113, gripper=149.635)
    deck.n9.move(x=-288.0, y=116.0, z=100, gripper=149.635)
    deck.c9.output(0, False)
    # go to safe height
    deck.n9.move(x=-288.0, y=116.0, z=300, gripper=149.635)


def filter_from(location: Location):
    # go up to safe height
    deck.n9.move(location.x, location.y, z=300)
    # open gripper
    deck.c9.output(0, False)
    # go down and pick up tube
    deck.n9.move(location.x, location.y, location.z + 7)
    deck.n9.move(location.x,location.y,location.z)
    deck.c9.output(0, True)
    time.sleep(1)
    # go up to safe height
    deck.n9.move(location.x, location.y, z=300)


def filter_to(location: Location):
    # go up to safe height
    deck.n9.move(location.x, location.y, z=300)
    # go down and drop filter at 'A1'
    deck.n9.move(location.x, location.y, location.z + 15)
    deck.n9.move(location.x, location.y, location.z + 3)
    deck.c9.output(0, False)
    # go to safe height
    deck.n9.move(location.x, location.y, z=300)


def remove_filter_filterstation():
    filter_from(deck.FILTER_STATION)
    filter_to(deck.SOLID_WASTE)
    # # go down and dispose
    # deck.n9.move(x=-249, y=161, z=117)
    # deck.c9.output(0, False)
    # # go to safe height
    # deck.n9.move(x=-249, y=161, z=300)


# def filter_to_gripper():
#     # go to safe middle location at safe height
#     deck.n9.move(x=-199.6, y=199.4, z=300)
#     # open gripper
#     deck.c9.output(0, False)
#     # go down and pick up tube
#     deck.n9.move(x=-199.6, y=199.4, z=155)
#     deck.n9.move(x=-199.6, y=199.4, z=148.867)
#     deck.c9.output(0, True)
#     # go up to safe height
#     deck.n9.move(x=-199.6, y=199.4, z=300)
#     # move to gripper
#     deck.n9.move(x=-187.3, y=-13.4, z=300)
#     deck.n9.move(x=-186.8, y=-11.9, z=137)
#     deck.n9.move(x=-186.8, y=-11.9, z=133)
#     deck.c9.output(0, False)
#     deck.n9.move(x=-187.3, y=-13.4, z=300)


def tube_to_capstation():
    # safe height
    deck.n9.move(x=-242.86, y=117.70, z=300, gripper=-84.72)
    # approach
    deck.n9.move(x=-242.86, y=117.70, z=105)
    # push in
    deck.n9.move(x=-242.86, y=117.70, z=96.52)
    # open gripper
    deck.c9.output(0, False)
    # safe height
    deck.n9.move(x=-242.86, y=117.70, z=300)


def tube_from_capstation():
    # safe height
    deck.n9.move(x=-242.86, y=117.70, z=300, gripper=-84.72)
    # open gripper
    deck.c9.output(0, False)
    # approach
    deck.n9.move(x=-242.86, y=117.70, z=105)
    # push in
    deck.n9.move(x=-242.86, y=117.70, z=98)
    # close gripper
    deck.c9.output(0, True)
    # gradually pull out
    deck.n9.move(x=-242.86, y=117.70, z=105)
    # safe height
    deck.n9.move(x=-242.86, y=117.70, z=300)


def pickup_opener():
    # safe height
    deck.n9.move(x=-252.1, y=95.6, z=300, gripper=-178.918)
    # open gripper
    deck.c9.output(0, False)
    # go down
    deck.n9.move(x=-252.1, y=95.6, z=97.53)
    # close gripper
    deck.c9.output(0, True)
    # bring up
    deck.n9.move(x=-252.1, y=95.6, z=300)


def park_opener():
    # safe height
    deck.n9.move(x=-252.1, y=95.6, z=300, gripper=-178.918)
    # go down
    deck.n9.move(x=-252.1, y=95.6, z=97.53)
    # open gripper
    deck.c9.output(0, False)
    # safe height
    deck.n9.move(x=-252.1, y=95.6, z=300)


def uncap_tube():
    pickup_opener()
    # safe height
    # deck.n9.move(x=-223.7, y=-24.1, z=300, gripper=179.70569482421877)
    deck.n9.move(x=-285, y=116, z=300, gripper=178.26)
    # push filter down first
    deck.n9.move(x=-241.5, y=116.6, z=155)
    deck.n9.move(x=-241.5, y=116.6, z=149)
    deck.n9.move(x=-285, y=116, z=151, gripper=178.26)
    # approach position
    # deck.n9.move(x=-223.7, y=-24.1, z=148)
    deck.n9.move(x=-270, y=116, z=140)
    # open prepare position
    # deck.n9.move(x=-207.8, y=-18.7, z=150.3265380859375)
    deck.n9.move(x=-263.3, y=116.6, z=143)
    # open position
    # deck.n9.move(x=-207.8, y=-18.7, z=158.04)
    deck.n9.move(x=-263.3, y=116.6, z=151)
    # cap push prepare position
    # deck.n9.move(x=-176.3, y=-12.6, z=158.04)
    ##### deck.n9.move(x=-250.5, y=116.6, z=170)
    deck.n9.move(x=-218.5, y=116.6, z=151)
    # cap push position
    # deck.n9.move(x=-176.3, y=-12.6, z=146.37)
    deck.n9.move(x=-218.5, y=116.6, z=134)
    time.sleep(3)
    # safe height, move away
    # deck.n9.move(x=-151, y=127, z=300)
    deck.n9.move(x=-217.8, y=116.6, z=300)
    park_opener()


def cap_tube():
    pickup_opener()
    ############# cap eppendorf tube ############
    deck.n9.move(x=-201.8, y=128.0, z=300, gripper=-90.46)
    # push filter down first
    deck.n9.move(x=-260, y=128.0, z=300)
    deck.n9.move(x=-241.5, y=116.6, z=155)
    deck.n9.move(x=-241.5, y=116.6, z=148)
    deck.n9.move(x=-201.8, y=128.0, z=220, gripper=-90.46)
    # approach position
    # deck.n9.move(x=-136.81, y=-14, z=139.74, gripper=12.138)
    deck.n9.move(x=-201.8, y=128.0, z=128)
    # close prepare position
    # deck.n9.move(x=-157.1167724609375, y=-14, z=139.74)
    deck.n9.move(x=-220, y=128.0, z=128)
    # close position
    # deck.n9.move(x=-157.1167724609375, y=-14, z=146.3)
    deck.n9.move(x=-227.5, y=129.2, z=153.77)
    # stand back position
    # deck.n9.move(x=-155.1, y=-13.9711181640625, z=146.3)
    # prepare to push position
    # deck.n9.move(x=-155.1, y=-14, z=165)
    # push position
    # deck.n9.move(x=-184, y=-14, z=165)
    deck.n9.move(x=-241.5, y=129.2, z=153.77)
    # cap position
    # deck.n9.move(x=-184, y=-14, z=146.5)
    deck.n9.move(x=-241.5, y=129.2, z=146)
    # safe height, move away
    # deck.n9.move(x=-151, y=127, z=300)
    deck.n9.move(x=-241.5, y=129.2, z=300)
    park_opener()


def dispose_solid_waste():
    location = deck.SOLID_WASTE
    safe_location = Location(location.x, location.y, z=300)
    # go up to safe height
    # deck.n9.move(x=-240.8, y=117.4, z=264)
    deck.n9.move(location.x, location.y, z=300)
    # go down and dispose
    deck.n9.move(location.x, location.y, location.y)
    deck.c9.output(0, False)
    # go to safe height
    deck.n9.move(location.x, location.y, z=300)


# def vial_to_vision(vision_index: str = 'A1', drop_times: int = 0, capped: bool=False):
#    location = deck.vision_grid.locations[vision_index]
#    safe_location = Location(x=location.x, y=location.y, z=300)
#    deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#    deck.n9.move_to_location(location, probe=False, gripper=-90)
#    deck.c9.output(0, False)
#    time.sleep(1)
#    deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#    #location = deck.vision_grid.locations[vision_index]
#    #face_hole_location = Location(x=location.x, y=200, z=location.z)
#    #safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
#    #deck.n9.move_to_location(safe_location, probe=False, gripper=0)
#    #deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
#    #deck.n9.move_to_location(location, probe=False, gripper=0)
#    #deck.c9.output(0, False)
#    #time.sleep(1)
#    #deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
#    #deck.n9.move_to_location(safe_location, probe=False, gripper=0)

# def vial_to_vision(vision_index: str = 'A1', drop_times: int = 0, capped: bool=False):
#     if vision_index == 'A1':
#         location = deck.vision_grid.locations[vision_index]
#         face_hole_location = Location(x=location.x, y=200, z=location.z)
#         safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
#         safe_location2 = Location(x=location.x, y=location.y, z=300)
#         deck.n9.move_to_location(safe_location, probe=False, gripper=90)
#         deck.n9.move_to_location(face_hole_location, probe=False, gripper=90)
#         deck.n9.move_to_location(location, probe=False, gripper=90)
#         time.sleep(1)
#         deck.c9.output(0, False)
#         time.sleep(1)
#         deck.n9.move_to_location(safe_location2, probe=False, gripper=90)
#     if vision_index == 'A2':
#         location = deck.vision_grid.locations[vision_index]
#         safe_location = Location(x=location.x, y=location.y, z=300)
#         grip_cap_location =Location(x=location.x, y=location.y, z=135.6)
#         deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#         deck.n9.move_to_location(grip_cap_location, probe=False, gripper=-90)
#         deck.c9.output(0, False)
#         time.sleep(1)
#         deck.n9.move_to_location(safe_location, probe=False, gripper=-90)\

def open_vision_station():
    # open the station
    location = deck.VISION_OPEN_INIT
    safe_location = Location(location.x, location.y, 310)
    approach_location = Location(location.x, location.y, location.z + 35)
    deck.n9.move_to_location(safe_location,gripper=90, probe=False)
    deck.n9.move_to_location(approach_location, probe=False,gripper=90)
    deck.n9.move_to_location(location, probe=False,gripper=90)
    # push open
    deck.n9.move_to_location(deck.VISION_OPEN_FINAL, probe=False,gripper=90)
    deck.n9.move_to_location(location, probe=False,gripper=90)

    # up at safe height
    deck.n9.move_to_location(safe_location, probe=False,gripper=90)


def close_vision_station():
    # open the station
    location = deck.VISION_CLOSE_INIT
    safe_location = Location(location.x, location.y, 310)
    approach_location = Location(location.x, location.y, location.z + 5)
    deck.n9.move_to_location(safe_location, gripper=90, probe=False)
    deck.n9.move_to_location(approach_location, probe=False,gripper=90)
    deck.n9.move_to_location(location, probe=False,gripper=90)
    # push close
    deck.n9.move_to_location(deck.VISION_CLOSE_FINAL, probe=False,gripper=90)
    # up at safe height
    deck.n9.move_to_location(location, probe=False,gripper=90)

    deck.n9.move_to_location(safe_location, probe=False,gripper=90)



# heat station ver
def vial_to_vision(vision_index: str = 'A1', drop_times: int = 0, capped: bool = False):
    '''
    OPEN VISION STATION
    '''
    if vision_index == 'A1':
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location = Location(x=location.x, y=location.y, z=location.z + 30)
        deck.n9.move_to_location(safe_location, probe=False, gripper=90)
        deck.n9.move_to_location(approach_location, probe=False, gripper=90)
        deck.n9.move_to_location(location, probe=False, gripper=90)
        time.sleep(1)
        deck.c9.output(0, False)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False, gripper=90)
    if vision_index == 'A2':
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location2 = Location(x=location.x, y=location.y, z=location.z + 30)
        deck.n9.move_to_location(safe_location, probe=False, gripper=146.86)
        deck.n9.move_to_location(approach_location2, probe=False, gripper=146.86)
        deck.n9.move_to_location(location, probe=False, gripper=146.86)
        time.sleep(1)
        deck.c9.output(0, False)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False, gripper=146.86)


# heating station ver
def vial_from_vision(vision_index: str = 'A1', capped: bool = False):
    open_vision_station()
    if vision_index == 'A1':
        # arm gripper should be open
        deck.c9.output(0, False)

        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location = Location(x=location.x, y=location.y, z=location.z + 30)
        deck.n9.move_to_location(safe_location, probe=False, gripper=90)
        deck.n9.move_to_location(approach_location, probe=False)
        deck.n9.move_to_location(location, probe=False)
        time.sleep(1)
        deck.c9.output(0, True)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)
    if vision_index == 'A2':
        # turbidity station
        deck.c9.output(0, False)
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location2 = Location(x=location.x, y=location.y, z=location.z + 30)
        deck.n9.move_to_location(safe_location, probe=False, gripper=90)
        deck.n9.move_to_location(approach_location2, probe=False)
        deck.n9.move_to_location(location, probe=False)
        time.sleep(1)
        deck.c9.output(0, True)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)


def tube_to_vision(vision_index: str = 'A1', drop_times: int = 0, capped: bool = False):
    '''
    OPEN VISION STATION
    '''
    if vision_index == 'A1':
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location = Location(x=location.x, y=location.y, z=location.z + 30)
        release_location = Location(x=location.x, y=location.y, z=location.z + 8)
        deck.n9.move_to_location(safe_location, probe=False, gripper=118.5)
        deck.n9.move_to_location(approach_location, probe=False)
        deck.n9.move_to_location(release_location, probe=False)
        time.sleep(1)
        deck.c9.output(0, False)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)
    if vision_index == 'A2':
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location2 = Location(x=location.x, y=location.y, z=location.z + 30)
        release_location = Location(x=location.x, y=location.y, z=location.z + 8)
        deck.n9.move_to_location(safe_location, probe=False, gripper=118.5)
        deck.n9.move_to_location(approach_location2, probe=False)
        deck.n9.move_to_location(release_location, probe=False)
        time.sleep(1)
        deck.c9.output(0, False)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)


# heating station ver
def tube_from_vision(vision_index: str = 'A1', capped: bool = False):
    if vision_index == 'A1':
        # arm gripper should be open
        deck.c9.output(0, False)

        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location = Location(x=location.x, y=location.y, z=location.z + 30)
        grip_location = Location(x=location.x, y=location.y, z=location.z + 5)
        deck.n9.move_to_location(safe_location, probe=False, gripper=86.53)
        deck.n9.move_to_location(approach_location, probe=False)
        deck.n9.move_to_location(grip_location, probe=False)
        time.sleep(1)
        deck.c9.output(0, True)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)
    if vision_index == 'A2':
        # turbidity station
        deck.c9.output(0, False)
        location = deck.vision_grid.locations[vision_index]
        safe_location = Location(x=location.x, y=location.y, z=300)
        approach_location2 = Location(x=location.x, y=location.y, z=location.z + 30)
        grip_location = Location(x=location.x, y=location.y, z=location.z + 5)
        deck.n9.move_to_location(safe_location, probe=False, gripper=86.53)
        deck.n9.move_to_location(approach_location2, probe=False)
        deck.n9.move_to_location(grip_location, probe=False)
        time.sleep(1)
        deck.c9.output(0, True)
        time.sleep(1)
        deck.n9.move_to_location(safe_location, probe=False)

def needle_to_vision_safe(index, volume):
    # get needle out of vision_needle_check vial
    location = deck.vision_grid_needle.locations['A1']  # todo double check is correct
    safe_location = location.copy(z=300)
    pierce_location = location.copy(z=187)
    approach_location = location.copy(z=200)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)

def needle_to_vision_dispense():
    location = deck.vision_grid_needle.locations['A1']  # todo double check is correct
    safe_location = location.copy(z=300)
    pierce_location = location.copy(z=187)
    approach_location = location.copy(z=200)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)
    # needle in
    deck.n9.move_to_location(approach_location, gripper=109, probe=True)
    deck.n9.move_to_location(pierce_location, gripper=109, probe=True)
    deck.dosing_pump.move_absolute_ml(0, deck.SOLVENT_DRAW_RATE)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)

def needle_to_vision():
    location = deck.vision_grid_needle.locations['A1']  # todo double check is correct
    safe_location = location.copy(z=300)
    pierce_location = location.copy(z=187)
    approach_location = location.copy(z=200)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)
    # needle in
    deck.n9.move_to_location(approach_location, gripper=109, probe=True)
    deck.n9.move_to_location(pierce_location, gripper=109, probe=True)

def needle_from_vision():
    location = deck.vision_grid_needle.locations['A1']  # todo double check is correct
    safe_location = location.copy(z=300)
    deck.n9.move_to_location(safe_location, gripper=109, probe=True)

# def vial_from_vision(vision_index: str = 'A1', capped: bool = False):
#    # arm gripper should be open
#    deck.c9.output(0, False)
#    # open the vision_needle_check door
#    #poke_vision_top_open()
#    location = deck.vision_grid.locations[vision_index]
#    safe_location = Location(x=location.x, y=location.y, z=300)
#    deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#    deck.n9.move_to_location(location, probe=False, gripper=-90)
#    deck.c9.output(0, True)
#    deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#   # safe_location = location.copy(z=300)
#   # deck.n9.move_to_location(safe_location, probe=False)
#   # if capped:
#   #     grip_cap_location = location.copy(z=145.5)
#   # else:
#    #    grip_cap_location = location.copy(z=132.2)
#   # deck.n9.move_to_location(grip_cap_location)
#    #deck.c9.output(0, True)
#    #deck.n9.move_to_location(safe_location, probe=False)
#    # arm gripper should be open
#    #deck.c9.output(0, False)
#    # open the vision_needle_check door
#    #poke_vision_top_open()
#   #location = deck.vision_grid.locations[vision_index]
#   #face_hole_location = Location(x=location.x ,y=200, z=location.z)
#   #safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
#   #grip_cap_location = Location(x=location.x, y=location.y, z=146.2)
#   #deck.n9.move_to_location(safe_location, probe=False, gripper=0)
#   #deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
#   #deck.n9.move_to_location(location, probe=False, gripper=0)
#   #deck.n9.move_to_location(grip_cap_location, probe=False, gripper=0)
#   #deck.c9.output(0, True)
#   #deck.n9.move_to_location(location, probe=False, gripper=0)
#   #deck.n9.move_to_location(face_hole_location, probe=False, gripper=0)
#   #deck.n9.move_to_location(safe_location, probe=False, gripper=0)
#   # safe_location = location.copy(z=300)
#   # deck.n9.move_to_location(safe_location, probe=False)
#   # if capped:
#   #     grip_cap_location = location.copy(z=145.5)
#   # else:
#   ##    grip_cap_location = location.copy(z=132.2)
#   # deck.n9.move_to_location(grip_cap_location)
#   ##deck.c9.output(0, True)
#   ##deck.n9.move_to_location(safe_location, probe=False)

# def vial_from_vision(vision_index: str = 'A1', capped: bool = False):
#     if vision_index == 'A1':
#         # arm gripper should be open
#         deck.c9.output(0, False)
#         # open the vision door
#         #poke_vision_top_open()
#         location = deck.vision_grid.locations[vision_index]
#         face_hole_location = Location(x=location.x ,y=200, z=location.z)
#         safe_location_1 = Location(x=location.x, y=location.y, z=300)
#         safe_location = Location(x=face_hole_location.x, y=face_hole_location.y, z=300)
#         grip_cap_location = Location(x=location.x, y=location.y, z=144.5)
#         deck.n9.move_to_location(safe_location_1, probe=False, gripper=90)
#         deck.n9.move_to_location(location, probe=False, gripper=90)
#         deck.n9.move_to_location(grip_cap_location, probe=False, gripper=90)
#         time.sleep(1)
#         deck.c9.output(0, True)
#         time.sleep(1)
#         deck.n9.move_to_location(location, probe=False, gripper=90)
#         deck.n9.move_to_location(face_hole_location, probe=False, gripper=90)
#         deck.n9.move_to_location(safe_location, probe=False, gripper=90)
#     if vision_index == 'A2':
#         # turbidity station
#         deck.c9.output(0, False)
#         location = deck.vision_grid.locations[vision_index]
#         safe_location = Location(x=location.x, y=location.y, z=300)
#         grip_cap_location = Location(x=location.x, y=location.y, z=135.2)
#
#         deck.n9.move_to_location(safe_location, probe=False, gripper=-90)
#         deck.n9.move_to_location(grip_cap_location, probe=False, gripper=-90)
#         deck.c9.output(0, True)
#         deck.n9.move_to_location(safe_location, probe=False, gripper=-90)


def vial_to_gripper(rxn_index: str = 'A1', capped: bool = False, from_tray: bool = False, scale_check=False):
    # check if gripper empty
    deck.c9.output(1, False)
    if scale_check:
        if deck.scale.settled_weigh(matching=2) > 0.05:
            action = input(print("GRIPPER NOT EMPTY"))
    if from_tray:
        # have arm gripper open
        deck.c9.output(0, False)
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, gripper=109)
        if capped:
            grip_cap_location = location.copy(z=169.7)
        else:
            grip_cap_location = location.copy(z=162.9)
        deck.n9.move_to_location(grip_cap_location, gripper=109)
        # # gripper closes around vial's cap
        deck.c9.output(0, True)
        # # bring it to safe height Position: x=-193.2, y=260.3, z=186.005, gripper=150.61699999999996
        deck.n9.move_to_location(safe_location, gripper=109)
    # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62)
    if capped:
        # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=117.05, gripper=150.62)
    else:
        # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=110, gripper=150.62)
    # scale gripper closes around vial
    deck.c9.output(0, False)
    time.sleep(0.5)
    deck.c9.output(1, True)
    # arm gripper opens around the cap

    # gripper goes up to safe height
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62099999999998)


def vial_from_gripper(rxn_index='A1', capped: bool = False, to_tray: bool = False):
    deck.c9.output(0, False)
    deck.c9.output(1, True)
    # todo add gripper location to deck
    # go to scale still on safe height Position: x=-186.3, y=-16.3, z=186.005, gripper=149.60299999999995
    deck.n9.move(x=-186.6, y=-13.9, z=300, gripper=150.62)
    if capped:
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=116, gripper=150.62)
    else:
        # # # go down to scale gripper Position: x=-186.24, y=-13.9, z=97.30800000000001, gripper=109.299999999995
        deck.n9.move(x=-186.6, y=-13.9, z=108.18, gripper=150.62)

    # arm gripper closes around the cap
    deck.c9.output(0, True)
    # scale gripper open around vial
    deck.c9.output(1, False)
    time.sleep(2)
    # gripper goes up to safe height
    deck.n9.move(x=-186.6, y=-13.9, z=250, gripper=150.62099999999998)
    if to_tray:
        location = deck.reaction_grid_gripper.locations[rxn_index]
        safe_location = location.copy(z=300)
        deck.n9.move_to_location(safe_location, gripper=150)
        if capped:
            grip_cap_location = location.copy(z=169.7)
        else:
            grip_cap_location = location.copy(z=146)
        deck.n9.move_to_location(grip_cap_location, gripper=150)
        # # gripper opens around vial's cap
        deck.c9.output(0, False)
        # # bring it to safe height Position: x=-193.2, y=260.3, z=186.005, gripper=150.61699999999996
        deck.n9.move_to_location(safe_location, gripper=150)

def filter_common_zone():
    deck.c9.output(0, False)
    deck.n9.move(x=-241.92989501953124, y=117.05677490234375, z=300, gripper=175.59500000000003)
    deck.n9.move(x=-241.92989501953124, y=117.05677490234375, z=105, gripper=175.59500000000003)
    deck.c9.output(0, True)
    deck.n9.move(x=-241.92989501953124, y=117.05677490234375, z=134, gripper=175.59500000000003)
    deck.n9.move(x=-241.92989501953124, y=117.05677490234375, z=300, gripper=175.59500000000003)

def close_common_zone_gripper():
    deck.c9.output(0, True)
    deck.n9.move(x=-200.94039306640624, y=107.6800048828125, z=300, gripper=168.9275)
    deck.n9.move(x=-200.94039306640624, y=107.6800048828125, z=100.9, gripper=168.9275)
    deck.n9.move(x=-224.44039306640624, y=107.6800048828125, z=100.9, gripper=168.9275)
    deck.n9.move(x=-200.94039306640624, y=107.6800048828125, z=100.9, gripper=168.9275)
    deck.n9.move(x=-200.94039306640624, y=107.6800048828125, z=300, gripper=168.9275)

def open_common_zone_gripper():
    deck.c9.output(0, True)
    deck.n9.move(x=-257.907373046875, y=104.42664794921875, z=300, gripper=169.55219482421876)
    deck.n9.move(x=-257.907373046875, y=104.42664794921875, z=100.2, gripper=169.55219482421876)
    deck.n9.move(x=-234.40737304687502, y=104.42664794921875, z=100.2, gripper=169.55219482421876)
    deck.n9.move(x=-257.907373046875, y=104.42664794921875, z=100.2, gripper=169.55219482421876)
    deck.n9.move(x=-257.907373046875, y=104.42664794921875, z=300, gripper=169.55219482421876)
    deck.n9.move(x=-199.100537109375, y=262.2477783203125, z=300, gripper=150.62099999999998)



def insert_dispense_head(wall_index: str = None):
    # n9 out of the path- to waste beaker at safe height
    deck.n9.move(x=29.25811767578125, y=148.32469482421874, z=300, gripper=2.9949999999999903, probe=True,
                 elbow_bias=deck.c9.BIAS_MAX_SHOULDER)
    # open Quantos door
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    deck.quan.move_z_stage(2000)

    # ur sequence
    sequence2 = deck.grid_sequence
    grid = Grid.from_locations(sequence2['top_left'], sequence2['bottom_left'], sequence2['bottom_right'], rows=3,
                               columns=1)
    deck.ur.move_joints(sequence2.joints['approach_grid'], velocity=20, acceleration=40)
    deck.ur.open_gripper(0.34, 0, 0.8)
    location = grid[wall_index]
    safe_location = location.translate(x=0.100)
    deck.ur.move_to_location(safe_location, velocity=100, acceleration=200)
    deck.ur.move_to_location(location, velocity=100, acceleration=200)
    deck.ur.open_gripper(0.61, 0, 0.8)
    deck.ur.move_to_location(safe_location, velocity=100, acceleration=200)

    sequence1 = deck.gun_swap_sequence
    deck.ur.move_joints(sequence1.joints['Waypoint_8'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['turnup'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_4'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_5'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_6'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['swing'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_3'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_1'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_2'], velocity=20, acceleration=40)
    deck.ur.open_gripper(0.31, 0, 0.8)
    deck.ur.move_joints(sequence1.joints['Waypoint_1'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_3'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['swing'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_6'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_5'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_4'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['turnup'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_8'], velocity=20, acceleration=40)


def remove_dispense_head(wall_index: str = None):
    # n9 out of the path- to waste beaker at safe height
    deck.n9.move(x=29.25811767578125, y=148.32469482421874, z=300, gripper=2.9949999999999903, probe=True,
                 elbow_bias=deck.c9.BIAS_MAX_SHOULDER)
    # open Quantos door
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "open"
    deck.quan.move_z_stage(2000)
    deck.quan.unlock_dosing_pin_position()

    deck.ur.open_gripper(0.34, 0, 0.8)
    sequence1 = deck.gun_swap_sequence
    deck.ur.move_joints(sequence1.joints['Waypoint_8'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['turnup'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_4'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_5'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_6'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['swing'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_3'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_1'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_2'], velocity=20, acceleration=40)
    deck.ur.open_gripper(0.61, 0, 0.8)
    deck.ur.move(z=2.5, relative=True)
    deck.ur.move_joints(sequence1.joints['Waypoint_1'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_3'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['swing'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_6'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_5'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_4'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['turnup'], velocity=20, acceleration=40)
    deck.ur.move_joints(sequence1.joints['Waypoint_8'], velocity=20, acceleration=40)
    # ur sequence
    sequence2 = deck.grid_sequence
    grid = Grid.from_locations(sequence2['top_left'], sequence2['bottom_left'], sequence2['bottom_right'], rows=2,
                               columns=1)
    # deck.ur.move_joints(sequence2.joints['approach_grid'], velocity=20, acceleration=40)
    location = grid[wall_index]
    safe_location = location.translate(x=100, z=1.5)
    deck.ur.move_to_location(safe_location, velocity=100, acceleration=200)
    deck.ur.move_to_location(location.translate(z=1.5), velocity=100, acceleration=200)
    deck.ur.move_to_location(location, velocity=100, acceleration=200)
    deck.ur.open_gripper(0.34, 0, 0.8)
    deck.ur.move_to_location(safe_location, velocity=100, acceleration=200)

    # home quantos z stage and close the door
    deck.quan.home_z_stage()
    deck.quan.front_door_position = "close"


if __name__ == '__main__':
    #vial_from_quantos(filter=False)
    open_vision_station()
    close_vision_station()
    for i in range(1000):
        vial_from_gripper(capped=False,to_tray=False)
        vial_to_quantos(filter=False)
        dose_with_quantos(solid_weight=2)
        vial_from_quantos(filter=False)
        vial_to_gripper(capped=False,from_tray=False)

