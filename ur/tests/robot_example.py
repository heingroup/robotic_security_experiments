from hein_robots.universal_robots.ur3 import UR3Arm  #class for arm commands
from hein_robots.universal_robots.urscript_sequence import URScriptSequence #class for reading move and path info from .script file
from hein_robots.robotics import Location, Cartesian  #class for reading waypoints coordinates , exmaple not included in this file
from hein_robots.grids import Grid,StaticGrid   #class for defining grids, example not included in this file


#arm instantiation, use proper IP address
arm = UR3Arm('192.168.254.88')

# let's open and close the gripper, value between 0 for fully open and 1 for fully close
arm.open_gripper(0.7)
arm.open_gripper(0.2)


#lets move the robot between two points you found on the tablet

#sequence file path , path of the directory where .script file is locatied
example_sequence_file='C:/where/your/file/is.script'
sequence=URScriptSequence(example_sequence_file)

#move the arm to point1 , use movej command for joint movements and movel for linear, general rule is linear move for short distances and joint for long
arm.move_joints(sequence.joints['Waypoint1'], velocity=40)

#move arm to point 2
arm.move_joints(sequence.joints['Waypoint2'], velocity=40)
