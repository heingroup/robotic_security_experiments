import niraapad.backends # Required so that Niraapad's classes are used instead
from niraapad.lab_computer.niraapad_client import NiraapadClient
NiraapadClient.connect_to_middlebox(host_name, port) # Connect to the middlebox server
from niraapad.shared.utils import MO
from niraapad.shared.utils import BACKENDS
NiraapadClient.update_mos(exceptions={BACKENDS.ARDUINO_AUGMENTED_QUANTOS: MO.DIRECT_PLUS_MIDDLEBOX})

from ur.configuration import ur_deck

ur_deck.ur3_centrifuge.run_centrifuge(5)
