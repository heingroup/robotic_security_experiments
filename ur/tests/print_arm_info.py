import sys
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.robotics import Location
from datetime import datetime
import math


arm = UR3Arm('192.168.254.88')
print("robot created")

connected = arm.connected
print("arm is connected!", connected)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
count = arm.joint_count
print("count StartTime is: ", StartTime)
print("joint_count is: ", count)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
Velocity = arm.default_joint_velocity
print("velocity StartTime is: ", StartTime)
print("velocity is: ", Velocity)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
Acceleration = arm.acceleration
print("acceleration StartTime is: ", StartTime)
print("acceleration is: ", Acceleration)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
position = arm.pose
print("pose StartTime is: ", StartTime)
print("printing first position")
print(position)


StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
location = arm.location
print("location StartTime is: ", StartTime)
print("printing first location..")
print(location)

targets = [Location(x=145.7, y=-310.0, z=189.9, rx=-179.4, ry=1.262, rz=126.3),
           Location(x=128.7, y=-139.2, z=234.1, rx=-178.9, ry=2.612, rz=173.9),
           Location(x=145.7, y=-310.0, z=96.61, rx=-179.4, ry=1.262, rz=126.3)
           ]

for target in targets:
    StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    arm.move_to_location(target)
    print("move_to_location StartTime is: ", StartTime)
    print("printing next location..")
    print(target)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
position = arm.pose
print("position StartTime is: ", StartTime)
print("printing next position")
print(position)
angles = list(map(lambda i: math.degrees(i), position.orient.to_euler('xyz')))
print("printing angles and locations..")
print(angles)
location = arm.location
print(location)

targets = [
    ([-267.69808031125496, -86.643030336582, 77.15383416730805, -81.88499761734606, 270.40593248599754,
      56.11333716396972], Location(x=145.7, y=-310.0, z=96.61, rx=-179.4, ry=1.262, rz=126.3)),
    ([-276.1051734415304, -124.89270202488177, 89.62018483901277, -55.80357324058484, 272.86178315711203, 0.0],
     Location(x=145.7, y=-310.0, z=96.61, rx=-179.4, ry=1.262, rz=126.3)),
    ([-267.713871708616, -85.4219564375238, 101.14796087248268, -107.09969643004598, 270.3634760301167,
      56.16277407318227], Location(x=145.7, y=-310.0, z=96.61, rx=-179.4, ry=1.262, rz=126.3))
]

for joints, location in targets:
    StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    arm.move_joints(joints)
    print("move_joints StartTime is: ", StartTime)

StartTime = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
JPosition = arm.joint_positions
print("joint_position StartTime is: ", StartTime)
print("printing joint position")
print(JPosition)

arm.disconnect()