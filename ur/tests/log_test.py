import logging
logging.basicConfig(filename='debug.log', format='%(asctime)s %(levelname)s:%(name)s:%(message)s')
logging.getLogger('hein_robots.base.robot_arms').setLevel(logging.DEBUG)
logging.getLogger('ursecmon').setLevel(logging.ERROR)
from hein_robots.universal_robots.ur3 import UR3Arm
arm = UR3Arm('192.168.254.88')

arm.open_gripper(0.1)