# Script to detect markers in HSV value range

# import relevant libraries
import cv2
import numpy as np
from heinsight.vision_utilities.camera import Camera

def nothing(x):
    pass

def height_width(image):
    """
    Find the height and width of an image, whether it is a grey scale image or not

    :param image: an image, as a numpy array
    :return: int, int: the height and width of an image
    """
    if len(image.shape) is 3:
        image_height, image_width, _ = image.shape
    elif len(image.shape) is 2:
        image_height, image_width = image.shape
    else:
        raise ValueError('Image must be passed as a numpy array and have either 3 or 2 channels')

    return image_height, image_width

# Load image
# image = cv2.imread(r'angle_detection/dots/version_2/dot images/2021_03_09_10_43_41_477095.jpg')

camera = Camera(2)
image = camera.take_photo(save_photo=False)
height, width = height_width(image)
# hsv_img = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

# Create a window
cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.resizeWindow(winname='image', width=width, height=height)

# Create trackbars for color change
# Hue is from 0-179 for Opencv
cv2.createTrackbar('HMin', 'image', 0, 179, nothing)
cv2.createTrackbar('SMin', 'image', 0, 255, nothing)
cv2.createTrackbar('VMin', 'image', 0, 255, nothing)
cv2.createTrackbar('HMax', 'image', 0, 179, nothing)
cv2.createTrackbar('SMax', 'image', 0, 255, nothing)
cv2.createTrackbar('VMax', 'image', 0, 255, nothing)

# Set default value for Max HSV trackbars
cv2.setTrackbarPos('HMax', 'image', 179)
cv2.setTrackbarPos('SMax', 'image', 255)
cv2.setTrackbarPos('VMax', 'image', 255)

# Initialize HSV min/max values
hMin = sMin = vMin = hMax = sMax = vMax = 0
phMin = psMin = pvMin = phMax = psMax = pvMax = 0

while(1):
    # Get current positions of all trackbars
    hMin = cv2.getTrackbarPos('HMin', 'image')
    sMin = cv2.getTrackbarPos('SMin', 'image')
    vMin = cv2.getTrackbarPos('VMin', 'image')
    hMax = cv2.getTrackbarPos('HMax', 'image')
    sMax = cv2.getTrackbarPos('SMax', 'image')
    vMax = cv2.getTrackbarPos('VMax', 'image')

    # Set minimum and maximum HSV values to display
    lower = np.array([hMin, sMin, vMin])
    upper = np.array([hMax, sMax, vMax])

    # Convert to HSV format and color threshold
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    result = cv2.bitwise_and(image, image, mask=mask)

    # Print if there is a change in HSV value
    if((phMin != hMin) | (psMin != sMin) | (pvMin != vMin) | (phMax != hMax) | (psMax != sMax) | (pvMax != vMax) ):
        print("(hMin = %d , sMin = %d, vMin = %d), (hMax = %d , sMax = %d, vMax = %d)" % (hMin , sMin , vMin, hMax, sMax , vMax))
        phMin = hMin
        psMin = sMin
        pvMin = vMin
        phMax = hMax
        psMax = sMax
        pvMax = vMax

    # Display result image
    cv2.imshow('image', result)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()