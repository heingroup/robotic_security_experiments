from typing import Optional
import math
import time
from north_c9.axis import Output
from hein_robots.universal_robots.ur3 import UR3Arm
from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.robotics import Location
from ur.components.vision_angle_measurement.measurement import VisionDotAngleMeasurement, NoDotFoundException


class UR3Centrifuge:
    CENTRIFUGE_SEQUENCE_NAMES = ['center']
    COVER_SEQUENCE_NAMES = ['face_cent', 'cent_cover_sh', 'cent_cov_engage', 'cov_putdown_sh', 'cov_putdown_eng']
    VIAL_SEQUENCE_NAMES = ['home', 'safe_height1', 'engage1', 'r_safe_height1', 'rotor_engage', 'r_pickup1']
    ROD_SEQUENCE_NAMES = ['rotor_center', 'face_tray', 'rod_safeheight', 'rod_engage']

    CENTRIFUGE_STOP_TIME = 5.0

    def __init__(self, ur: UR3Arm, relay: Output, angle_measurement: VisionDotAngleMeasurement,
                 centrifuge_sequence_file: str, cover_sequence_file: str, vial_sequence_file: str, rod_sequence_file: str,
                 measurement_offset: Location = Location(y=100, z=50), safe_offset: float = 10, plunge_depth: float = 17, centrifuge_start_angle: float = 230,
                 centrifuge_fudge_angle: float = 23,
                 centrifuge_radius: float = 16, show_images: bool = False, joint_velocity: float = 60, linear_velocity: float = 80, rotate_velocity: float = 10):
        self.ur = ur
        self.relay = relay
        self.angle_measurement = angle_measurement

        self.centrifuge_sequence = URScriptSequence(centrifuge_sequence_file, location_names=self.CENTRIFUGE_SEQUENCE_NAMES)
        self.cover_sequence = URScriptSequence(cover_sequence_file, location_names=self.COVER_SEQUENCE_NAMES)
        self.vial_sequence = URScriptSequence(vial_sequence_file, location_names=self.VIAL_SEQUENCE_NAMES)
        self.rod_sequence = URScriptSequence(rod_sequence_file, location_names=self.ROD_SEQUENCE_NAMES)

        self.centrifuge_location = self.centrifuge_sequence['center']
        self.centrifuge_location_joints = self.centrifuge_sequence.joints['center']

        self.measurement_offset = measurement_offset
        self.safe_offset = safe_offset
        self.plunge_depth = plunge_depth
        self.centrifuge_start_angle = centrifuge_start_angle
        self.centrifuge_fudge_angle = centrifuge_fudge_angle
        self.show_images = show_images
        self.centrifuge_radius = centrifuge_radius

        self.joint_velocity = joint_velocity
        self.linear_velocity = linear_velocity
        self.rotate_velocity = rotate_velocity

    def start_spinning(self):
        self.relay.on()

    def stop_spinning(self):
        self.relay.off()

    def spin_centrifuge(self, duration: float):
        self.start_spinning()
        time.sleep(duration)
        self.stop_spinning()
        time.sleep(self.CENTRIFUGE_STOP_TIME)

    def move_to_measurement_location(self):
        measurement_location = self.centrifuge_location + self.measurement_offset
        self.ur.move_joints(self.centrifuge_location_joints, velocity=self.joint_velocity)
        self.ur.move_to_location(measurement_location, velocity=self.linear_velocity)

    def measure_centrifuge_angle(self) -> float:
        return self.centrifuge_start_angle - self.angle_measurement.measure_angle(show_image=self.show_images)

    def relative_index_location(self, index: int, angle: float, radius: Optional[float] = None):
        centrifuge_radius = radius if radius is not None else self.centrifuge_radius
        index_angle = index * 60.0 + angle

        return Location(
            x=math.cos(math.radians(index_angle)) * centrifuge_radius,
            y=math.sin(math.radians(index_angle)) * centrifuge_radius
        )

    def rotate_centrifuge(self):
        self.pickup_rod()

        angle = self.measure_centrifuge_angle()
        index_location = self.centrifuge_location + self.relative_index_location(0, angle)

        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=-self.plunge_depth), velocity=self.linear_velocity)

        final_angle = self.centrifuge_fudge_angle
        rotation = angle - final_angle

        lower_centrifuge_location = self.centrifuge_location.translate(z=-self.plunge_depth)
        midpoint = lower_centrifuge_location + self.relative_index_location(0, angle / 2)
        endpoint = lower_centrifuge_location + self.relative_index_location(0, 0)
        fudge_endpoint = lower_centrifuge_location + self.relative_index_location(0, self.centrifuge_fudge_angle)
        print(abs(rotation))
        if abs(rotation) > 80:
            self.ur.move_circular(midpoint, endpoint, velocity=self.rotate_velocity)
        else:
            self.ur.move_to_location(endpoint, velocity=self.rotate_velocity)

        self.ur.move_to_location(fudge_endpoint, velocity=self.rotate_velocity)
        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.place_rod()

    def rotate_centrifuge_inverse(self):
        self.pickup_rod()

        angle = 28.97
        index_location = Location(x=278.8, y=-2.789, z=152.8, rx=180.0, ry=-1.184e-11, rz=180.0)

        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.ur.move_to_location(index_location.translate(z=-self.plunge_depth), velocity=self.linear_velocity)

        # final_angle = self.centrifuge_fudge_angle
        rotation = 180

        lower_centrifuge_location = self.centrifuge_location.translate(z=-self.plunge_depth)
        # midpoint = lower_centrifuge_location + self.relative_index_location(0, angle / 2)
        # endpoint = lower_centrifuge_location + self.relative_index_location(0, 0)\
        midpoint=Location(x=260.4, y=4.846, z=135.8, rx=180.0, ry=-1.184e-11, rz=180.0)
        endpoint = Location(x=251.2, y=-18.98, z=152.8, rx=180.0, ry=-1.184e-11, rz=180.0)
        fudge_endpoint = lower_centrifuge_location + self.relative_index_location(0, self.centrifuge_fudge_angle)
        print(abs(rotation))

        self.ur.move_circular(midpoint, endpoint, velocity=self.rotate_velocity)

        # self.ur.move_to_location(fudge_endpoint, velocity=self.rotate_velocity)
        self.ur.move_to_location(self.centrifuge_location.translate(z=self.safe_offset), velocity=self.linear_velocity)
        self.place_rod()

    def pickup_cover_from_centrifuge(self):
        #move arm out of the view of camera
        self.ur.move_joints(self.cover_sequence.joints['cov_putdown_sh'], velocity=self.joint_velocity)
        try:
            raise NoDotFoundException
            self.measure_centrifuge_angle()
        except NoDotFoundException:
            self.ur.close_gripper(0)
            self.ur.move_joints(self.cover_sequence.joints['cent_cover_sh'], velocity=self.joint_velocity)
            self.ur.move_to_location(self.cover_sequence['cent_cov_engage'], velocity=self.linear_velocity)
            self.ur.close_gripper(0.65)
            self.ur.move_to_location(self.cover_sequence['cent_cover_sh'], velocity=self.linear_velocity)
        else:
            cover_status="off"
            return cover_status

    def place_cover_in_safe_location(self):
        self.ur.move_joints(self.cover_sequence.joints['cov_putdown_sh'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.cover_sequence['cov_putdown_eng'].translate(z=5), velocity=self.linear_velocity)
        self.ur.close_gripper(0.1)
        self.ur.move_to_location(self.cover_sequence['cov_putdown_sh'], velocity=self.linear_velocity)

    def pickup_cover_from_safe_location(self):
        self.ur.close_gripper(0.1)
        self.ur.move_to_location(self.cover_sequence['cov_putdown_sh'], velocity=self.linear_velocity)
        self.ur.move_to_location(self.cover_sequence['cov_putdown_eng'], velocity=self.linear_velocity)
        self.ur.close_gripper(0.7)
        self.ur.move_joints(self.cover_sequence.joints['cov_putdown_sh'], velocity=self.joint_velocity)

    def place_cover_on_centrifuge(self):
        self.ur.move_joints(self.cover_sequence.joints['cent_cover_sh'], velocity=self.joint_velocity)
        self.ur.move_joints(self.cover_sequence.joints['cent_cov_engage'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.1)
        self.ur.move_joints(self.cover_sequence.joints['cent_cover_sh'], velocity=self.joint_velocity)

    def pickup_vial_from_transfer_location(self):
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['engage1'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.73)
        self.ur.move_joints(self.vial_sequence.joints['safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def place_vial_in_transfer_location(self):
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['engage1'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.54)
        self.ur.move_joints(self.vial_sequence.joints['safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def pickup_vial_from_centrifuge(self):
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        #self.ur.move_joints(self.vial_sequence.joints['r_pickup1'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence['r_pickup1'],velocity=self.linear_velocity)
        self.ur.close_gripper(0.72)
        self.ur.move_to_location(self.vial_sequence['r_safe_height1'], velocity=self.linear_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def place_vial_in_centrifuge(self):
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['rotor_engage'], velocity=self.joint_velocity)
        self.ur.move_to_location(self.vial_sequence['r_pickup1'], velocity=self.linear_velocity)
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.vial_sequence.joints['r_safe_height1'], velocity=self.joint_velocity)
        self.ur.move_joints(self.vial_sequence.joints['home'], velocity=self.joint_velocity)

    def pickup_rod(self):
        self.ur.close_gripper(0)
        self.ur.move_joints(self.rod_sequence.joints['face_tray'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['rod_safeheight'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['rod_engage'], velocity=self.joint_velocity)
        self.ur.close_gripper(1)
        self.ur.move_joints(self.rod_sequence.joints['rod_safeheight'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['face_tray'], velocity=self.joint_velocity)

    def place_rod(self):
        self.ur.move_joints(self.rod_sequence.joints['face_tray'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['rod_safeheight'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['rod_engage'], velocity=self.joint_velocity)
        self.ur.close_gripper(0.5)
        self.ur.move_joints(self.rod_sequence.joints['rod_safeheight'], velocity=self.joint_velocity)
        self.ur.move_joints(self.rod_sequence.joints['face_tray'], velocity=self.joint_velocity)

    def run_centrifuge(self, centrifuge_duration: float):
        # remove cover
        self.pickup_cover_from_centrifuge()
        self.place_cover_in_safe_location()
        # rotate centrifuge to correct location
        self.rotate_centrifuge()
        # move vial to centrifuge
        self.pickup_vial_from_transfer_location()
        self.place_vial_in_centrifuge()
        #
        # self.rotate_centrifuge_inverse()
        # self.pickup_vial_from_transfer_location()
        # self.place_vial_in_centrifuge()

        # place cover on centrifuge
        self.pickup_cover_from_safe_location()
        self.place_cover_on_centrifuge()
        # run centrifuge
        self.spin_centrifuge(centrifuge_duration)
        # remove cover
        self.pickup_cover_from_centrifuge()
        self.place_cover_in_safe_location()
        # rotate centrifuge to correct location
        self.rotate_centrifuge()
        # move vial to transfer location
        self.pickup_vial_from_centrifuge()
        self.place_vial_in_transfer_location()
        # place cover on centrifuge
        self.pickup_cover_from_safe_location()
        self.place_cover_on_centrifuge()
