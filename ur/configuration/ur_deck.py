from hein_robots.universal_robots.urscript_sequence import URScriptSequence
from hein_robots.robotics import Location, Cartesian
from hein_robots.grids import Grid, StaticGrid

from ur.components.quantos_guns.quantos_gun import QuantosGuns
from n9.configuration.deck import quan, centrifuge_output, UR_arm
# arm
from ur.components.ur3_centrifuge.ur3_centrifuge import UR3Centrifuge
from ur.components.vision_angle_measurement.measurement import VisionDotAngleMeasurement

arm = UR_arm

# centrifuge vision
centrifuge_angle_measurement = VisionDotAngleMeasurement(
   '../../UR/components/vision_angle_measurement/roi_selection.json',
   '../../UR/components/vision_angle_measurement/colour_limits.yaml', camera_index=2)

ur3_centrifuge = UR3Centrifuge(arm, centrifuge_output, centrifuge_angle_measurement,
                           centrifuge_sequence_file='../../UR/configuration/sequences/centrifuge_center.script',
                          cover_sequence_file='../../UR/configuration/sequences/cover.script',
                           vial_sequence_file='../../UR/configuration/sequences/cent_tube.script',
                           rod_sequence_file='../../UR/configuration/sequences/rod.script',
                           )

WALL_GRID_SEQUENCE_NAMES = ['engage_1', 'engage_2', 'engage_3', 'engage_4', 'engage_5', 'engage_6']
wall_grid = StaticGrid([Location(x=-299.1, y=300.4, z=181.8, rx=90.0, ry=0, rz=-90.0),
                        Location(x=-299.2, y=251.7, z=178.2, rx=90.0, ry=-0, rz=-90.0),
                        Location(x=-299.1, y=301.1, z=273.0, rx=90.0, ry=0, rz=-90.0),
                        Location(x=-299.1, y=252.6, z=271.4, rx=90.0, ry=-0.0002063, rz=-90.0),
                        Location(x=-298.9, y=301.2, z=365.0, rx=90.0, ry=0.0009137, rz=-90.0),
                        Location(x=-297.2, y=253.5, z=361.8, rx=90.0, ry=0.0004479, rz=-90.0)],
                       columns=2, rows=3)
wall_sequence_file = '../../UR/configuration/sequences/indexes1.script'
sequence = URScriptSequence(wall_sequence_file)
print(sequence.locations.values())
quantos_guns = QuantosGuns(arm, quantos=quan, wall_grid=wall_grid,
                          wall_sequence_file='../../UR/configuration/sequences/indexes1.script',
                          quantos_sequence_file='../../UR/configuration/sequences/rbtprogrm1.script')
