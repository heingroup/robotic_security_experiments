from camera import Camera
import cv2
from pathlib import Path
from keras_segmentation.models.unet import resnet50_unet

def height_width(image):
    """
    Find the height and width of an image, whether it is a grey scale image or not

    :param image: an image, as a numpy array
    :return: int, int: the height and width of an image
    """
    if len(image.shape) is 3:
        image_height, image_width, _ = image.shape
    elif len(image.shape) is 2:
        image_height, image_width = image.shape
    else:
        raise ValueError('Image must be passed as a numpy array and have either 3 or 2 channels')

    return image_height, image_width


if __name__ == '__main__':
    CAMERA_PORT = 1  # todo
    TEMP_SAVE_PATH = Path(r'temp.png')

    camera = Camera(CAMERA_PORT)
    model = resnet50_unet(n_classes=2, input_height=512, input_width=512)
    model.load_weights("model2.h5")

    # live stream webcam and image segmentation output to find a vial
    camera_window_name = 'Camera'
    height, width = height_width(camera.take_photo())
    cv2.resizeWindow(winname=camera_window_name, width=width, height=height)
    cv2.namedWindow(camera_window_name, cv2.WINDOW_NORMAL)
    segment_window_name = 'Vial detection ML - segment2'
    cv2.resizeWindow(winname=segment_window_name, width=255, height=255)
    cv2.namedWindow(segment_window_name, cv2.WINDOW_NORMAL)
    while True:
        image = camera.take_photo()
        segment_image = model.predict_segmentation(inp=image, out_fname=str(TEMP_SAVE_PATH))
        cv2.imshow(winname=camera_window_name, mat=image)
        segment_image = cv2.imread(str(TEMP_SAVE_PATH))
        cv2.imshow(winname=segment_window_name, mat=segment_image)
        cv2.waitKey(1)

